{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 29,
  "bbox_top": 1,
  "bbox_bottom": 27,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 28,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"43129842-09fc-4d03-97db-107827e58193","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43129842-09fc-4d03-97db-107827e58193","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"LayerId":{"name":"32480b6b-c6f0-4ca8-ae8f-6998fc58efcb","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_fire_joe_stand","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"resourceVersion":"1.0","name":"43129842-09fc-4d03-97db-107827e58193","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"31016dfd-2207-4356-9c17-a3bb0bac210b","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"31016dfd-2207-4356-9c17-a3bb0bac210b","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"LayerId":{"name":"32480b6b-c6f0-4ca8-ae8f-6998fc58efcb","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_fire_joe_stand","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"resourceVersion":"1.0","name":"31016dfd-2207-4356-9c17-a3bb0bac210b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_fire_joe_stand","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5c02f633-e409-49d0-8f5e-543066509b2f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43129842-09fc-4d03-97db-107827e58193","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"32a8c733-c829-4677-8e52-36bef3873308","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"31016dfd-2207-4356-9c17-a3bb0bac210b","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 19,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_fire_joe_stand","path":"sprites/spr_fire_joe_stand/spr_fire_joe_stand.yy",},
    "resourceVersion": "1.3",
    "name": "spr_fire_joe_stand",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"32480b6b-c6f0-4ca8-ae8f-6998fc58efcb","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "FireJoe",
    "path": "folders/Sprites/ENEMIES/FireJoe.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_fire_joe_stand",
  "tags": [],
  "resourceType": "GMSprite",
}