{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 223,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 224,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a9ffb312-cef8-45b1-8713-20f58838a72f","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9ffb312-cef8-45b1-8713-20f58838a72f","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},"LayerId":{"name":"9378e3c7-3fa9-4c98-8176-04d7c58b3121","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_slash_man_6","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},"resourceVersion":"1.0","name":"a9ffb312-cef8-45b1-8713-20f58838a72f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bg_slash_man_6","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"603fdee4-5396-42a6-a6bc-532f08c1aa03","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9ffb312-cef8-45b1-8713-20f58838a72f","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bg_slash_man_6","path":"sprites/spr_bg_slash_man_6/spr_bg_slash_man_6.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bg_slash_man_6",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9378e3c7-3fa9-4c98-8176-04d7c58b3121","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "SlashMan",
    "path": "folders/Sprites/STAGE/SlashMan.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bg_slash_man_6",
  "tags": [],
  "resourceType": "GMSprite",
}