{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 29,
  "bbox_top": 0,
  "bbox_bottom": 9,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 34,
  "height": 19,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ad584c70-9ecd-497a-bd7a-9c3132e114d3","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ad584c70-9ecd-497a-bd7a-9c3132e114d3","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"LayerId":{"name":"e577c0de-3678-4f8e-bc47-a5543747fde5","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_jet_fly","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"resourceVersion":"1.0","name":"ad584c70-9ecd-497a-bd7a-9c3132e114d3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"df726d64-1aad-47b7-8425-4c1d5f566610","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"df726d64-1aad-47b7-8425-4c1d5f566610","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"LayerId":{"name":"e577c0de-3678-4f8e-bc47-a5543747fde5","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_jet_fly","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"resourceVersion":"1.0","name":"df726d64-1aad-47b7-8425-4c1d5f566610","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_jet_fly","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"faf0edaa-2761-4a58-99f7-53080377f9ea","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ad584c70-9ecd-497a-bd7a-9c3132e114d3","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c1e5b98-81e3-4789-97d1-8287207355fd","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"df726d64-1aad-47b7-8425-4c1d5f566610","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 17,
    "yorigin": 10,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_jet_fly","path":"sprites/spr_proto_jet_fly/spr_proto_jet_fly.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_jet_fly",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e577c0de-3678-4f8e-bc47-a5543747fde5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Proto-Tool",
    "path": "folders/Sprites/PLAYER/SUPPORT/Proto-Tool.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_jet_fly",
  "tags": [],
  "resourceType": "GMSprite",
}