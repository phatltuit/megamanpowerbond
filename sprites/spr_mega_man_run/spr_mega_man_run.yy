{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 19,
  "bbox_top": 3,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 24,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fc42e0d1-562d-4524-be8a-cf54dc012d85","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc42e0d1-562d-4524-be8a-cf54dc012d85","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":{"name":"342e5473-24b6-4c1f-adb9-34eab17df3c2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_run","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"fc42e0d1-562d-4524-be8a-cf54dc012d85","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9df1beb3-ab5a-424b-96c8-259f9fd98258","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9df1beb3-ab5a-424b-96c8-259f9fd98258","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":{"name":"342e5473-24b6-4c1f-adb9-34eab17df3c2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_run","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"9df1beb3-ab5a-424b-96c8-259f9fd98258","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"230c1731-0d83-47f6-826b-e959d1a54383","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"230c1731-0d83-47f6-826b-e959d1a54383","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":{"name":"342e5473-24b6-4c1f-adb9-34eab17df3c2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_run","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"230c1731-0d83-47f6-826b-e959d1a54383","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b5e749b0-28a4-4583-9b1b-7c205ebb6be2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b5e749b0-28a4-4583-9b1b-7c205ebb6be2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"LayerId":{"name":"342e5473-24b6-4c1f-adb9-34eab17df3c2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_run","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","name":"b5e749b0-28a4-4583-9b1b-7c205ebb6be2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mega_man_run","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"81a1c80c-f204-448e-9011-1608bf0a4f4f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc42e0d1-562d-4524-be8a-cf54dc012d85","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"be4aa9cb-4008-447e-820a-71c3eb4b3f40","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9df1beb3-ab5a-424b-96c8-259f9fd98258","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e9a64f45-a217-4e00-ba1e-5b7f28c2e65f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"230c1731-0d83-47f6-826b-e959d1a54383","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"217daa41-747c-4a77-8951-d5730c4885a5","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b5e749b0-28a4-4583-9b1b-7c205ebb6be2","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mega_man_run","path":"sprites/spr_mega_man_run/spr_mega_man_run.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"342e5473-24b6-4c1f-adb9-34eab17df3c2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Normal",
    "path": "folders/Sprites/PLAYER/MEGAMAN/Normal.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mega_man_run",
  "tags": [],
  "resourceType": "GMSprite",
}