{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 17,
  "bbox_top": 2,
  "bbox_bottom": 17,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 20,
  "height": 20,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"0bf87b6b-d341-49fc-aa1b-c5b2a56e1a8f","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0bf87b6b-d341-49fc-aa1b-c5b2a56e1a8f","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":{"name":"d8cfb761-33e1-47ec-bd54-c124555da147","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"0bf87b6b-d341-49fc-aa1b-c5b2a56e1a8f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b9342606-cec7-4371-9b29-5f9ab72f4d77","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b9342606-cec7-4371-9b29-5f9ab72f4d77","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":{"name":"d8cfb761-33e1-47ec-bd54-c124555da147","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"b9342606-cec7-4371-9b29-5f9ab72f4d77","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4aeeee23-d2ee-41a9-afaa-c8137a35f438","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4aeeee23-d2ee-41a9-afaa-c8137a35f438","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":{"name":"d8cfb761-33e1-47ec-bd54-c124555da147","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"4aeeee23-d2ee-41a9-afaa-c8137a35f438","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fce56062-d9e6-42b0-afb3-373b7e21377f","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fce56062-d9e6-42b0-afb3-373b7e21377f","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":{"name":"d8cfb761-33e1-47ec-bd54-c124555da147","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"fce56062-d9e6-42b0-afb3-373b7e21377f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3cb12980-13c3-42eb-a4df-417d8224b484","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3cb12980-13c3-42eb-a4df-417d8224b484","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":{"name":"d8cfb761-33e1-47ec-bd54-c124555da147","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"3cb12980-13c3-42eb-a4df-417d8224b484","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"178bb5fa-70ef-4e9e-abc3-1260930d05e2","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"178bb5fa-70ef-4e9e-abc3-1260930d05e2","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"LayerId":{"name":"d8cfb761-33e1-47ec-bd54-c124555da147","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","name":"178bb5fa-70ef-4e9e-abc3-1260930d05e2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8210db46-2131-4af4-b140-cfcb801b857c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0bf87b6b-d341-49fc-aa1b-c5b2a56e1a8f","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"353a84de-f950-47dc-9950-6c520a9bed7b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9342606-cec7-4371-9b29-5f9ab72f4d77","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"17945a36-9608-4823-a4a6-dff2014395fe","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4aeeee23-d2ee-41a9-afaa-c8137a35f438","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d5578a5e-a574-4687-a0ea-b8c3d3db65ba","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fce56062-d9e6-42b0-afb3-373b7e21377f","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f20ff758-13ad-4232-b5f6-a991577bc3f4","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3cb12980-13c3-42eb-a4df-417d8224b484","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"782aec4a-3056-4d29-923f-5323c44f2465","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"178bb5fa-70ef-4e9e-abc3-1260930d05e2","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 10,
    "yorigin": 10,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_boss_power_orb","path":"sprites/spr_boss_power_orb/spr_boss_power_orb.yy",},
    "resourceVersion": "1.3",
    "name": "spr_boss_power_orb",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d8cfb761-33e1-47ec-bd54-c124555da147","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "BOSS",
    "path": "folders/Sprites/BOSS.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_boss_power_orb",
  "tags": [],
  "resourceType": "GMSprite",
}