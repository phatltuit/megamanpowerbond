{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 28,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 29,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"12bbd070-3ae3-4051-932d-4eda0e738601","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12bbd070-3ae3-4051-932d-4eda0e738601","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"LayerId":{"name":"40e098b0-4862-4f38-9f08-b1ba27c36a12","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_climb","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"resourceVersion":"1.0","name":"12bbd070-3ae3-4051-932d-4eda0e738601","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e5b3817c-abf0-4798-b3d0-c859ef7a8684","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e5b3817c-abf0-4798-b3d0-c859ef7a8684","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"LayerId":{"name":"40e098b0-4862-4f38-9f08-b1ba27c36a12","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_climb","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"resourceVersion":"1.0","name":"e5b3817c-abf0-4798-b3d0-c859ef7a8684","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_man_climb","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9cbf7845-8495-47da-a6d0-8b567120c46d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12bbd070-3ae3-4051-932d-4eda0e738601","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"35c907e7-8fdd-4452-81b0-cf8424647f88","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e5b3817c-abf0-4798-b3d0-c859ef7a8684","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_man_climb","path":"sprites/spr_proto_man_climb/spr_proto_man_climb.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_man_climb",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"40e098b0-4862-4f38-9f08-b1ba27c36a12","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PROTOMAN",
    "path": "folders/Sprites/PLAYER/PROTOMAN.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_man_climb",
  "tags": [],
  "resourceType": "GMSprite",
}