{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 223,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 224,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9ecb8986-bc5c-45fd-b914-f361a4650859","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9ecb8986-bc5c-45fd-b914-f361a4650859","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"LayerId":{"name":"b817ba5d-8289-4595-babb-718574082665","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_menu","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","name":"9ecb8986-bc5c-45fd-b914-f361a4650859","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62f5e341-0c3c-46c4-9d3b-00134de8c9fc","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62f5e341-0c3c-46c4-9d3b-00134de8c9fc","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"LayerId":{"name":"b817ba5d-8289-4595-babb-718574082665","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_menu","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","name":"62f5e341-0c3c-46c4-9d3b-00134de8c9fc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f55a2315-9708-4e50-97af-0e53381ae8ef","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f55a2315-9708-4e50-97af-0e53381ae8ef","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"LayerId":{"name":"b817ba5d-8289-4595-babb-718574082665","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_menu","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","name":"f55a2315-9708-4e50-97af-0e53381ae8ef","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_weapon_menu","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7e9dacdf-f48b-4003-90fa-371552feb552","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9ecb8986-bc5c-45fd-b914-f361a4650859","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6796866-ce15-461c-a2ae-254435a8de6c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62f5e341-0c3c-46c4-9d3b-00134de8c9fc","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"87f9f254-d6a2-4f9d-a0ea-5eb405ed67df","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f55a2315-9708-4e50-97af-0e53381ae8ef","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_weapon_menu","path":"sprites/spr_weapon_menu/spr_weapon_menu.yy",},
    "resourceVersion": "1.3",
    "name": "spr_weapon_menu",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b817ba5d-8289-4595-babb-718574082665","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Menu",
    "path": "folders/Sprites/PLAYER/WEAPON/Menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_weapon_menu",
  "tags": [],
  "resourceType": "GMSprite",
}