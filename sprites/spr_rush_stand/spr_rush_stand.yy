{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 26,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 27,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a330de34-aeb3-47fb-892f-61a404c2c148","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a330de34-aeb3-47fb-892f-61a404c2c148","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"LayerId":{"name":"bcee2d8e-5f61-48ae-985b-5e15ce0d2b96","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_rush_stand","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"resourceVersion":"1.0","name":"a330de34-aeb3-47fb-892f-61a404c2c148","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"898edb1a-5a7b-4800-8476-969b793f46f2","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"898edb1a-5a7b-4800-8476-969b793f46f2","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"LayerId":{"name":"bcee2d8e-5f61-48ae-985b-5e15ce0d2b96","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_rush_stand","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"resourceVersion":"1.0","name":"898edb1a-5a7b-4800-8476-969b793f46f2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_rush_stand","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"801df03a-b283-4b0a-926d-aa448e3039b0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a330de34-aeb3-47fb-892f-61a404c2c148","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c211f3a6-7e66-4d9a-8897-e6c623b3de8f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"898edb1a-5a7b-4800-8476-969b793f46f2","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_rush_stand","path":"sprites/spr_rush_stand/spr_rush_stand.yy",},
    "resourceVersion": "1.3",
    "name": "spr_rush_stand",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"bcee2d8e-5f61-48ae-985b-5e15ce0d2b96","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Rush",
    "path": "folders/Sprites/PLAYER/SUPPORT/Rush.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_rush_stand",
  "tags": [],
  "resourceType": "GMSprite",
}