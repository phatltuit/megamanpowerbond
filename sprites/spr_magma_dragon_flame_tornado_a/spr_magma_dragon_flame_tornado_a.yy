{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 19,
  "bbox_top": 6,
  "bbox_bottom": 143,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 20,
  "height": 144,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4d646050-dd4a-4b51-9c33-eb64687f9c45","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4d646050-dd4a-4b51-9c33-eb64687f9c45","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":{"name":"cd0571ad-bbb1-400b-8a8d-aaceb1f62d13","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_magma_dragon_flame_tornado_a","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"4d646050-dd4a-4b51-9c33-eb64687f9c45","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"346dd086-0978-47c9-b118-3c5db2ecaa9d","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"346dd086-0978-47c9-b118-3c5db2ecaa9d","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":{"name":"cd0571ad-bbb1-400b-8a8d-aaceb1f62d13","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_magma_dragon_flame_tornado_a","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"346dd086-0978-47c9-b118-3c5db2ecaa9d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"901fb56c-e9ad-40d0-a5d8-1f04fcc3be7b","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"901fb56c-e9ad-40d0-a5d8-1f04fcc3be7b","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":{"name":"cd0571ad-bbb1-400b-8a8d-aaceb1f62d13","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_magma_dragon_flame_tornado_a","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"901fb56c-e9ad-40d0-a5d8-1f04fcc3be7b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f05ad8f-c83c-4d4a-80a0-307a5dd37ef1","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f05ad8f-c83c-4d4a-80a0-307a5dd37ef1","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"LayerId":{"name":"cd0571ad-bbb1-400b-8a8d-aaceb1f62d13","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_magma_dragon_flame_tornado_a","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","name":"7f05ad8f-c83c-4d4a-80a0-307a5dd37ef1","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_magma_dragon_flame_tornado_a","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4637861b-1fc9-49c3-aa1a-4163666c7364","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d646050-dd4a-4b51-9c33-eb64687f9c45","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9721813-b94c-44d6-8788-eb79bff5f3ee","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"346dd086-0978-47c9-b118-3c5db2ecaa9d","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66503672-df73-4bfa-b72f-1d241438f2f4","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"901fb56c-e9ad-40d0-a5d8-1f04fcc3be7b","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31985d27-39db-4177-9eb0-10fede570156","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f05ad8f-c83c-4d4a-80a0-307a5dd37ef1","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 11,
    "yorigin": 122,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_magma_dragon_flame_tornado_a","path":"sprites/spr_magma_dragon_flame_tornado_a/spr_magma_dragon_flame_tornado_a.yy",},
    "resourceVersion": "1.3",
    "name": "spr_magma_dragon_flame_tornado_a",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cd0571ad-bbb1-400b-8a8d-aaceb1f62d13","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Magma-Dragon",
    "path": "folders/Sprites/BOSS/Magma-Dragon.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_magma_dragon_flame_tornado_a",
  "tags": [],
  "resourceType": "GMSprite",
}