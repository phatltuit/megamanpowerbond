{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 26,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 27,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c1bca88f-b4b9-4405-acb6-3b9c8876c033","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1bca88f-b4b9-4405-acb6-3b9c8876c033","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":{"name":"cab3b48c-efb8-44be-a9e1-6beafe481055","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_adap_run","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"c1bca88f-b4b9-4405-acb6-3b9c8876c033","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aba0fea2-a2cc-4ce5-823c-4a4036f67fe3","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aba0fea2-a2cc-4ce5-823c-4a4036f67fe3","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":{"name":"cab3b48c-efb8-44be-a9e1-6beafe481055","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_adap_run","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"aba0fea2-a2cc-4ce5-823c-4a4036f67fe3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a737b065-8e6a-492f-8110-eefd3a1ee25d","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a737b065-8e6a-492f-8110-eefd3a1ee25d","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":{"name":"cab3b48c-efb8-44be-a9e1-6beafe481055","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_adap_run","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"a737b065-8e6a-492f-8110-eefd3a1ee25d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e308a362-77d3-4f82-bff9-bfb50529fc03","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e308a362-77d3-4f82-bff9-bfb50529fc03","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"LayerId":{"name":"cab3b48c-efb8-44be-a9e1-6beafe481055","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_adap_run","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","name":"e308a362-77d3-4f82-bff9-bfb50529fc03","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mega_adap_run","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ae27b2c9-1c11-4b48-95db-9ece9562fa79","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1bca88f-b4b9-4405-acb6-3b9c8876c033","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9ea55539-fb1f-4ad4-b8fd-085aca21d791","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aba0fea2-a2cc-4ce5-823c-4a4036f67fe3","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"343a1a12-2166-4fd9-966b-f977cc390281","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a737b065-8e6a-492f-8110-eefd3a1ee25d","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"94917ac6-6fb7-47fd-bdf3-df14ffdcdb82","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e308a362-77d3-4f82-bff9-bfb50529fc03","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mega_adap_run","path":"sprites/spr_mega_adap_run/spr_mega_adap_run.yy",},
    "resourceVersion": "1.3",
    "name": "spr_mega_adap_run",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cab3b48c-efb8-44be-a9e1-6beafe481055","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PowerAdapter",
    "path": "folders/Sprites/PLAYER/MEGAMAN/PowerAdapter.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mega_adap_run",
  "tags": [],
  "resourceType": "GMSprite",
}