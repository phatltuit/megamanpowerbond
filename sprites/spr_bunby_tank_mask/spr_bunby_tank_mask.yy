{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 26,
  "bbox_top": 2,
  "bbox_bottom": 28,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 29,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"87e246a7-f248-4207-9836-d6fcd8f1c999","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"87e246a7-f248-4207-9836-d6fcd8f1c999","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},"LayerId":{"name":"edec9c9c-4797-4141-b507-4ec1d3d7b2fa","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bunby_tank_mask","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},"resourceVersion":"1.0","name":"87e246a7-f248-4207-9836-d6fcd8f1c999","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bunby_tank_mask","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a838a631-79c9-4fc3-a623-8c5345a62c73","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"87e246a7-f248-4207-9836-d6fcd8f1c999","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 20,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bunby_tank_mask","path":"sprites/spr_bunby_tank_mask/spr_bunby_tank_mask.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bunby_tank_mask",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"edec9c9c-4797-4141-b507-4ec1d3d7b2fa","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "BunbyTank",
    "path": "folders/Sprites/ENEMIES/BunbyTank.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bunby_tank_mask",
  "tags": [],
  "resourceType": "GMSprite",
}