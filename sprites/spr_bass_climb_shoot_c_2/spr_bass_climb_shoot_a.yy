{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 28,
  "bbox_top": 0,
  "bbox_bottom": 30,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"71e53602-8e99-4a71-9f64-51990b3974bb","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71e53602-8e99-4a71-9f64-51990b3974bb","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"LayerId":{"name":"21829d97-20c1-438c-bc37-682d343a2b71","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_climb_shoot_a","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","name":"71e53602-8e99-4a71-9f64-51990b3974bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6cbc1949-e1ff-49e6-8f21-7302a21983d3","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6cbc1949-e1ff-49e6-8f21-7302a21983d3","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"LayerId":{"name":"21829d97-20c1-438c-bc37-682d343a2b71","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_climb_shoot_a","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","name":"6cbc1949-e1ff-49e6-8f21-7302a21983d3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"79b0cbdf-2a53-45c5-a474-f519d9cf61b0","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"79b0cbdf-2a53-45c5-a474-f519d9cf61b0","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"LayerId":{"name":"21829d97-20c1-438c-bc37-682d343a2b71","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_climb_shoot_a","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","name":"79b0cbdf-2a53-45c5-a474-f519d9cf61b0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bass_climb_shoot_a","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"264b866d-0ab1-4adb-936e-f36a09632823","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71e53602-8e99-4a71-9f64-51990b3974bb","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"948d5701-f00a-4bcb-b50a-618e57dc6810","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6cbc1949-e1ff-49e6-8f21-7302a21983d3","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"030351a0-e010-418b-bde6-7336fa9b11f5","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"79b0cbdf-2a53-45c5-a474-f519d9cf61b0","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 17,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bass_climb_shoot_a","path":"sprites/spr_bass_climb_shoot_a/spr_bass_climb_shoot_a.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bass_climb_shoot_a",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"21829d97-20c1-438c-bc37-682d343a2b71","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Normal",
    "path": "folders/Sprites/PLAYER/BASS/Normal.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bass_climb_shoot_a",
  "tags": [],
  "resourceType": "GMSprite",
}