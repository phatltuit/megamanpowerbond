{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 29,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 30,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"80aae95d-ba12-4433-bbb6-f92fe06b5780","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"80aae95d-ba12-4433-bbb6-f92fe06b5780","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":{"name":"f17ac3f0-3076-4043-af5b-12a617859a50","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_jump_shoot","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"80aae95d-ba12-4433-bbb6-f92fe06b5780","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc49770f-7ca0-48a1-85ce-f4e7a1cefacc","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc49770f-7ca0-48a1-85ce-f4e7a1cefacc","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":{"name":"f17ac3f0-3076-4043-af5b-12a617859a50","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_jump_shoot","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"cc49770f-7ca0-48a1-85ce-f4e7a1cefacc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"515f1d0c-7e7c-4d5b-a10f-b9387900c810","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"515f1d0c-7e7c-4d5b-a10f-b9387900c810","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":{"name":"f17ac3f0-3076-4043-af5b-12a617859a50","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_jump_shoot","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"515f1d0c-7e7c-4d5b-a10f-b9387900c810","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4854c0c9-676b-4ce4-9a08-310297c06e7f","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4854c0c9-676b-4ce4-9a08-310297c06e7f","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"LayerId":{"name":"f17ac3f0-3076-4043-af5b-12a617859a50","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_jump_shoot","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","name":"4854c0c9-676b-4ce4-9a08-310297c06e7f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bass_jump_shoot","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1610e658-550c-4d84-930a-6b8ab9047e07","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"80aae95d-ba12-4433-bbb6-f92fe06b5780","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d22da1ae-df88-438a-aa90-00e7f17eb433","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc49770f-7ca0-48a1-85ce-f4e7a1cefacc","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5ae66c5e-2b1f-4234-97c1-116b51cb2d7e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"515f1d0c-7e7c-4d5b-a10f-b9387900c810","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e9e9238-8282-4f95-9158-7fe29b630a4d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4854c0c9-676b-4ce4-9a08-310297c06e7f","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 18,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bass_jump_shoot","path":"sprites/spr_bass_jump_shoot/spr_bass_jump_shoot.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bass_jump_shoot",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f17ac3f0-3076-4043-af5b-12a617859a50","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Normal",
    "path": "folders/Sprites/PLAYER/BASS/Normal.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bass_jump_shoot",
  "tags": [],
  "resourceType": "GMSprite",
}