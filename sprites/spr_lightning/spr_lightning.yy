{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 1,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 95,
  "bbox_top": 0,
  "bbox_bottom": 111,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 128,
  "height": 112,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3489d102-de44-4612-b129-bcd57ddba8c9","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3489d102-de44-4612-b129-bcd57ddba8c9","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"3489d102-de44-4612-b129-bcd57ddba8c9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a9ac7651-d571-4446-83fd-501fcc0da684","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9ac7651-d571-4446-83fd-501fcc0da684","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"a9ac7651-d571-4446-83fd-501fcc0da684","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2adb5895-bee4-4328-8652-454a5626f260","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2adb5895-bee4-4328-8652-454a5626f260","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"2adb5895-bee4-4328-8652-454a5626f260","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ab63cc2-2f30-4765-9f2b-db998dfbc655","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ab63cc2-2f30-4765-9f2b-db998dfbc655","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"4ab63cc2-2f30-4765-9f2b-db998dfbc655","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fbeab999-6019-45d3-984b-872651ce09ca","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fbeab999-6019-45d3-984b-872651ce09ca","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"fbeab999-6019-45d3-984b-872651ce09ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1c73e5f2-e12f-4b50-bf58-a96b9eb4b217","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c73e5f2-e12f-4b50-bf58-a96b9eb4b217","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"1c73e5f2-e12f-4b50-bf58-a96b9eb4b217","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1cd4f814-9926-4faa-af84-d27a935cf96f","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1cd4f814-9926-4faa-af84-d27a935cf96f","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"1cd4f814-9926-4faa-af84-d27a935cf96f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62d897ea-d6ad-4b6e-8415-e10a25537010","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62d897ea-d6ad-4b6e-8415-e10a25537010","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"62d897ea-d6ad-4b6e-8415-e10a25537010","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4dc3681b-0ce7-443f-be63-cef261d5e010","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4dc3681b-0ce7-443f-be63-cef261d5e010","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"4dc3681b-0ce7-443f-be63-cef261d5e010","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca33f781-f9c0-49a9-95a3-8ca75f1315f6","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca33f781-f9c0-49a9-95a3-8ca75f1315f6","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"ca33f781-f9c0-49a9-95a3-8ca75f1315f6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"50238157-712f-4dc1-81c1-a4e6d2d63118","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"50238157-712f-4dc1-81c1-a4e6d2d63118","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"50238157-712f-4dc1-81c1-a4e6d2d63118","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf5421a2-77d5-46bc-8f1e-9be45a221d45","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf5421a2-77d5-46bc-8f1e-9be45a221d45","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"bf5421a2-77d5-46bc-8f1e-9be45a221d45","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12e9c0a5-f932-43db-a314-f4a0e1ecf10f","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12e9c0a5-f932-43db-a314-f4a0e1ecf10f","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"12e9c0a5-f932-43db-a314-f4a0e1ecf10f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"57ec52d6-8b9c-4371-b2f8-86b53c1464f2","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"57ec52d6-8b9c-4371-b2f8-86b53c1464f2","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"57ec52d6-8b9c-4371-b2f8-86b53c1464f2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d9254c4-63cd-4c4b-9959-1452ff0cf093","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d9254c4-63cd-4c4b-9959-1452ff0cf093","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"1d9254c4-63cd-4c4b-9959-1452ff0cf093","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"69f0b011-b57f-40a9-8ab7-1536f0988cac","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"69f0b011-b57f-40a9-8ab7-1536f0988cac","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"69f0b011-b57f-40a9-8ab7-1536f0988cac","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c408aeac-836f-43f5-9de8-bf88bafd5dc5","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c408aeac-836f-43f5-9de8-bf88bafd5dc5","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"c408aeac-836f-43f5-9de8-bf88bafd5dc5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2e5ef92a-55b4-4933-8532-147e0a8a4671","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2e5ef92a-55b4-4933-8532-147e0a8a4671","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"2e5ef92a-55b4-4933-8532-147e0a8a4671","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5667cc96-e9d2-4307-a66a-ad2167956ec3","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5667cc96-e9d2-4307-a66a-ad2167956ec3","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"5667cc96-e9d2-4307-a66a-ad2167956ec3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9395c686-3313-4845-af7d-4ebd4b37fc13","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9395c686-3313-4845-af7d-4ebd4b37fc13","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"9395c686-3313-4845-af7d-4ebd4b37fc13","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"32fb0082-2ea2-461f-b9f3-cc515bc56685","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"32fb0082-2ea2-461f-b9f3-cc515bc56685","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"32fb0082-2ea2-461f-b9f3-cc515bc56685","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d8f27265-d275-4cd3-a867-e6f2bf9a50c1","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8f27265-d275-4cd3-a867-e6f2bf9a50c1","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"d8f27265-d275-4cd3-a867-e6f2bf9a50c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89d7ea21-05b1-4795-9012-5b27c62d300f","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89d7ea21-05b1-4795-9012-5b27c62d300f","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"89d7ea21-05b1-4795-9012-5b27c62d300f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"353cf30e-e6a5-4b42-b860-54c5b1ee305a","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"353cf30e-e6a5-4b42-b860-54c5b1ee305a","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"353cf30e-e6a5-4b42-b860-54c5b1ee305a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70a4d474-a692-480a-b9b1-41971c6bd5fd","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70a4d474-a692-480a-b9b1-41971c6bd5fd","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"70a4d474-a692-480a-b9b1-41971c6bd5fd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18c812a5-3870-4080-b684-247ed70d64cf","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18c812a5-3870-4080-b684-247ed70d64cf","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"18c812a5-3870-4080-b684-247ed70d64cf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c37c6cd2-79ff-40d3-afcb-c9c7cbb9320c","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c37c6cd2-79ff-40d3-afcb-c9c7cbb9320c","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"c37c6cd2-79ff-40d3-afcb-c9c7cbb9320c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aab26a1b-ed7a-469d-8510-dbc6a8abde6d","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aab26a1b-ed7a-469d-8510-dbc6a8abde6d","path":"sprites/spr_lightning/spr_lightning.yy",},"LayerId":{"name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","name":"aab26a1b-ed7a-469d-8510-dbc6a8abde6d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 28.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5c171a97-5399-4885-b79b-ddc1a725b40b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3489d102-de44-4612-b129-bcd57ddba8c9","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"40b35ae0-0c35-4094-a75e-51651d148965","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9ac7651-d571-4446-83fd-501fcc0da684","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"092789d2-6a2e-434b-a413-870cf829c0d3","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2adb5895-bee4-4328-8652-454a5626f260","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"78b39428-b888-4aec-9773-ff733364310e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ab63cc2-2f30-4765-9f2b-db998dfbc655","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a033ec03-2e57-4bec-ae5f-387b00798017","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fbeab999-6019-45d3-984b-872651ce09ca","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"394a948e-6db6-491d-9f27-83220c2032d4","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c73e5f2-e12f-4b50-bf58-a96b9eb4b217","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"79e1241a-0c19-47a8-8be4-6c456c4a5f7b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1cd4f814-9926-4faa-af84-d27a935cf96f","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e6cfe659-6792-4319-a5e5-51083ee431f2","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62d897ea-d6ad-4b6e-8415-e10a25537010","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c7912a12-ad28-4cce-9e56-e3df0baad1e6","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4dc3681b-0ce7-443f-be63-cef261d5e010","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"41b42138-e6df-44e5-ab3e-a309fc2253f7","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca33f781-f9c0-49a9-95a3-8ca75f1315f6","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"58ca0c0c-bba8-4e30-8ce1-d98fe48a6343","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"50238157-712f-4dc1-81c1-a4e6d2d63118","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fcec3fbe-28ec-42a5-b128-ffb2d85bfe1e","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf5421a2-77d5-46bc-8f1e-9be45a221d45","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5b7a8b4f-75a1-4d2d-b251-1493179925fb","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12e9c0a5-f932-43db-a314-f4a0e1ecf10f","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d1e22500-7705-48db-958f-e1f98c772469","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"57ec52d6-8b9c-4371-b2f8-86b53c1464f2","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c840e97-9bfe-440b-b21c-35ade64ca896","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d9254c4-63cd-4c4b-9959-1452ff0cf093","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4c782382-9d66-4f11-b55d-8e4902467d11","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"69f0b011-b57f-40a9-8ab7-1536f0988cac","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"21b20807-4a8c-4683-889e-a7391debbba4","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c408aeac-836f-43f5-9de8-bf88bafd5dc5","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44f97b88-5f55-49aa-aadd-7e4598447e8c","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2e5ef92a-55b4-4933-8532-147e0a8a4671","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0c8246f1-49a0-4dbe-bac3-af625efdaa71","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5667cc96-e9d2-4307-a66a-ad2167956ec3","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ea6d7de-8c4d-4375-a204-33bce6ccd056","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9395c686-3313-4845-af7d-4ebd4b37fc13","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd6fbae9-bd64-4b9f-afbc-58a06cd6791d","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32fb0082-2ea2-461f-b9f3-cc515bc56685","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"613b8776-9a0f-4d99-847b-4c993db13a54","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8f27265-d275-4cd3-a867-e6f2bf9a50c1","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"267142dd-c0fb-4596-8e97-dedc8ec9336d","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89d7ea21-05b1-4795-9012-5b27c62d300f","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e6c2873c-fc01-489c-98f9-1d478cee3196","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"353cf30e-e6a5-4b42-b860-54c5b1ee305a","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31432aa3-777b-432c-a462-a5fbc866d05a","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70a4d474-a692-480a-b9b1-41971c6bd5fd","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afab4284-743a-4aaa-b198-56fa4922abb6","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18c812a5-3870-4080-b684-247ed70d64cf","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"54b3c612-766d-4b63-ace0-898f0029753a","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c37c6cd2-79ff-40d3-afcb-c9c7cbb9320c","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ff5cb92a-ff63-4190-bccc-3327d6964fdc","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aab26a1b-ed7a-469d-8510-dbc6a8abde6d","path":"sprites/spr_lightning/spr_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_lightning","path":"sprites/spr_lightning/spr_lightning.yy",},
    "resourceVersion": "1.3",
    "name": "spr_lightning",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"01949e5c-dc23-4871-bcc4-da35c04e4ef5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "EFFECT",
    "path": "folders/Sprites/EFFECT.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_lightning",
  "tags": [],
  "resourceType": "GMSprite",
}