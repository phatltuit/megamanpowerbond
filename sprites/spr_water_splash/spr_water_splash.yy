{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 30,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"61e38dbc-7291-42eb-b09d-cd9aefc69ac2","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"61e38dbc-7291-42eb-b09d-cd9aefc69ac2","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":{"name":"51512ac2-0fe0-49ae-96b6-0e432d44e28d","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"61e38dbc-7291-42eb-b09d-cd9aefc69ac2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aee02f2f-769d-428e-b22e-0caa100433cb","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aee02f2f-769d-428e-b22e-0caa100433cb","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":{"name":"51512ac2-0fe0-49ae-96b6-0e432d44e28d","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"aee02f2f-769d-428e-b22e-0caa100433cb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3eb0591f-f57b-47b8-b85b-9fbcd9850c72","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3eb0591f-f57b-47b8-b85b-9fbcd9850c72","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":{"name":"51512ac2-0fe0-49ae-96b6-0e432d44e28d","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"3eb0591f-f57b-47b8-b85b-9fbcd9850c72","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49b7dfcb-76fd-4c74-8cd7-6a1a0d24206c","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49b7dfcb-76fd-4c74-8cd7-6a1a0d24206c","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":{"name":"51512ac2-0fe0-49ae-96b6-0e432d44e28d","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"49b7dfcb-76fd-4c74-8cd7-6a1a0d24206c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"16bdc6aa-278b-4b88-9ce3-fab433e06572","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"16bdc6aa-278b-4b88-9ce3-fab433e06572","path":"sprites/spr_water_splash/spr_water_splash.yy",},"LayerId":{"name":"51512ac2-0fe0-49ae-96b6-0e432d44e28d","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","name":"16bdc6aa-278b-4b88-9ce3-fab433e06572","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e227ff12-6cb7-4762-8bde-81d4cecfe1c9","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"61e38dbc-7291-42eb-b09d-cd9aefc69ac2","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"315529fb-2808-4790-a7ed-75d61094b999","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aee02f2f-769d-428e-b22e-0caa100433cb","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b77afc83-1fc7-4f46-aefd-54a13a603b1d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3eb0591f-f57b-47b8-b85b-9fbcd9850c72","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"484c8a8c-b4e1-4bdd-bd61-c9394c8af018","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49b7dfcb-76fd-4c74-8cd7-6a1a0d24206c","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"703a9ae4-4545-46b6-bec8-1b35aa2db87b","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"16bdc6aa-278b-4b88-9ce3-fab433e06572","path":"sprites/spr_water_splash/spr_water_splash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 18,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_water_splash","path":"sprites/spr_water_splash/spr_water_splash.yy",},
    "resourceVersion": "1.3",
    "name": "spr_water_splash",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"51512ac2-0fe0-49ae-96b6-0e432d44e28d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "EFFECT",
    "path": "folders/Sprites/EFFECT.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_water_splash",
  "tags": [],
  "resourceType": "GMSprite",
}