{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 33,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 35,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"586b3de7-da50-4215-b642-2058ce0a04fd","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"586b3de7-da50-4215-b642-2058ce0a04fd","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"LayerId":{"name":"d1bc0141-50c3-4ba9-a87d-2db1d72798b0","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_treble_fly_transform","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","name":"586b3de7-da50-4215-b642-2058ce0a04fd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee3e420a-8a68-426f-8bec-1e1069a01e5e","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee3e420a-8a68-426f-8bec-1e1069a01e5e","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"LayerId":{"name":"d1bc0141-50c3-4ba9-a87d-2db1d72798b0","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_treble_fly_transform","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","name":"ee3e420a-8a68-426f-8bec-1e1069a01e5e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4aab0f84-cb08-4ef1-bd65-592d0688154f","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4aab0f84-cb08-4ef1-bd65-592d0688154f","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"LayerId":{"name":"d1bc0141-50c3-4ba9-a87d-2db1d72798b0","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_treble_fly_transform","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","name":"4aab0f84-cb08-4ef1-bd65-592d0688154f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_treble_fly_transform","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2337850c-eb2d-4591-a91b-1e44428c6c39","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"586b3de7-da50-4215-b642-2058ce0a04fd","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bfc3715d-99e0-4229-9ea5-0a16d0a9702b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee3e420a-8a68-426f-8bec-1e1069a01e5e","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6273b1b-550a-4fd2-88aa-2a332c643355","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4aab0f84-cb08-4ef1-bd65-592d0688154f","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_treble_fly_transform","path":"sprites/spr_treble_fly_transform/spr_treble_fly_transform.yy",},
    "resourceVersion": "1.3",
    "name": "spr_treble_fly_transform",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d1bc0141-50c3-4ba9-a87d-2db1d72798b0","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Treble",
    "path": "folders/Sprites/PLAYER/SUPPORT/Treble.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_treble_fly_transform",
  "tags": [],
  "resourceType": "GMSprite",
}