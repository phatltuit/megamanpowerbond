{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 11,
  "bbox_right": 29,
  "bbox_top": 2,
  "bbox_bottom": 35,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 46,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"25b94393-7f24-4e1b-8c93-2246ff00f75d","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25b94393-7f24-4e1b-8c93-2246ff00f75d","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":{"name":"f89a5fdf-83cc-4395-b197-8178203c1882","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"25b94393-7f24-4e1b-8c93-2246ff00f75d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d60ea3e9-f1a5-442d-9476-a77da0a0c5bf","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d60ea3e9-f1a5-442d-9476-a77da0a0c5bf","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":{"name":"f89a5fdf-83cc-4395-b197-8178203c1882","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"d60ea3e9-f1a5-442d-9476-a77da0a0c5bf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"defc3b76-9ab4-4393-b0a1-f3fb4119151a","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"defc3b76-9ab4-4393-b0a1-f3fb4119151a","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":{"name":"f89a5fdf-83cc-4395-b197-8178203c1882","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"defc3b76-9ab4-4393-b0a1-f3fb4119151a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12a54f3f-a181-4a66-bbb2-675a8b7b2225","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12a54f3f-a181-4a66-bbb2-675a8b7b2225","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":{"name":"f89a5fdf-83cc-4395-b197-8178203c1882","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"12a54f3f-a181-4a66-bbb2-675a8b7b2225","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"32e76876-5dea-455e-939f-074843094dbb","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"32e76876-5dea-455e-939f-074843094dbb","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"LayerId":{"name":"f89a5fdf-83cc-4395-b197-8178203c1882","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","name":"32e76876-5dea-455e-939f-074843094dbb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8daf4efa-ea23-46a8-aa92-9358b1248adb","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25b94393-7f24-4e1b-8c93-2246ff00f75d","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d1c80afd-3abb-47a4-8971-399c70f67b0f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d60ea3e9-f1a5-442d-9476-a77da0a0c5bf","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"75aaf630-e3ba-4fd8-b239-f60a581faf30","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"defc3b76-9ab4-4393-b0a1-f3fb4119151a","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dcbe4b73-3f19-46a2-ad11-9cb1bed7a965","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12a54f3f-a181-4a66-bbb2-675a8b7b2225","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9c68b0f-bcac-472e-8e35-09518ca91146","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32e76876-5dea-455e-939f-074843094dbb","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 21,
    "yorigin": 20,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mega_man_jump_slash_2","path":"sprites/spr_mega_man_jump_slash_2/spr_mega_man_jump_slash_2.yy",},
    "resourceVersion": "1.3",
    "name": "spr_mega_man_jump_slash_2",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f89a5fdf-83cc-4395-b197-8178203c1882","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Primary",
    "path": "folders/Sprites/PLAYER/MEGAMAN/Primary.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mega_man_jump_slash_2",
  "tags": [],
  "resourceType": "GMSprite",
}