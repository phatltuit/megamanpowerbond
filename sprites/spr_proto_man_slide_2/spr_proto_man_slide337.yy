{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 29,
  "bbox_top": 4,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c77d074c-a9ca-4942-b22d-58f5dc165f5f","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c77d074c-a9ca-4942-b22d-58f5dc165f5f","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},"LayerId":{"name":"b94b1c49-6eac-48a5-9413-8b041f4e2a5a","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_slide337","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},"resourceVersion":"1.0","name":"c77d074c-a9ca-4942-b22d-58f5dc165f5f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_man_slide337","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f33dac2c-6fe6-4790-9a32-1296d5126f34","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c77d074c-a9ca-4942-b22d-58f5dc165f5f","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 17,
    "yorigin": 12,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_man_slide337","path":"sprites/spr_proto_man_slide337/spr_proto_man_slide337.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_man_slide",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b94b1c49-6eac-48a5-9413-8b041f4e2a5a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Primary",
    "path": "folders/Sprites/PLAYER/PROTOMAN/Primary.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_man_slide337",
  "tags": [],
  "resourceType": "GMSprite",
}