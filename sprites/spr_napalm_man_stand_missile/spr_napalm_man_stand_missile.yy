{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"eecb7725-2e65-475e-b854-cee93596f383","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eecb7725-2e65-475e-b854-cee93596f383","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"LayerId":{"name":"df53ef93-7838-4c7e-98dc-07852f75ee08","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_napalm_man_stand_missile","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","name":"eecb7725-2e65-475e-b854-cee93596f383","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee322f0c-5c77-4f3e-a933-acb11c91ac09","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee322f0c-5c77-4f3e-a933-acb11c91ac09","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"LayerId":{"name":"df53ef93-7838-4c7e-98dc-07852f75ee08","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_napalm_man_stand_missile","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","name":"ee322f0c-5c77-4f3e-a933-acb11c91ac09","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a79191e5-42ac-450d-b4e9-3f09bbf8fc2b","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a79191e5-42ac-450d-b4e9-3f09bbf8fc2b","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"LayerId":{"name":"df53ef93-7838-4c7e-98dc-07852f75ee08","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_napalm_man_stand_missile","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","name":"a79191e5-42ac-450d-b4e9-3f09bbf8fc2b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_napalm_man_stand_missile","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"dc400b4a-2afd-4c28-86a0-d5a6d557d934","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eecb7725-2e65-475e-b854-cee93596f383","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e160733d-5b48-4937-9782-fc4f4bee7b46","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee322f0c-5c77-4f3e-a933-acb11c91ac09","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a1e9b8c5-ddab-4b0e-97c3-6683ccab79ae","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a79191e5-42ac-450d-b4e9-3f09bbf8fc2b","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_napalm_man_stand_missile","path":"sprites/spr_napalm_man_stand_missile/spr_napalm_man_stand_missile.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"df53ef93-7838-4c7e-98dc-07852f75ee08","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Napalm-Man",
    "path": "folders/Sprites/BOSS/Napalm-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_napalm_man_stand_missile",
  "tags": [],
  "resourceType": "GMSprite",
}