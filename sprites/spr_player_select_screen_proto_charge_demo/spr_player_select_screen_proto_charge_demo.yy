{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 23,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 25,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fcca19a4-b1ac-4824-8d72-85b9e4f7b117","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fcca19a4-b1ac-4824-8d72-85b9e4f7b117","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"LayerId":{"name":"7c3d3f92-40b7-4214-9d4c-1cbd02813e33","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_select_screen_proto_charge_demo","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","name":"fcca19a4-b1ac-4824-8d72-85b9e4f7b117","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"470c4f39-1aef-47ea-8721-fbd7ac5d0ed0","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"470c4f39-1aef-47ea-8721-fbd7ac5d0ed0","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"LayerId":{"name":"7c3d3f92-40b7-4214-9d4c-1cbd02813e33","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_select_screen_proto_charge_demo","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","name":"470c4f39-1aef-47ea-8721-fbd7ac5d0ed0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ccd691f9-b182-4a9f-b0c6-463ccab93bc9","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ccd691f9-b182-4a9f-b0c6-463ccab93bc9","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"LayerId":{"name":"7c3d3f92-40b7-4214-9d4c-1cbd02813e33","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_select_screen_proto_charge_demo","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","name":"ccd691f9-b182-4a9f-b0c6-463ccab93bc9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_select_screen_proto_charge_demo","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"1d6ae3de-1948-477e-b293-895c0f56b2b4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fcca19a4-b1ac-4824-8d72-85b9e4f7b117","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"952cc945-74bf-44c0-b52d-eed1aee1782e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"470c4f39-1aef-47ea-8721-fbd7ac5d0ed0","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a58b575-cc3b-46fc-8da1-c814589c9f03","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ccd691f9-b182-4a9f-b0c6-463ccab93bc9","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 2,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_select_screen_proto_charge_demo","path":"sprites/spr_player_select_screen_proto_charge_demo/spr_player_select_screen_proto_charge_demo.yy",},
    "resourceVersion": "1.3",
    "name": "spr_player_select_screen_proto_charge_demo",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"7c3d3f92-40b7-4214-9d4c-1cbd02813e33","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Intro",
    "path": "folders/Sprites/SCREEN/Intro.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_select_screen_proto_charge_demo",
  "tags": [],
  "resourceType": "GMSprite",
}