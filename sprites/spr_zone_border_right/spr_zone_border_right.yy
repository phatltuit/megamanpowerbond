{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 13,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 223,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 224,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3bcceaa9-aa7c-4a9e-ab47-2480e90d1b83","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3bcceaa9-aa7c-4a9e-ab47-2480e90d1b83","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},"LayerId":{"name":"6000e4c0-0a35-4a8a-bbed-1a4bf14156ea","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_zone_border_right","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},"resourceVersion":"1.0","name":"3bcceaa9-aa7c-4a9e-ab47-2480e90d1b83","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_zone_border_right","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"66891ca2-e94c-453a-b92f-d8e53ddd7894","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3bcceaa9-aa7c-4a9e-ab47-2480e90d1b83","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_zone_border_right","path":"sprites/spr_zone_border_right/spr_zone_border_right.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6000e4c0-0a35-4a8a-bbed-1a4bf14156ea","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ZoneBorder",
    "path": "folders/Sprites/ZONE/ZoneBorder.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_zone_border_right",
  "tags": [],
  "resourceType": "GMSprite",
}