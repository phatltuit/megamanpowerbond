{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 23,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 24,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f1e42a74-e6bb-4cd2-8f9c-854cacdd1003","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f1e42a74-e6bb-4cd2-8f9c-854cacdd1003","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"LayerId":{"name":"042e760f-19dd-4367-8b05-c1f710f1e105","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_hanya_attacker_patrol","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"resourceVersion":"1.0","name":"f1e42a74-e6bb-4cd2-8f9c-854cacdd1003","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3e03374c-8850-44d5-8629-4193d8932edd","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3e03374c-8850-44d5-8629-4193d8932edd","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"LayerId":{"name":"042e760f-19dd-4367-8b05-c1f710f1e105","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_hanya_attacker_patrol","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"resourceVersion":"1.0","name":"3e03374c-8850-44d5-8629-4193d8932edd","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_hanya_attacker_patrol","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5eb41f0a-5238-46b9-9f22-c8adb4e3fc39","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f1e42a74-e6bb-4cd2-8f9c-854cacdd1003","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"69deaeca-d919-4592-ab9d-7211261c5916","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3e03374c-8850-44d5-8629-4193d8932edd","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_hanya_attacker_patrol","path":"sprites/spr_hanya_attacker_patrol/spr_hanya_attacker_patrol.yy",},
    "resourceVersion": "1.3",
    "name": "spr_hanya_attacker_patrol",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"042e760f-19dd-4367-8b05-c1f710f1e105","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "HanyaAttacker",
    "path": "folders/Sprites/ENEMIES/HanyaAttacker.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_hanya_attacker_patrol",
  "tags": [],
  "resourceType": "GMSprite",
}