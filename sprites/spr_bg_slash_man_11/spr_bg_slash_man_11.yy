{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 223,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 224,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e32a4184-99b9-43b1-822f-546434eeedce","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e32a4184-99b9-43b1-822f-546434eeedce","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},"LayerId":{"name":"4db6723c-a400-4ceb-9fe4-0714f3722433","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_slash_man_11","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},"resourceVersion":"1.0","name":"e32a4184-99b9-43b1-822f-546434eeedce","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bg_slash_man_11","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fa905157-6313-4c0a-95d5-847c7295d77b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e32a4184-99b9-43b1-822f-546434eeedce","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bg_slash_man_11","path":"sprites/spr_bg_slash_man_11/spr_bg_slash_man_11.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bg_slash_man_11",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4db6723c-a400-4ceb-9fe4-0714f3722433","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "SlashMan",
    "path": "folders/Sprites/STAGE/SlashMan.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bg_slash_man_11",
  "tags": [],
  "resourceType": "GMSprite",
}