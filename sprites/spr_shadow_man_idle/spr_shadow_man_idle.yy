{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 28,
  "bbox_top": 1,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 29,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"83e44b35-f40b-420d-a392-6fb81cb40485","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"83e44b35-f40b-420d-a392-6fb81cb40485","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":{"name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"83e44b35-f40b-420d-a392-6fb81cb40485","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b9b2c3c9-b58c-4b05-b8aa-aa99695cac40","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b9b2c3c9-b58c-4b05-b8aa-aa99695cac40","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":{"name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"b9b2c3c9-b58c-4b05-b8aa-aa99695cac40","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb71bc5d-496f-4f24-99d2-be2f839a9027","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb71bc5d-496f-4f24-99d2-be2f839a9027","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":{"name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"eb71bc5d-496f-4f24-99d2-be2f839a9027","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f53fc2fb-5a1c-49b3-8387-04e2921232d7","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f53fc2fb-5a1c-49b3-8387-04e2921232d7","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":{"name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"f53fc2fb-5a1c-49b3-8387-04e2921232d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"92f1e658-265f-4117-8f45-8f18e98b2e90","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92f1e658-265f-4117-8f45-8f18e98b2e90","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":{"name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"92f1e658-265f-4117-8f45-8f18e98b2e90","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ab44bc8-2b02-45ca-a371-84c1d0917d22","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ab44bc8-2b02-45ca-a371-84c1d0917d22","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"LayerId":{"name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","name":"4ab44bc8-2b02-45ca-a371-84c1d0917d22","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e1fcc321-2cc3-4b35-94c4-36d62d7312ba","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"83e44b35-f40b-420d-a392-6fb81cb40485","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2b9796f4-cc47-4d17-81c2-20993ab6ba2b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9b2c3c9-b58c-4b05-b8aa-aa99695cac40","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e348b579-5fb3-40af-b36c-38439f094641","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb71bc5d-496f-4f24-99d2-be2f839a9027","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b2f7d5b7-c263-4e26-a319-86fe668c3a29","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f53fc2fb-5a1c-49b3-8387-04e2921232d7","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"76b96c67-30aa-4dcc-b1da-d30fb07b5087","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92f1e658-265f-4117-8f45-8f18e98b2e90","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"29f34cd7-e4c3-4650-b3fb-354a9e28c79f","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ab44bc8-2b02-45ca-a371-84c1d0917d22","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_shadow_man_idle","path":"sprites/spr_shadow_man_idle/spr_shadow_man_idle.yy",},
    "resourceVersion": "1.3",
    "name": "spr_shadow_man_idle",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d41262ae-6ee1-4a9d-99e7-2aeebbd85929","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Shadow-Man",
    "path": "folders/Sprites/BOSS/Shadow-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_shadow_man_idle",
  "tags": [],
  "resourceType": "GMSprite",
}