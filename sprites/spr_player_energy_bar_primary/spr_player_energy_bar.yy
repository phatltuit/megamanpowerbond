{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 11,
  "bbox_top": 0,
  "bbox_bottom": 64,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 12,
  "height": 65,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3a81448f-5807-4a0b-926f-9430163752cc","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3a81448f-5807-4a0b-926f-9430163752cc","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"3a81448f-5807-4a0b-926f-9430163752cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34b35f86-10df-46f2-beba-7c3e27674ba8","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34b35f86-10df-46f2-beba-7c3e27674ba8","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"34b35f86-10df-46f2-beba-7c3e27674ba8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9a8153f6-32e0-459f-8aae-60515fe73b28","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9a8153f6-32e0-459f-8aae-60515fe73b28","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"9a8153f6-32e0-459f-8aae-60515fe73b28","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc16ae38-0e44-465a-838a-b3a8165538e6","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc16ae38-0e44-465a-838a-b3a8165538e6","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"fc16ae38-0e44-465a-838a-b3a8165538e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5090afe0-d10a-4194-becd-7d99a46dd391","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5090afe0-d10a-4194-becd-7d99a46dd391","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"5090afe0-d10a-4194-becd-7d99a46dd391","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5d334d1a-2771-4700-8436-319206872f4b","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5d334d1a-2771-4700-8436-319206872f4b","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"5d334d1a-2771-4700-8436-319206872f4b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41c17b19-643d-4b5a-bf65-59bba3866168","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41c17b19-643d-4b5a-bf65-59bba3866168","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"41c17b19-643d-4b5a-bf65-59bba3866168","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12eb4fc2-c70b-4331-81ec-de06291817a7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12eb4fc2-c70b-4331-81ec-de06291817a7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"12eb4fc2-c70b-4331-81ec-de06291817a7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1bbadb89-14de-4894-992d-813f39c1889d","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1bbadb89-14de-4894-992d-813f39c1889d","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"1bbadb89-14de-4894-992d-813f39c1889d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00264641-9821-450b-bf81-bd95dd95240c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00264641-9821-450b-bf81-bd95dd95240c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"00264641-9821-450b-bf81-bd95dd95240c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e185c44-7727-4528-ad22-a761c3b82a75","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e185c44-7727-4528-ad22-a761c3b82a75","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"4e185c44-7727-4528-ad22-a761c3b82a75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af468741-1186-4ff2-97c4-9581fba62765","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af468741-1186-4ff2-97c4-9581fba62765","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"af468741-1186-4ff2-97c4-9581fba62765","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 28.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ad65f607-87ff-45d4-9410-c02444cd1576","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9776911d-b75f-4b0b-81f4-48e28a0c52be","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a81448f-5807-4a0b-926f-9430163752cc","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"28a1049c-7238-4cf1-9062-f25400b1709d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7083590b-bee6-403d-9ca3-2d16e73d61d9","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34b35f86-10df-46f2-beba-7c3e27674ba8","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9623ef08-61ac-4a55-bdba-5ebe8c134e46","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f60ab592-249c-48ca-8d94-1e51d50c9b5f","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5fdbc02b-2a37-4a90-9796-d42c7c40e287","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9a8153f6-32e0-459f-8aae-60515fe73b28","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9c47eb44-15d1-49da-9193-1081391d7d86","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a2415a89-c619-4338-81ad-21cb2db16f7e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc0cd8b9-3a4c-4ac6-81ab-564d2937a90e","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e49f5b51-96e0-4e16-8091-09c99b76b6f7","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a689853-373b-4236-a19a-434f732537dd","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"011d30b9-585e-45ee-8dde-03c6f3068e2f","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc16ae38-0e44-465a-838a-b3a8165538e6","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"30673c3d-797d-4457-86a5-8db27e8b90f7","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a7dffd1-2bf4-4503-9ed1-7d6ea826c224","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5090afe0-d10a-4194-becd-7d99a46dd391","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f479f08b-e2d7-41a3-9637-23e9a81860b7","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5d334d1a-2771-4700-8436-319206872f4b","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6d61889f-0f28-4b33-bd11-98bac25b5c85","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0bc03d38-6a3d-45c3-b674-a3d1976cd6aa","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7bbbc6ce-6776-4b93-83d0-c919871f96a9","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41c17b19-643d-4b5a-bf65-59bba3866168","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8443420a-0fa2-45b0-b5a1-8849ac33b303","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"040fafb2-5a9c-4af1-a99f-8e21696b990c","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed4f5183-0bee-4117-bcc1-5ad5361ade90","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12eb4fc2-c70b-4331-81ec-de06291817a7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c2b98e9d-7683-47de-8171-af2b2e2946d5","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1bbadb89-14de-4894-992d-813f39c1889d","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a426ba11-476e-456f-a152-749e68ea646c","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00264641-9821-450b-bf81-bd95dd95240c","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"76000fda-bf0b-418b-a927-c09f0793947b","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9b8b40fa-6f54-4d7c-a5a6-9b0ee49f1812","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e185c44-7727-4528-ad22-a761c3b82a75","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"41d42fc8-9456-4e62-865b-0b710939f90b","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af468741-1186-4ff2-97c4-9581fba62765","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d68c3997-9055-45f0-a4c5-f2d00453fb5e","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_energy_bar","path":"sprites/spr_player_energy_bar/spr_player_energy_bar.yy",},
    "resourceVersion": "1.3",
    "name": "spr_player_energy_bar",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "HP-WP-BAR",
    "path": "folders/Sprites/PLAYER/HP-WP-BAR.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_energy_bar",
  "tags": [],
  "resourceType": "GMSprite",
}