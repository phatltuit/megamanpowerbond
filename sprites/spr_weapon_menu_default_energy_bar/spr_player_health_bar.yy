{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 11,
  "bbox_top": 0,
  "bbox_bottom": 64,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 12,
  "height": 65,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3a81448f-5807-4a0b-926f-9430163752cc","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3a81448f-5807-4a0b-926f-9430163752cc","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"3a81448f-5807-4a0b-926f-9430163752cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34b35f86-10df-46f2-beba-7c3e27674ba8","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34b35f86-10df-46f2-beba-7c3e27674ba8","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"34b35f86-10df-46f2-beba-7c3e27674ba8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9a8153f6-32e0-459f-8aae-60515fe73b28","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9a8153f6-32e0-459f-8aae-60515fe73b28","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"9a8153f6-32e0-459f-8aae-60515fe73b28","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc16ae38-0e44-465a-838a-b3a8165538e6","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc16ae38-0e44-465a-838a-b3a8165538e6","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"fc16ae38-0e44-465a-838a-b3a8165538e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5090afe0-d10a-4194-becd-7d99a46dd391","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5090afe0-d10a-4194-becd-7d99a46dd391","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"5090afe0-d10a-4194-becd-7d99a46dd391","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5d334d1a-2771-4700-8436-319206872f4b","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5d334d1a-2771-4700-8436-319206872f4b","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"5d334d1a-2771-4700-8436-319206872f4b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41c17b19-643d-4b5a-bf65-59bba3866168","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41c17b19-643d-4b5a-bf65-59bba3866168","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"41c17b19-643d-4b5a-bf65-59bba3866168","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12eb4fc2-c70b-4331-81ec-de06291817a7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12eb4fc2-c70b-4331-81ec-de06291817a7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"12eb4fc2-c70b-4331-81ec-de06291817a7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1bbadb89-14de-4894-992d-813f39c1889d","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1bbadb89-14de-4894-992d-813f39c1889d","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"1bbadb89-14de-4894-992d-813f39c1889d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00264641-9821-450b-bf81-bd95dd95240c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00264641-9821-450b-bf81-bd95dd95240c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"00264641-9821-450b-bf81-bd95dd95240c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e185c44-7727-4528-ad22-a761c3b82a75","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e185c44-7727-4528-ad22-a761c3b82a75","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"4e185c44-7727-4528-ad22-a761c3b82a75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af468741-1186-4ff2-97c4-9581fba62765","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af468741-1186-4ff2-97c4-9581fba62765","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"af468741-1186-4ff2-97c4-9581fba62765","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"LayerId":{"name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 28.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ec1ee0d4-5579-4863-93cf-fd2d4aa32393","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e5f12293-b7fe-44c9-ae26-95ddbb7f7533","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39af0474-5ba9-4af6-8b3c-bc55b81b5c88","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a81448f-5807-4a0b-926f-9430163752cc","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b48dc8cf-6a99-4468-93d0-29eb3cf4e687","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fe9d394b-b77d-4d14-8faf-872cf391f92e","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4dac34af-6f7c-4a24-97bb-3a6afcb1f0ab","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34b35f86-10df-46f2-beba-7c3e27674ba8","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"292aded3-0149-4275-9953-7fec6e6e888c","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4cdc39f6-e0a6-4fe2-9b43-d982a91e0e80","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42dd25ee-304a-45f0-b37d-e38a0c4496b9","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71bafc5f-a63b-4a6e-91fd-41c38441339e","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a5836219-fbe4-47f0-b73e-4b2416971afd","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9a8153f6-32e0-459f-8aae-60515fe73b28","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42e5b988-df16-43ba-aab1-d03112c3cb72","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3ad3697b-d220-41d0-aee5-ebe3cdb958b5","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"56bf1587-616a-435b-ac1e-a58e7d9e99a8","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3a7aab6c-028d-47b8-9a95-b91a83b2771c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a25fd88-21a0-4743-aa5a-fb11b059e3d7","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3cfa6403-1a89-4dad-add1-a8b52df3d50c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"676e62db-b2e1-4479-9ac7-b8396b16f5e8","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3fe3645f-7b6a-40d1-9dcf-1a9fb2d87ee9","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66b41330-28b0-4e5d-87c8-4d83de85f332","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71efe6cd-63da-4fc6-a89e-ee7498d269d1","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"239c363d-7a4b-47f3-bc44-f82a50ec6d19","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc16ae38-0e44-465a-838a-b3a8165538e6","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6df2e6b-b2d3-4047-aca7-de1d109bd854","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b82be99-374e-4cfc-8c40-d1c768f5984a","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"de435045-4866-4353-92b9-a2367a1bffcf","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5090afe0-d10a-4194-becd-7d99a46dd391","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c747e5ac-d2db-47b0-9760-518e64b57dc6","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5d334d1a-2771-4700-8436-319206872f4b","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"896654de-2ac5-42d8-be89-4c68ad814840","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a8eb48e-f0b3-4b65-a0c1-5c555e0868f9","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"97ca4d57-e892-4308-a061-aa76a7805ee2","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bbd694d4-0f6e-47af-abf3-05246c9fbec2","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2b7fc88f-a09b-4e69-b09c-feabb805dae9","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41c17b19-643d-4b5a-bf65-59bba3866168","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81772427-7ec7-458a-adf8-b98bbdcbff16","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"144a52f8-4874-4ee6-a8c0-2cfda6f51e16","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82a03985-fc9d-47f6-8367-2537c2743c3e","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dfe105e-3451-46bc-8312-5cde7dd0b974","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cbfedd4f-264c-4e28-bf45-77671f7370de","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12eb4fc2-c70b-4331-81ec-de06291817a7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68bc6f89-ac9c-4917-ad19-00a839317e68","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1bbadb89-14de-4894-992d-813f39c1889d","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"09460584-5f21-40fa-8977-db64c4882a30","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00264641-9821-450b-bf81-bd95dd95240c","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"84577591-1170-4c0d-ab3e-d426d0116f92","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"28a5fb70-a13e-48a3-bd01-01ca68dc84de","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b664a056-3d96-4b4b-92d9-9b47cd156a85","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e185c44-7727-4528-ad22-a761c3b82a75","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06f6c839-3ca2-45d7-8e52-344cd0b15936","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af468741-1186-4ff2-97c4-9581fba62765","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6080c56-9b01-497e-851d-cd2bb0703499","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fcba7d5e-60e2-4485-9745-f489e5672ed7","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_player_health_bar","path":"sprites/spr_player_health_bar/spr_player_health_bar.yy",},
    "resourceVersion": "1.3",
    "name": "spr_player_health_bar",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3191b0d9-d1e7-43b5-9317-f4983417f8c7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "HP-WP-BAR",
    "path": "folders/Sprites/PLAYER/HP-WP-BAR.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_player_health_bar",
  "tags": [],
  "resourceType": "GMSprite",
}