{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 7,
  "bbox_top": 0,
  "bbox_bottom": 7,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 8,
  "height": 8,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f611f89d-bd4b-4bab-a64f-48192cad6733","path":"sprites/spr_dust/spr_dust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f611f89d-bd4b-4bab-a64f-48192cad6733","path":"sprites/spr_dust/spr_dust.yy",},"LayerId":{"name":"7467ba71-0b8a-46dd-b001-a51b4c31787a","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dust","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","name":"f611f89d-bd4b-4bab-a64f-48192cad6733","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"85553fc2-1e9a-423b-9b17-a9bbf6cb192f","path":"sprites/spr_dust/spr_dust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"85553fc2-1e9a-423b-9b17-a9bbf6cb192f","path":"sprites/spr_dust/spr_dust.yy",},"LayerId":{"name":"7467ba71-0b8a-46dd-b001-a51b4c31787a","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dust","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","name":"85553fc2-1e9a-423b-9b17-a9bbf6cb192f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"76ce49ef-c52d-4727-9f1f-2b23acbbf7c6","path":"sprites/spr_dust/spr_dust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"76ce49ef-c52d-4727-9f1f-2b23acbbf7c6","path":"sprites/spr_dust/spr_dust.yy",},"LayerId":{"name":"7467ba71-0b8a-46dd-b001-a51b4c31787a","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dust","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","name":"76ce49ef-c52d-4727-9f1f-2b23acbbf7c6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_dust","path":"sprites/spr_dust/spr_dust.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"51e20d6c-9edc-437b-a080-0efeab5d5ba1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f611f89d-bd4b-4bab-a64f-48192cad6733","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"764142a3-a1eb-4d0b-b335-29b692b5993e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"85553fc2-1e9a-423b-9b17-a9bbf6cb192f","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5c3c6d94-8dbd-4413-af7a-51273d8ae63e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"76ce49ef-c52d-4727-9f1f-2b23acbbf7c6","path":"sprites/spr_dust/spr_dust.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 7,
    "yorigin": 7,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_dust","path":"sprites/spr_dust/spr_dust.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"7467ba71-0b8a-46dd-b001-a51b4c31787a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "EFFECT",
    "path": "folders/Sprites/PLAYER/EFFECT.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_dust",
  "tags": [],
  "resourceType": "GMSprite",
}