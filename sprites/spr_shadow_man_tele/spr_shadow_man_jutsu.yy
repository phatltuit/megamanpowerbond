{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 25,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "width": 26,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"44431309-7a40-4dad-a32a-d1855d095d17","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"44431309-7a40-4dad-a32a-d1855d095d17","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":{"name":"5f8b3993-000f-4112-a485-883d2bbc7171","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"44431309-7a40-4dad-a32a-d1855d095d17","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bbd49b33-e148-4375-83dd-869f4794b266","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bbd49b33-e148-4375-83dd-869f4794b266","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":{"name":"5f8b3993-000f-4112-a485-883d2bbc7171","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"bbd49b33-e148-4375-83dd-869f4794b266","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c779cbb4-b9e1-4167-96b6-c59164ccea3a","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c779cbb4-b9e1-4167-96b6-c59164ccea3a","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":{"name":"5f8b3993-000f-4112-a485-883d2bbc7171","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"c779cbb4-b9e1-4167-96b6-c59164ccea3a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b7971bd-26ef-4cf8-88c5-5ebab0872776","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b7971bd-26ef-4cf8-88c5-5ebab0872776","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":{"name":"5f8b3993-000f-4112-a485-883d2bbc7171","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"7b7971bd-26ef-4cf8-88c5-5ebab0872776","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"92a13e60-1deb-497f-b084-4cbea28c7c07","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92a13e60-1deb-497f-b084-4cbea28c7c07","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":{"name":"5f8b3993-000f-4112-a485-883d2bbc7171","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"92a13e60-1deb-497f-b084-4cbea28c7c07","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ce8ddba-0926-40a6-b45c-1f77c03f6051","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ce8ddba-0926-40a6-b45c-1f77c03f6051","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"LayerId":{"name":"5f8b3993-000f-4112-a485-883d2bbc7171","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","name":"8ce8ddba-0926-40a6-b45c-1f77c03f6051","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a1b4a763-8569-46af-9795-8badd8b13a72","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"44431309-7a40-4dad-a32a-d1855d095d17","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e672589f-369c-4c0c-92ea-ce7b2f4ad0c1","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bbd49b33-e148-4375-83dd-869f4794b266","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9fa0069a-e40a-4b2d-a888-29ad65a2f72b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c779cbb4-b9e1-4167-96b6-c59164ccea3a","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0fafe1db-b1c8-458c-a10b-85f7e6ee9a59","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b7971bd-26ef-4cf8-88c5-5ebab0872776","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8749b6d1-29eb-4016-aa3b-0dda4f00f537","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92a13e60-1deb-497f-b084-4cbea28c7c07","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1ad7272a-78f0-491a-a4dc-e43676fb8d81","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ce8ddba-0926-40a6-b45c-1f77c03f6051","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 13,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_shadow_man_jutsu","path":"sprites/spr_shadow_man_jutsu/spr_shadow_man_jutsu.yy",},
    "resourceVersion": "1.3",
    "name": "spr_shadow_man_jutsu",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5f8b3993-000f-4112-a485-883d2bbc7171","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Shadow-Man",
    "path": "folders/Sprites/BOSS/Shadow-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_shadow_man_jutsu",
  "tags": [],
  "resourceType": "GMSprite",
}