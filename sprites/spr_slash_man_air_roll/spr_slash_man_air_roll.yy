{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 32,
  "bbox_top": 5,
  "bbox_bottom": 33,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 36,
  "height": 39,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"729c97e8-fda7-47e8-a2c4-fac3acd56ea8","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"729c97e8-fda7-47e8-a2c4-fac3acd56ea8","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":{"name":"a9818307-6e84-44ea-a49f-77fb2ea5c40e","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_air_roll","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"729c97e8-fda7-47e8-a2c4-fac3acd56ea8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c7d8a0e-204f-4d69-83cd-1925f595a7f3","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c7d8a0e-204f-4d69-83cd-1925f595a7f3","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":{"name":"a9818307-6e84-44ea-a49f-77fb2ea5c40e","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_air_roll","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"3c7d8a0e-204f-4d69-83cd-1925f595a7f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00b975b4-2165-4226-bc75-23bab6c185ff","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00b975b4-2165-4226-bc75-23bab6c185ff","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":{"name":"a9818307-6e84-44ea-a49f-77fb2ea5c40e","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_air_roll","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"00b975b4-2165-4226-bc75-23bab6c185ff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2a150177-db23-4c01-b778-6ef21c70e86c","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2a150177-db23-4c01-b778-6ef21c70e86c","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"LayerId":{"name":"a9818307-6e84-44ea-a49f-77fb2ea5c40e","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_air_roll","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","name":"2a150177-db23-4c01-b778-6ef21c70e86c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_slash_man_air_roll","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fdb2d6a0-db9a-435a-823e-fa24bd322bca","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"729c97e8-fda7-47e8-a2c4-fac3acd56ea8","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0c7248d-4da1-4ac1-9b96-0475c01d08e3","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c7d8a0e-204f-4d69-83cd-1925f595a7f3","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f9d76c29-e195-40fc-9b84-7f819dca4e94","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00b975b4-2165-4226-bc75-23bab6c185ff","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc38507b-097c-46e4-8324-84e6f3ba2805","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a150177-db23-4c01-b778-6ef21c70e86c","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 18,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_slash_man_air_roll","path":"sprites/spr_slash_man_air_roll/spr_slash_man_air_roll.yy",},
    "resourceVersion": "1.3",
    "name": "spr_slash_man_air_roll",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a9818307-6e84-44ea-a49f-77fb2ea5c40e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Slash-Man",
    "path": "folders/Sprites/BOSS/Slash-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_slash_man_air_roll",
  "tags": [],
  "resourceType": "GMSprite",
}