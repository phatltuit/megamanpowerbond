{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 3,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"068b32c5-ebdd-4935-9b17-0e8258f2f331","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"068b32c5-ebdd-4935-9b17-0e8258f2f331","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},"LayerId":{"name":"d4138bc0-12e9-4c94-b36c-6325869895d9","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_bass_buster","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},"resourceVersion":"1.0","name":"068b32c5-ebdd-4935-9b17-0e8258f2f331","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_weapon_bass_buster","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7d7a671f-f382-4348-af9a-3fcf1f31ce7c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"068b32c5-ebdd-4935-9b17-0e8258f2f331","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_weapon_bass_buster","path":"sprites/spr_weapon_bass_buster/spr_weapon_bass_buster.yy",},
    "resourceVersion": "1.3",
    "name": "spr_weapon_bass_buster",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d4138bc0-12e9-4c94-b36c-6325869895d9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Menu",
    "path": "folders/Sprites/PLAYER/WEAPON/Menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_weapon_bass_buster",
  "tags": [],
  "resourceType": "GMSprite",
}