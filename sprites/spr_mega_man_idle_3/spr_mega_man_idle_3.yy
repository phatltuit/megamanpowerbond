{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 19,
  "bbox_top": 3,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 25,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"39d6e50e-43d0-4f0d-b33d-9740bd304b73","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"39d6e50e-43d0-4f0d-b33d-9740bd304b73","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},"LayerId":{"name":"b10217e0-fdf7-49de-b03d-90d0e67ce965","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_idle_3","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},"resourceVersion":"1.0","name":"39d6e50e-43d0-4f0d-b33d-9740bd304b73","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mega_man_idle_3","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"af63689e-b4ed-4fa0-8da6-8f4dd7763e51","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39d6e50e-43d0-4f0d-b33d-9740bd304b73","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mega_man_idle_3","path":"sprites/spr_mega_man_idle_3/spr_mega_man_idle_3.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b10217e0-fdf7-49de-b03d-90d0e67ce965","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Outline",
    "path": "folders/Sprites/PLAYER/MEGAMAN/Outline.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mega_man_idle_3",
  "tags": [],
  "resourceType": "GMSprite",
}