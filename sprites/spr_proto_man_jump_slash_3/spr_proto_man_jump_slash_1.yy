{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 60,
  "bbox_top": 0,
  "bbox_bottom": 43,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 46,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7c386408-fb47-43fa-bfa0-e7d740e3ec88","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7c386408-fb47-43fa-bfa0-e7d740e3ec88","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":{"name":"9c3399ac-5cbe-4ef6-8b87-00edcaf27985","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"7c386408-fb47-43fa-bfa0-e7d740e3ec88","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"17ab2dc3-ef54-486d-a655-c71c082396a1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"17ab2dc3-ef54-486d-a655-c71c082396a1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":{"name":"9c3399ac-5cbe-4ef6-8b87-00edcaf27985","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"17ab2dc3-ef54-486d-a655-c71c082396a1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"492f471a-6a0c-4785-97e1-c78b70007d03","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"492f471a-6a0c-4785-97e1-c78b70007d03","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":{"name":"9c3399ac-5cbe-4ef6-8b87-00edcaf27985","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"492f471a-6a0c-4785-97e1-c78b70007d03","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4bf8ba82-789b-4fd5-bc20-eda8ec9a8197","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4bf8ba82-789b-4fd5-bc20-eda8ec9a8197","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":{"name":"9c3399ac-5cbe-4ef6-8b87-00edcaf27985","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"4bf8ba82-789b-4fd5-bc20-eda8ec9a8197","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41979d6d-bd47-4cd8-86bd-c4e32dbe433c","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41979d6d-bd47-4cd8-86bd-c4e32dbe433c","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"LayerId":{"name":"9c3399ac-5cbe-4ef6-8b87-00edcaf27985","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","name":"41979d6d-bd47-4cd8-86bd-c4e32dbe433c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"880e9cd7-8688-4fa2-8781-b7d7191d2cd2","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7c386408-fb47-43fa-bfa0-e7d740e3ec88","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eea52177-06ce-4a8a-9da1-d55cae4b07cc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17ab2dc3-ef54-486d-a655-c71c082396a1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"36fcbd55-4f66-4252-acb7-5c906ffb44b8","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"492f471a-6a0c-4785-97e1-c78b70007d03","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d5b774d-a161-4597-a49c-8fa006f6a94e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4bf8ba82-789b-4fd5-bc20-eda8ec9a8197","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f5040d1-7b85-41ae-8866-3808d9c26f6a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41979d6d-bd47-4cd8-86bd-c4e32dbe433c","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 21,
    "yorigin": 20,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_man_jump_slash_1","path":"sprites/spr_proto_man_jump_slash_1/spr_proto_man_jump_slash_1.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_man_jump_slash_1",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9c3399ac-5cbe-4ef6-8b87-00edcaf27985","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Primary",
    "path": "folders/Sprites/PLAYER/PROTOMAN/Primary.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_man_jump_slash_1",
  "tags": [],
  "resourceType": "GMSprite",
}