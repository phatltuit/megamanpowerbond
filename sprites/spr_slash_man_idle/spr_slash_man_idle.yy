{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 38,
  "bbox_top": 10,
  "bbox_bottom": 38,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 40,
  "height": 39,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"597642e2-1a17-49fe-85d3-6ea213d1ea3f","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"597642e2-1a17-49fe-85d3-6ea213d1ea3f","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"597642e2-1a17-49fe-85d3-6ea213d1ea3f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2eda6162-43ec-405b-9a7e-73373670ccf2","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2eda6162-43ec-405b-9a7e-73373670ccf2","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"2eda6162-43ec-405b-9a7e-73373670ccf2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4648b585-9cd5-4de8-8d5d-2edb3c82823e","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4648b585-9cd5-4de8-8d5d-2edb3c82823e","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"4648b585-9cd5-4de8-8d5d-2edb3c82823e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8fa86bd-b630-4df7-8392-13ca2c365525","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8fa86bd-b630-4df7-8392-13ca2c365525","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"a8fa86bd-b630-4df7-8392-13ca2c365525","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a54845f4-c333-4a65-892c-f9de87bbab26","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a54845f4-c333-4a65-892c-f9de87bbab26","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"a54845f4-c333-4a65-892c-f9de87bbab26","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"79285444-7fa4-4b6a-81b7-ab75adab3c41","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"79285444-7fa4-4b6a-81b7-ab75adab3c41","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"79285444-7fa4-4b6a-81b7-ab75adab3c41","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f3dd6d9-d489-4bfd-96ff-fb2960eef6d3","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f3dd6d9-d489-4bfd-96ff-fb2960eef6d3","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"4f3dd6d9-d489-4bfd-96ff-fb2960eef6d3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5899e2d2-3f6e-4cd2-b36e-7c5a98bfd78f","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5899e2d2-3f6e-4cd2-b36e-7c5a98bfd78f","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"LayerId":{"name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","name":"5899e2d2-3f6e-4cd2-b36e-7c5a98bfd78f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"15faf631-17c8-4135-8763-4d95ff9e4cbf","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"597642e2-1a17-49fe-85d3-6ea213d1ea3f","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbc8066e-2297-41ee-ae56-d906ab0c02c7","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2eda6162-43ec-405b-9a7e-73373670ccf2","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0512b60f-a9ed-4852-b34f-e0b1cbe6704d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4648b585-9cd5-4de8-8d5d-2edb3c82823e","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6aec481f-1b83-4b5b-b912-6d018a29bb20","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8fa86bd-b630-4df7-8392-13ca2c365525","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3fd0426a-2317-46a7-8671-5bcb2bb2acd7","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a54845f4-c333-4a65-892c-f9de87bbab26","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0adeb6c-7e65-49a9-a28f-5cbeaf7238f2","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"79285444-7fa4-4b6a-81b7-ab75adab3c41","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b390595c-5e7e-4c81-969c-eea8ce467e30","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f3dd6d9-d489-4bfd-96ff-fb2960eef6d3","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ce7811c-abbb-4dc6-a07c-1133f15c2ed3","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5899e2d2-3f6e-4cd2-b36e-7c5a98bfd78f","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 17,
    "yorigin": 30,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_slash_man_idle","path":"sprites/spr_slash_man_idle/spr_slash_man_idle.yy",},
    "resourceVersion": "1.3",
    "name": "spr_slash_man_idle",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"26a9b4db-10f3-4949-a15a-cbe0eaa5ba19","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Slash-Man",
    "path": "folders/Sprites/BOSS/Slash-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_slash_man_idle",
  "tags": [],
  "resourceType": "GMSprite",
}