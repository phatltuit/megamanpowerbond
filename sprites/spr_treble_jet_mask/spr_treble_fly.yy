{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 34,
  "bbox_top": 0,
  "bbox_bottom": 16,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 35,
  "height": 19,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4c04a1a9-59af-48e4-b4c0-c94cf855fe35","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4c04a1a9-59af-48e4-b4c0-c94cf855fe35","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"LayerId":{"name":"7c60a0a2-9ee3-44b4-8d53-f993ff0a9c3b","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_treble_fly","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"resourceVersion":"1.0","name":"4c04a1a9-59af-48e4-b4c0-c94cf855fe35","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0db66dc7-709d-4fdd-8c94-f726633e28bb","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0db66dc7-709d-4fdd-8c94-f726633e28bb","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"LayerId":{"name":"7c60a0a2-9ee3-44b4-8d53-f993ff0a9c3b","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_treble_fly","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"resourceVersion":"1.0","name":"0db66dc7-709d-4fdd-8c94-f726633e28bb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_treble_fly","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d67a296e-50d6-440a-a5fd-ae16ae020a25","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4c04a1a9-59af-48e4-b4c0-c94cf855fe35","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f82eef69-8c4c-4f39-b1fc-bcd34bd7393e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0db66dc7-709d-4fdd-8c94-f726633e28bb","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 17,
    "yorigin": 10,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_treble_fly","path":"sprites/spr_treble_fly/spr_treble_fly.yy",},
    "resourceVersion": "1.3",
    "name": "spr_treble_fly",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"7c60a0a2-9ee3-44b4-8d53-f993ff0a9c3b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Treble",
    "path": "folders/Sprites/PLAYER/SUPPORT/Treble.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_treble_fly",
  "tags": [],
  "resourceType": "GMSprite",
}