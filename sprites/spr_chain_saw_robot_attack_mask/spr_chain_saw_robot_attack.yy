{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 65,
  "bbox_top": 0,
  "bbox_bottom": 46,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 66,
  "height": 47,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ab873a88-5970-40bb-86e4-0ced67f3e1e6","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab873a88-5970-40bb-86e4-0ced67f3e1e6","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"LayerId":{"name":"5e56e967-142f-409c-a4ad-c179a25d1275","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chain_saw_robot_attack","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"resourceVersion":"1.0","name":"ab873a88-5970-40bb-86e4-0ced67f3e1e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ac0d88d-84d5-4011-9e2e-4699ce548be6","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ac0d88d-84d5-4011-9e2e-4699ce548be6","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"LayerId":{"name":"5e56e967-142f-409c-a4ad-c179a25d1275","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_chain_saw_robot_attack","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"resourceVersion":"1.0","name":"1ac0d88d-84d5-4011-9e2e-4699ce548be6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_chain_saw_robot_attack","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f3983b4a-eec4-43f5-b54f-2bdc85cc1a60","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab873a88-5970-40bb-86e4-0ced67f3e1e6","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8528853d-47f0-4d80-a88d-5ab5d51f67bc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ac0d88d-84d5-4011-9e2e-4699ce548be6","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 24,
    "yorigin": 38,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_chain_saw_robot_attack","path":"sprites/spr_chain_saw_robot_attack/spr_chain_saw_robot_attack.yy",},
    "resourceVersion": "1.3",
    "name": "spr_chain_saw_robot_attack",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5e56e967-142f-409c-a4ad-c179a25d1275","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ChainSawRobot",
    "path": "folders/Sprites/ENEMIES/ChainSawRobot.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_chain_saw_robot_attack",
  "tags": [],
  "resourceType": "GMSprite",
}