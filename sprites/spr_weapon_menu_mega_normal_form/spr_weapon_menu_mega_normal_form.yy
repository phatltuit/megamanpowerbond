{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 19,
  "bbox_top": 3,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 25,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"cc8f097d-4887-4410-a3dc-6fa8123f4958","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc8f097d-4887-4410-a3dc-6fa8123f4958","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},"LayerId":{"name":"fa2a65c5-c9fa-4369-8081-4b8dd92dc813","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_menu_mega_normal_form","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},"resourceVersion":"1.0","name":"cc8f097d-4887-4410-a3dc-6fa8123f4958","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_weapon_menu_mega_normal_form","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e368adf4-dd58-45a3-8d98-ad40cdfc83c1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc8f097d-4887-4410-a3dc-6fa8123f4958","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_weapon_menu_mega_normal_form","path":"sprites/spr_weapon_menu_mega_normal_form/spr_weapon_menu_mega_normal_form.yy",},
    "resourceVersion": "1.3",
    "name": "spr_weapon_menu_mega_normal_form",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"fa2a65c5-c9fa-4369-8081-4b8dd92dc813","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Menu",
    "path": "folders/Sprites/PLAYER/WEAPON/Menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_weapon_menu_mega_normal_form",
  "tags": [],
  "resourceType": "GMSprite",
}