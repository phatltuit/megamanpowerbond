{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 49,
  "bbox_top": 3,
  "bbox_bottom": 43,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 50,
  "height": 44,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bb1f9f50-f6ca-4b5e-a3ea-7207ef29caa4","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bb1f9f50-f6ca-4b5e-a3ea-7207ef29caa4","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"LayerId":{"name":"871f6d15-b024-4393-87e7-adb2449bcb7f","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_splash_woman_sing","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"resourceVersion":"1.0","name":"bb1f9f50-f6ca-4b5e-a3ea-7207ef29caa4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5b466228-783b-49bc-9fe3-7b83866ffadc","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5b466228-783b-49bc-9fe3-7b83866ffadc","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"LayerId":{"name":"871f6d15-b024-4393-87e7-adb2449bcb7f","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_splash_woman_sing","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"resourceVersion":"1.0","name":"5b466228-783b-49bc-9fe3-7b83866ffadc","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_splash_woman_sing","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 5.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"809f95ba-3a1b-4370-a13f-c7c95af1eb60","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bb1f9f50-f6ca-4b5e-a3ea-7207ef29caa4","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"171a8b2b-3b79-41fe-90cd-9bf3d3b5f670","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5b466228-783b-49bc-9fe3-7b83866ffadc","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_splash_woman_sing","path":"sprites/spr_splash_woman_sing/spr_splash_woman_sing.yy",},
    "resourceVersion": "1.3",
    "name": "spr_splash_woman_sing",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"871f6d15-b024-4393-87e7-adb2449bcb7f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Splash_Woman",
    "path": "folders/Sprites/BOSS/Splash_Woman.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_splash_woman_sing",
  "tags": [],
  "resourceType": "GMSprite",
}