{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 6,
  "bbox_right": 21,
  "bbox_top": 3,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6edd899e-6f30-456a-8f18-095c0ca894a1","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6edd899e-6f30-456a-8f18-095c0ca894a1","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":{"name":"76736a3c-0954-4025-9bdc-b87fe84df9a2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run_2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"6edd899e-6f30-456a-8f18-095c0ca894a1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a04031bf-860e-4747-b1cb-b06e8603611f","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a04031bf-860e-4747-b1cb-b06e8603611f","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":{"name":"76736a3c-0954-4025-9bdc-b87fe84df9a2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run_2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"a04031bf-860e-4747-b1cb-b06e8603611f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b07a3e6e-cfc8-4035-a5a5-c1b81a2d3435","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b07a3e6e-cfc8-4035-a5a5-c1b81a2d3435","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":{"name":"76736a3c-0954-4025-9bdc-b87fe84df9a2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run_2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"b07a3e6e-cfc8-4035-a5a5-c1b81a2d3435","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c3662704-bf44-469b-8b3f-8ab74f44fec1","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c3662704-bf44-469b-8b3f-8ab74f44fec1","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"LayerId":{"name":"76736a3c-0954-4025-9bdc-b87fe84df9a2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run_2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","name":"c3662704-bf44-469b-8b3f-8ab74f44fec1","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mega_man_shoot_run_2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4ac2d887-325a-4bd7-ada0-4e0d3c399627","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6edd899e-6f30-456a-8f18-095c0ca894a1","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8d75c79b-fc5f-404d-811e-7b48a77418ed","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a04031bf-860e-4747-b1cb-b06e8603611f","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa6f1703-00e0-43da-ab9e-f479514abbd4","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b07a3e6e-cfc8-4035-a5a5-c1b81a2d3435","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1fed7aab-3bd3-4f86-b437-16d34a30ee96","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c3662704-bf44-469b-8b3f-8ab74f44fec1","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 11,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mega_man_shoot_run_2","path":"sprites/spr_mega_man_shoot_run_2/spr_mega_man_shoot_run_2.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"76736a3c-0954-4025-9bdc-b87fe84df9a2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Primary",
    "path": "folders/Sprites/PLAYER/MEGAMAN/Primary.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mega_man_shoot_run_2",
  "tags": [],
  "resourceType": "GMSprite",
}