{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 223,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 224,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"60558ebc-ebfd-42f0-a0d8-29fc9a8f10fe","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"60558ebc-ebfd-42f0-a0d8-29fc9a8f10fe","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"60558ebc-ebfd-42f0-a0d8-29fc9a8f10fe","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd3fc82d-78d0-4bfc-b714-56d33d2ffd57","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd3fc82d-78d0-4bfc-b714-56d33d2ffd57","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"dd3fc82d-78d0-4bfc-b714-56d33d2ffd57","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ab89b8e-8acf-49fb-afa4-1724c290a354","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ab89b8e-8acf-49fb-afa4-1724c290a354","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"1ab89b8e-8acf-49fb-afa4-1724c290a354","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62a05ff6-8c63-45d0-abb0-964d2cb38cf7","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62a05ff6-8c63-45d0-abb0-964d2cb38cf7","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"62a05ff6-8c63-45d0-abb0-964d2cb38cf7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5e639895-be24-42ec-ba1f-05022f45ba76","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5e639895-be24-42ec-ba1f-05022f45ba76","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"5e639895-be24-42ec-ba1f-05022f45ba76","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"33bba990-9001-432b-ae5a-1b350003495b","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"33bba990-9001-432b-ae5a-1b350003495b","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"33bba990-9001-432b-ae5a-1b350003495b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5a2a5862-799d-41dd-a93d-6a11348522d2","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5a2a5862-799d-41dd-a93d-6a11348522d2","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"5a2a5862-799d-41dd-a93d-6a11348522d2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6d79c2bd-6f84-45ed-a286-cf3b17e3b4ff","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6d79c2bd-6f84-45ed-a286-cf3b17e3b4ff","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"6d79c2bd-6f84-45ed-a286-cf3b17e3b4ff","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ab6db83-d04a-4e9a-8da3-cc55210c94bc","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ab6db83-d04a-4e9a-8da3-cc55210c94bc","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"4ab6db83-d04a-4e9a-8da3-cc55210c94bc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c6cf2c17-5194-4418-b1b8-6fa292b82296","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6cf2c17-5194-4418-b1b8-6fa292b82296","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"c6cf2c17-5194-4418-b1b8-6fa292b82296","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8f3367d3-3769-4494-941f-eeaaca9ef6f8","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8f3367d3-3769-4494-941f-eeaaca9ef6f8","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"8f3367d3-3769-4494-941f-eeaaca9ef6f8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a2530707-7ac2-47d0-9f6c-b6e538c35417","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a2530707-7ac2-47d0-9f6c-b6e538c35417","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"a2530707-7ac2-47d0-9f6c-b6e538c35417","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"529c9a37-9271-4994-b4be-d10e1e741595","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"529c9a37-9271-4994-b4be-d10e1e741595","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"529c9a37-9271-4994-b4be-d10e1e741595","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f0bf7c9-961a-4769-80a1-7f540e78413b","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f0bf7c9-961a-4769-80a1-7f540e78413b","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"4f0bf7c9-961a-4769-80a1-7f540e78413b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70a8ddff-23b8-4b79-ac90-379861807471","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70a8ddff-23b8-4b79-ac90-379861807471","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"70a8ddff-23b8-4b79-ac90-379861807471","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"30b312e6-be44-48b5-88c1-205912594947","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"30b312e6-be44-48b5-88c1-205912594947","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"30b312e6-be44-48b5-88c1-205912594947","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af8dbc1b-34b4-4d5d-8a04-3ab2477ad156","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af8dbc1b-34b4-4d5d-8a04-3ab2477ad156","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"af8dbc1b-34b4-4d5d-8a04-3ab2477ad156","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"26b7090d-0d99-4837-a7b7-bdf33c541a39","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"26b7090d-0d99-4837-a7b7-bdf33c541a39","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"26b7090d-0d99-4837-a7b7-bdf33c541a39","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"78ad3963-ff67-40d7-8fcc-bf465ee93dab","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78ad3963-ff67-40d7-8fcc-bf465ee93dab","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"78ad3963-ff67-40d7-8fcc-bf465ee93dab","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"163201d6-8911-48d3-990e-4ac5e0595af7","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"163201d6-8911-48d3-990e-4ac5e0595af7","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"163201d6-8911-48d3-990e-4ac5e0595af7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e515db80-8235-4a13-964b-00b150f76102","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e515db80-8235-4a13-964b-00b150f76102","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"e515db80-8235-4a13-964b-00b150f76102","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a3e2d9f0-5afe-4a19-8a52-572de792d27e","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a3e2d9f0-5afe-4a19-8a52-572de792d27e","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"a3e2d9f0-5afe-4a19-8a52-572de792d27e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"136984c8-595b-411d-b089-0f4da256b324","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"136984c8-595b-411d-b089-0f4da256b324","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"136984c8-595b-411d-b089-0f4da256b324","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9fe69b2-a2f6-4965-be16-e002cb83cd9f","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9fe69b2-a2f6-4965-be16-e002cb83cd9f","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"d9fe69b2-a2f6-4965-be16-e002cb83cd9f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7f9dbe23-3827-4e95-bdff-7a014367e754","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7f9dbe23-3827-4e95-bdff-7a014367e754","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"7f9dbe23-3827-4e95-bdff-7a014367e754","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"765b1773-6921-438e-9b30-cf59dcf04647","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"765b1773-6921-438e-9b30-cf59dcf04647","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"765b1773-6921-438e-9b30-cf59dcf04647","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"413edaa3-5c19-4cef-accc-5ead3ec7fb2e","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"413edaa3-5c19-4cef-accc-5ead3ec7fb2e","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"413edaa3-5c19-4cef-accc-5ead3ec7fb2e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d3297a70-fcde-4484-af61-9ae6f9945631","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d3297a70-fcde-4484-af61-9ae6f9945631","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"LayerId":{"name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","name":"d3297a70-fcde-4484-af61-9ae6f9945631","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 28.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"74f312cc-7a1f-454b-b19c-33386d2a0824","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"60558ebc-ebfd-42f0-a0d8-29fc9a8f10fe","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e25e3e7-44f5-4419-a7b1-3f10912fa6d0","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd3fc82d-78d0-4bfc-b714-56d33d2ffd57","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d0e79d2-730b-44e7-a5b7-539e9b9ce071","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ab89b8e-8acf-49fb-afa4-1724c290a354","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6d198739-96c5-4be8-996b-9b6ea587ad39","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62a05ff6-8c63-45d0-abb0-964d2cb38cf7","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5dde9231-c483-4be3-9769-22b6fffdf6d8","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5e639895-be24-42ec-ba1f-05022f45ba76","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31415b9d-904b-4824-9336-386de5c032f6","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"33bba990-9001-432b-ae5a-1b350003495b","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b4de5e72-85a4-4de7-b480-dba3ccf4bb1a","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5a2a5862-799d-41dd-a93d-6a11348522d2","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f0f731fd-6b75-44ca-8a5f-c944948ca8fb","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6d79c2bd-6f84-45ed-a286-cf3b17e3b4ff","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9242e45-6132-44f8-8d77-ea4858fbfb34","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ab6db83-d04a-4e9a-8da3-cc55210c94bc","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06b7d185-57a5-4179-9bad-347aff25896b","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6cf2c17-5194-4418-b1b8-6fa292b82296","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"79a9a611-45d7-4359-9e72-ddc71269de76","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8f3367d3-3769-4494-941f-eeaaca9ef6f8","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"810dff5a-5d2f-425f-9fdc-eecc54af4e2c","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a2530707-7ac2-47d0-9f6c-b6e538c35417","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"99a86f64-982f-43dc-a36c-d33e9afb0c6e","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"529c9a37-9271-4994-b4be-d10e1e741595","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"824f851e-8be0-4483-a8fb-fc7d33195dad","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f0bf7c9-961a-4769-80a1-7f540e78413b","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c9cf987-6a15-4473-98d0-666eb00de28c","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70a8ddff-23b8-4b79-ac90-379861807471","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e4e9061c-35aa-4e4e-b9e9-77bb3a8cfbed","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30b312e6-be44-48b5-88c1-205912594947","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c994091a-195a-4170-9fec-429d80d23ea1","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af8dbc1b-34b4-4d5d-8a04-3ab2477ad156","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68088b1b-bce4-4b30-923c-e3ad143476fd","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"26b7090d-0d99-4837-a7b7-bdf33c541a39","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fbf41754-2d95-4ac0-b60c-a9d13e5f9d89","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78ad3963-ff67-40d7-8fcc-bf465ee93dab","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3cb8be8d-d816-4989-87e9-0d0693f32b39","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"163201d6-8911-48d3-990e-4ac5e0595af7","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f0acbd5-3265-4f07-b444-e4eb1fdf8b11","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e515db80-8235-4a13-964b-00b150f76102","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0fd33af7-3f54-4631-8e5b-1b4b83776814","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a3e2d9f0-5afe-4a19-8a52-572de792d27e","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e2a65712-42fb-4777-bd66-6a9b77cdb47c","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"136984c8-595b-411d-b089-0f4da256b324","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c33b912e-28b6-474e-b307-21dced7196b5","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9fe69b2-a2f6-4965-be16-e002cb83cd9f","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0f82ce74-b7ad-4dc9-9c43-23dbaed9761e","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7f9dbe23-3827-4e95-bdff-7a014367e754","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa8d8ab8-85db-4b89-b410-4d1ab98c8aa4","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"765b1773-6921-438e-9b30-cf59dcf04647","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e06d301-4296-42e9-a809-3e5a4fdb685f","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"413edaa3-5c19-4cef-accc-5ead3ec7fb2e","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13632a56-6b71-4345-834a-8dc081c0b5b8","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d3297a70-fcde-4484-af61-9ae6f9945631","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bg_36_lightning","path":"sprites/spr_bg_36_lightning/spr_bg_36_lightning.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bg_36_lightning",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4d0be7e8-fb77-453f-92a8-2c21cc2288f5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "BACKGROUND",
    "path": "folders/Sprites/BACKGROUND.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bg_36_lightning",
  "tags": [],
  "resourceType": "GMSprite",
}