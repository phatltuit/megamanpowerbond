{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 6,
  "bbox_right": 21,
  "bbox_top": 3,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"431e167b-e617-48be-89fa-722834416d64","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"431e167b-e617-48be-89fa-722834416d64","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":{"name":"2c81d545-c846-4e99-a392-a0a4d467c2a3","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"431e167b-e617-48be-89fa-722834416d64","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"227b4040-b64d-4573-b0a6-43ead0410167","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"227b4040-b64d-4573-b0a6-43ead0410167","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":{"name":"2c81d545-c846-4e99-a392-a0a4d467c2a3","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"227b4040-b64d-4573-b0a6-43ead0410167","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6cde7bad-baf2-4fe8-a067-855a95be74a6","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6cde7bad-baf2-4fe8-a067-855a95be74a6","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":{"name":"2c81d545-c846-4e99-a392-a0a4d467c2a3","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"6cde7bad-baf2-4fe8-a067-855a95be74a6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5d837945-4b63-4ba2-89f6-287020ed87ab","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5d837945-4b63-4ba2-89f6-287020ed87ab","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"LayerId":{"name":"2c81d545-c846-4e99-a392-a0a4d467c2a3","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_mega_man_shoot_run","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","name":"5d837945-4b63-4ba2-89f6-287020ed87ab","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_mega_man_shoot_run","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b91e3ff1-ed0a-4c3f-ad14-f356b7db7753","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"431e167b-e617-48be-89fa-722834416d64","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0a6492d7-ad33-4be9-a46d-82476acd85a7","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"227b4040-b64d-4573-b0a6-43ead0410167","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c921e753-e577-4bca-b3d3-390598d771db","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6cde7bad-baf2-4fe8-a067-855a95be74a6","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c3da6558-6fc2-4643-8d9d-a869fc1e1be1","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5d837945-4b63-4ba2-89f6-287020ed87ab","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 11,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_mega_man_shoot_run","path":"sprites/spr_mega_man_shoot_run/spr_mega_man_shoot_run.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2c81d545-c846-4e99-a392-a0a4d467c2a3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Normal",
    "path": "folders/Sprites/PLAYER/MEGAMAN/Normal.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_mega_man_shoot_run",
  "tags": [],
  "resourceType": "GMSprite",
}