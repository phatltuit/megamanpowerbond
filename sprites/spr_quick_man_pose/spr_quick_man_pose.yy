{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 28,
  "bbox_top": 0,
  "bbox_bottom": 30,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 29,
  "height": 31,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2ae7e1f2-7e8a-4cd5-984e-365a4e8aec6b","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ae7e1f2-7e8a-4cd5-984e-365a4e8aec6b","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"2ae7e1f2-7e8a-4cd5-984e-365a4e8aec6b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fbd4d133-778a-42ee-b0bd-51ea8ca703e2","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fbd4d133-778a-42ee-b0bd-51ea8ca703e2","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"fbd4d133-778a-42ee-b0bd-51ea8ca703e2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c79d490d-737f-46eb-985c-0cab164a68b0","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c79d490d-737f-46eb-985c-0cab164a68b0","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"c79d490d-737f-46eb-985c-0cab164a68b0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"742b45ba-1c53-4d1e-b6ab-27f3b3bf4b9a","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"742b45ba-1c53-4d1e-b6ab-27f3b3bf4b9a","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"742b45ba-1c53-4d1e-b6ab-27f3b3bf4b9a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5e3e1e06-a94b-4cb4-916c-298d0331ab47","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5e3e1e06-a94b-4cb4-916c-298d0331ab47","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"5e3e1e06-a94b-4cb4-916c-298d0331ab47","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cb04167e-bd78-4b50-84cb-7f4775bed22c","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cb04167e-bd78-4b50-84cb-7f4775bed22c","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"cb04167e-bd78-4b50-84cb-7f4775bed22c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0b67b12d-84f3-4753-9357-dacc12036de3","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0b67b12d-84f3-4753-9357-dacc12036de3","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"0b67b12d-84f3-4753-9357-dacc12036de3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7ff01b58-b35e-4bd5-bd9f-e0ec0f229a82","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7ff01b58-b35e-4bd5-bd9f-e0ec0f229a82","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"LayerId":{"name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","name":"7ff01b58-b35e-4bd5-bd9f-e0ec0f229a82","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"51fcfe60-9838-40c9-af2d-d3169ca28dcf","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ae7e1f2-7e8a-4cd5-984e-365a4e8aec6b","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cd19fa30-3dbf-4c30-8862-9bc5e8de34ac","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fbd4d133-778a-42ee-b0bd-51ea8ca703e2","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7c7e87a8-321f-49f3-b422-78f219afbebe","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c79d490d-737f-46eb-985c-0cab164a68b0","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9941755a-ec22-406e-8478-5ba78ba748c4","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"742b45ba-1c53-4d1e-b6ab-27f3b3bf4b9a","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c77ee26f-1a94-4208-899e-aeda4d953db0","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5e3e1e06-a94b-4cb4-916c-298d0331ab47","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f368860e-0d16-4a00-aee6-bd4bdbc254b7","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb04167e-bd78-4b50-84cb-7f4775bed22c","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"faf38eb2-8d7f-424f-96ba-a6d6439da401","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b67b12d-84f3-4753-9357-dacc12036de3","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dddb27d1-4d14-4244-893e-a360b3e5a3ac","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7ff01b58-b35e-4bd5-bd9f-e0ec0f229a82","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 22,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_quick_man_pose","path":"sprites/spr_quick_man_pose/spr_quick_man_pose.yy",},
    "resourceVersion": "1.3",
    "name": "spr_quick_man_pose",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8df33d49-2408-4ee2-bd3b-c4cb7bd34975","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Quick-Man",
    "path": "folders/Sprites/BOSS/Quick-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_quick_man_pose",
  "tags": [],
  "resourceType": "GMSprite",
}