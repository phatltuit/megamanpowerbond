{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 30,
  "bbox_top": 1,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 31,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5186b831-c564-4c9b-b614-219c46014cec","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5186b831-c564-4c9b-b614-219c46014cec","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},"LayerId":{"name":"f074fa1b-f48e-4272-bfa2-b9a73295dbd4","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_shadow_man_shadow_jutsu","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},"resourceVersion":"1.0","name":"5186b831-c564-4c9b-b614-219c46014cec","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_shadow_man_shadow_jutsu","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6415319e-826e-46fb-b6f1-5621df8a94cf","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5186b831-c564-4c9b-b614-219c46014cec","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_shadow_man_shadow_jutsu","path":"sprites/spr_shadow_man_shadow_jutsu/spr_shadow_man_shadow_jutsu.yy",},
    "resourceVersion": "1.3",
    "name": "spr_shadow_man_shadow_jutsu",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f074fa1b-f48e-4272-bfa2-b9a73295dbd4","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Shadow-Man",
    "path": "folders/Sprites/BOSS/Shadow-Man.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_shadow_man_shadow_jutsu",
  "tags": [],
  "resourceType": "GMSprite",
}