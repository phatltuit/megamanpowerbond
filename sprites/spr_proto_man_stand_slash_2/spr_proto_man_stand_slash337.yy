{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 59,
  "bbox_top": 1,
  "bbox_bottom": 34,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 60,
  "height": 35,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5327ef56-4c4d-4018-b53e-53e14ef73541","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5327ef56-4c4d-4018-b53e-53e14ef73541","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":{"name":"514ad8a3-d4ed-4541-ba00-33c8b5e5bce1","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"5327ef56-4c4d-4018-b53e-53e14ef73541","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"96726ca9-6828-486e-b935-1ade584c7e3c","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96726ca9-6828-486e-b935-1ade584c7e3c","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":{"name":"514ad8a3-d4ed-4541-ba00-33c8b5e5bce1","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"96726ca9-6828-486e-b935-1ade584c7e3c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"727e95bf-2a22-418e-ad72-eead49c2453d","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"727e95bf-2a22-418e-ad72-eead49c2453d","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":{"name":"514ad8a3-d4ed-4541-ba00-33c8b5e5bce1","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"727e95bf-2a22-418e-ad72-eead49c2453d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"774d60fe-5b3f-4431-89e4-52bbac9279d5","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"774d60fe-5b3f-4431-89e4-52bbac9279d5","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":{"name":"514ad8a3-d4ed-4541-ba00-33c8b5e5bce1","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"774d60fe-5b3f-4431-89e4-52bbac9279d5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3bc0b20b-cebd-4712-a7ff-7ca73ef49527","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3bc0b20b-cebd-4712-a7ff-7ca73ef49527","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"LayerId":{"name":"514ad8a3-d4ed-4541-ba00-33c8b5e5bce1","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","name":"3bc0b20b-cebd-4712-a7ff-7ca73ef49527","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e1017592-cd7f-4cee-9eb6-53c326ba5974","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5327ef56-4c4d-4018-b53e-53e14ef73541","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c240c6f6-596c-44f3-9301-63e102414b2a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96726ca9-6828-486e-b935-1ade584c7e3c","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"644ec284-5819-4d6f-aa82-1498b512184b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"727e95bf-2a22-418e-ad72-eead49c2453d","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a48cf888-0aef-4e55-b2ce-0f2c4a39c735","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"774d60fe-5b3f-4431-89e4-52bbac9279d5","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c0af7fc9-ece6-4758-8ece-31b1bd63e533","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3bc0b20b-cebd-4712-a7ff-7ca73ef49527","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 26,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_man_stand_slash337","path":"sprites/spr_proto_man_stand_slash337/spr_proto_man_stand_slash337.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_man_stand_slash",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"514ad8a3-d4ed-4541-ba00-33c8b5e5bce1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Primary",
    "path": "folders/Sprites/PLAYER/PROTOMAN/Primary.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_man_stand_slash337",
  "tags": [],
  "resourceType": "GMSprite",
}