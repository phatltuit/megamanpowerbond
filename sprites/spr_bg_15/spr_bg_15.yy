{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 223,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 224,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6030f48b-be3f-4fe5-9586-3eabe1f5092c","path":"sprites/spr_bg_15/spr_bg_15.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6030f48b-be3f-4fe5-9586-3eabe1f5092c","path":"sprites/spr_bg_15/spr_bg_15.yy",},"LayerId":{"name":"e725539a-4991-4852-acea-f3889b6827fa","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_15","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","name":"6030f48b-be3f-4fe5-9586-3eabe1f5092c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2ed48f0b-2a10-4145-a0a0-e84f8248f403","path":"sprites/spr_bg_15/spr_bg_15.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ed48f0b-2a10-4145-a0a0-e84f8248f403","path":"sprites/spr_bg_15/spr_bg_15.yy",},"LayerId":{"name":"e725539a-4991-4852-acea-f3889b6827fa","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_15","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","name":"2ed48f0b-2a10-4145-a0a0-e84f8248f403","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aafb8f4e-d3ef-4769-a21e-6633126e9c13","path":"sprites/spr_bg_15/spr_bg_15.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aafb8f4e-d3ef-4769-a21e-6633126e9c13","path":"sprites/spr_bg_15/spr_bg_15.yy",},"LayerId":{"name":"e725539a-4991-4852-acea-f3889b6827fa","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bg_15","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","name":"aafb8f4e-d3ef-4769-a21e-6633126e9c13","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bg_15","path":"sprites/spr_bg_15/spr_bg_15.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 6.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"53fba793-5105-4dd2-9a02-0dea1239f291","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6030f48b-be3f-4fe5-9586-3eabe1f5092c","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ab5ccd5-d04e-4f8c-b943-7d9cd1f87e09","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ed48f0b-2a10-4145-a0a0-e84f8248f403","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1913ebcb-6950-4ed8-8448-16b5922c1bbc","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aafb8f4e-d3ef-4769-a21e-6633126e9c13","path":"sprites/spr_bg_15/spr_bg_15.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bg_15","path":"sprites/spr_bg_15/spr_bg_15.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bg_15",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e725539a-4991-4852-acea-f3889b6827fa","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "BACKGROUND",
    "path": "folders/Sprites/BACKGROUND.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bg_15",
  "tags": [],
  "resourceType": "GMSprite",
}