{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 22,
  "bbox_top": 4,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 24,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9c58d1d6-dac7-4c09-a468-6fefe048ee2f","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9c58d1d6-dac7-4c09-a468-6fefe048ee2f","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":{"name":"4fe8100b-da12-49c4-81ab-5cd1d3b24cd2","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_tool_transform","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"9c58d1d6-dac7-4c09-a468-6fefe048ee2f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3cd26d2a-0875-4830-872c-8eb530d24607","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3cd26d2a-0875-4830-872c-8eb530d24607","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":{"name":"4fe8100b-da12-49c4-81ab-5cd1d3b24cd2","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_tool_transform","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"3cd26d2a-0875-4830-872c-8eb530d24607","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a61002f0-30a7-4919-a44e-83855e3d829c","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a61002f0-30a7-4919-a44e-83855e3d829c","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":{"name":"4fe8100b-da12-49c4-81ab-5cd1d3b24cd2","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_tool_transform","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"a61002f0-30a7-4919-a44e-83855e3d829c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e8976bd5-6cb4-4dc3-928d-27fe5383b594","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8976bd5-6cb4-4dc3-928d-27fe5383b594","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"LayerId":{"name":"4fe8100b-da12-49c4-81ab-5cd1d3b24cd2","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_tool_transform","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","name":"e8976bd5-6cb4-4dc3-928d-27fe5383b594","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_tool_transform","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"db172cb9-c08b-4b0b-a0b0-01a736e1fb46","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9c58d1d6-dac7-4c09-a468-6fefe048ee2f","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"565fe93e-45cc-4a4a-8657-60e8a4a527c4","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3cd26d2a-0875-4830-872c-8eb530d24607","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9d711525-54b9-4a4d-a340-62b41f885fbc","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a61002f0-30a7-4919-a44e-83855e3d829c","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cb49c0af-8e20-464e-b625-eea25c826c83","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8976bd5-6cb4-4dc3-928d-27fe5383b594","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_tool_transform","path":"sprites/spr_proto_tool_transform/spr_proto_tool_transform.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_tool_transform",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4fe8100b-da12-49c4-81ab-5cd1d3b24cd2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Proto-Tool",
    "path": "folders/Sprites/PLAYER/SUPPORT/Proto-Tool.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_tool_transform",
  "tags": [],
  "resourceType": "GMSprite",
}