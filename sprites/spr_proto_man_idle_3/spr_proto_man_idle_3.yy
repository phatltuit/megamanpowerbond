{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 22,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 23,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"90e77fa9-0085-4280-8b1e-3089b8c8e99d","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90e77fa9-0085-4280-8b1e-3089b8c8e99d","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"LayerId":{"name":"170ebd08-f3e7-4b83-ae4e-54259a52b537","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_idle_3","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","name":"90e77fa9-0085-4280-8b1e-3089b8c8e99d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e263bf34-ac3c-419f-a163-4af8a792cf4f","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e263bf34-ac3c-419f-a163-4af8a792cf4f","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"LayerId":{"name":"170ebd08-f3e7-4b83-ae4e-54259a52b537","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_idle_3","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","name":"e263bf34-ac3c-419f-a163-4af8a792cf4f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a89b6f7-6420-4e34-b665-50ae0fc9981e","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a89b6f7-6420-4e34-b665-50ae0fc9981e","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"LayerId":{"name":"170ebd08-f3e7-4b83-ae4e-54259a52b537","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_proto_man_idle_3","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","name":"7a89b6f7-6420-4e34-b665-50ae0fc9981e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_proto_man_idle_3","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 2.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"dc1d2ebf-59b4-4f84-9402-c92fe0466552","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90e77fa9-0085-4280-8b1e-3089b8c8e99d","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42314c32-77dc-4038-99e9-5a4ab81ded59","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e263bf34-ac3c-419f-a163-4af8a792cf4f","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13b8764c-cc9f-407b-872d-a8754e24f607","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a89b6f7-6420-4e34-b665-50ae0fc9981e","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_proto_man_idle_3","path":"sprites/spr_proto_man_idle_3/spr_proto_man_idle_3.yy",},
    "resourceVersion": "1.3",
    "name": "spr_proto_man_idle_3",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"170ebd08-f3e7-4b83-ae4e-54259a52b537","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Outline",
    "path": "folders/Sprites/PLAYER/PROTOMAN/Outline.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_proto_man_idle_3",
  "tags": [],
  "resourceType": "GMSprite",
}