{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 24,
  "bbox_top": 3,
  "bbox_bottom": 28,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 26,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"0370db30-f224-424c-9446-dab70f878340","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0370db30-f224-424c-9446-dab70f878340","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},"LayerId":{"name":"7cbaa441-c91c-4b17-821a-a16ed9a4fd68","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bass_damaged_2","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},"resourceVersion":"1.0","name":"0370db30-f224-424c-9446-dab70f878340","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bass_damaged_2","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b654e0a6-ce72-4459-b23d-45ee740b5db6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0370db30-f224-424c-9446-dab70f878340","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 13,
    "yorigin": 17,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bass_damaged_2","path":"sprites/spr_bass_damaged_2/spr_bass_damaged_2.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bass_damaged_2",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"7cbaa441-c91c-4b17-821a-a16ed9a4fd68","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Secondary",
    "path": "folders/Sprites/PLAYER/BASS/Secondary.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bass_damaged_2",
  "tags": [],
  "resourceType": "GMSprite",
}