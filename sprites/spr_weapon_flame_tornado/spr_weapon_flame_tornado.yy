{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 3,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2258cec4-6250-41ea-a1d7-29ab53cfca81","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2258cec4-6250-41ea-a1d7-29ab53cfca81","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"LayerId":{"name":"ac173c85-f8b0-4abc-9444-09538cc901cd","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_flame_tornado","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","name":"2258cec4-6250-41ea-a1d7-29ab53cfca81","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34d7a5c5-aa32-4940-abf6-652ce218b740","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34d7a5c5-aa32-4940-abf6-652ce218b740","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"LayerId":{"name":"ac173c85-f8b0-4abc-9444-09538cc901cd","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_flame_tornado","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","name":"34d7a5c5-aa32-4940-abf6-652ce218b740","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"100a562c-d927-4b97-8027-fc721a0602d0","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"100a562c-d927-4b97-8027-fc721a0602d0","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"LayerId":{"name":"ac173c85-f8b0-4abc-9444-09538cc901cd","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_weapon_flame_tornado","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","name":"100a562c-d927-4b97-8027-fc721a0602d0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_weapon_flame_tornado","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d8a20893-7e93-422a-93be-d7fbd4cbe604","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2258cec4-6250-41ea-a1d7-29ab53cfca81","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5c9eb515-ba91-440a-8668-23ea27dec787","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34d7a5c5-aa32-4940-abf6-652ce218b740","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f609ac47-e524-4771-b4e3-cddaceb81623","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"100a562c-d927-4b97-8027-fc721a0602d0","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_weapon_flame_tornado","path":"sprites/spr_weapon_flame_tornado/spr_weapon_flame_tornado.yy",},
    "resourceVersion": "1.3",
    "name": "spr_weapon_flame_tornado",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ac173c85-f8b0-4abc-9444-09538cc901cd","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Menu",
    "path": "folders/Sprites/PLAYER/WEAPON/Menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_weapon_flame_tornado",
  "tags": [],
  "resourceType": "GMSprite",
}