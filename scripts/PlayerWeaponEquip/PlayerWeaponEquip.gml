// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerWeaponEquip(){
	if keyboard_check_pressed(global.keyNextWp) and override == false{
		audio_stop_sound(WeaponChange);
		audio_play_sound(WeaponChange,1,false);
		shoot = false;
		quickEquip = true;
		quickEquipTime = 90;
		global.weaponIndex++;	
		instance_destroy(objPlayerWeapon);
		instance_destroy(objTarget);
		if global.weaponIndex > 9{
			global.weaponIndex = 0;
		}
		
		while global.weaponUnlocked[global.weaponIndex] == false{
			global.weaponIndex++;
			if global.weaponIndex > 9{
				global.weaponIndex = 0;
			}
		}
			
		if global.weaponIndex != 0{
			objPlayer.supportFly = false;
			SupporterDismiss();
		}
		PlayerColor();
				
	}else if keyboard_check_pressed(global.keyPrevWp) and override == false{
		audio_stop_sound(WeaponChange);
		audio_play_sound(WeaponChange,1,false);
		shoot = false;
		quickEquip = true;
		quickEquipTime = 90;
		global.weaponIndex--;	
		instance_destroy(objPlayerWeapon);
		instance_destroy(objTarget);
		if global.weaponIndex < 0{
			global.weaponIndex = 9;
		}
			
		while global.weaponUnlocked[global.weaponIndex] == false{
			global.weaponIndex--;
			if global.weaponIndex < 0{
				global.weaponIndex = 9;
			}
		}
			
		if global.weaponIndex != 0{
			objPlayer.supportFly = false;
			SupporterDismiss();
		}
		PlayerColor();
	}
}