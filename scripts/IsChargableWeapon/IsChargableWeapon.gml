// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsChargableWeapon(){
	switch (global.weaponIndex) {
		case 0:
			return object_index != objBass;
	    case 3:
		case 4:
		case 7:
	        return true;
	        break;
	    default:
	        return false;
	        break;
	}
}