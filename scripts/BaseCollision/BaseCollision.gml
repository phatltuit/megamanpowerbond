// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BaseCollision(){
	//Ceil
	var mySolid = instance_place(x, y+vspeed - 2, objSolid);
	if mySolid >= 0 and vspeed < 0
	{			
		y = mySolid.y;
		while place_meeting(x, y, mySolid)
			y += 1;	
		vspeed = 0;		
	}
	
	//Wall
	var mySolid = instance_place(x + hspeed + sign(hspeed), y, objSolid);
	if mySolid >= 0 and hspeed != 0{			
		x = mySolid.x;
		while place_meeting(x  + sign(hspeed), y, objSolid){
			x -=sign(hspeed);	
		}		
		hspeed = 0;		
		speed = 0;
	}

	//Ground
	var mySolid = instance_place(x, y+vspeed + 1, objSolid);
	if mySolid >= 0 and vspeed >= 0
	{			
		y = mySolid.y;
		while place_meeting(x, y, mySolid)
			y -= 1;
		ground = true;
		vspeed = 0;
	}else{
		ground = false;	
	}
	
	
	if ground == true{
		vspeed = 0;
		gravity = 0;
	}else{
		gravity = grav;	
	}	
}