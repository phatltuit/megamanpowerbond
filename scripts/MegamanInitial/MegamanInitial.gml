function MegamanInitial() {
	//Physics variables
	runSpdOriginal = 1.45;
	jumpSpdOriginal = 5.5;
	slideSpdOriginal = 2.75;
	
	
	runSpd = 1.45;
	jumpSpd = 5.5;
	slideSpd = 2.75;
	grav = 0.25;
	knockBackSpd = 0.2;
	climbSpd = 1.2;
	climbDir = 0;

	x_end = x;
	y_end = y;

	//State variables
	ready = false;
	beginPhase = 0;
	block = false;
	run = false;
	ground = false;
	slide = false;
	shoot = false;
	charge = false;
	stuck = false;
	damaged = false;
	knockBack = false;
	climb = false;
	openDoor = false;	
	canSlash = false;
	//image_speed = 0.6;

	MegamanHandleSprite();

	sprPrimary = noone;
	sprSecondary = noone;
	sprOutline = noone;
	primaryCol = noone;
	secondaryCol = noone;
	outlineCol = noone;
	
	
	

	//alarm
	slideAlarm = 25;
	chargeAlarm = 100;
	flashIndex = 1;
	chargeLv2Limit = 85;
	chargeLv3Limit = 100;

	//Create camera
	nextZoneXOffSet = 0;
	nextZoneYOffSet = 0;
}
