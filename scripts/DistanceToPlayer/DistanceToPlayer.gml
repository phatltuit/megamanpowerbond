// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function DistanceToPlayer(){
	if instance_exists(objPlayer){
		return abs(x-objPlayer.x);	
	}
	return 0;	

}