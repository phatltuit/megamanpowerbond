function MegamanDrawSprite() {
	switch (sprite_index) {	
		case sprSlam:
			sprPrimary = sprSlam_Draw[0];
			sprSecondary = sprSlam_Draw[1];
			sprOutline = sprSlam_Draw[2];
			break;
		
		case sprJumpDart:
			sprPrimary = sprJumpDart_Draw[0];
			sprSecondary = sprJumpDart_Draw[1];
			sprOutline = sprJumpDart_Draw[2];
			break;		
		
		case sprStandDart:
			sprPrimary = sprStandDart_Draw[0];
			sprSecondary = sprStandDart_Draw[1];
			sprOutline = sprStandDart_Draw[2];
	        break;
		
	    case sprStand:
	        sprPrimary = sprStand_Draw[0];
			sprSecondary = sprStand_Draw[1];
			sprOutline = sprStand_Draw[2];
	        break;
		
		case sprSlide:
	        sprPrimary = sprSlide_Draw[0];
			sprSecondary = sprSlide_Draw[1];
			sprOutline = sprSlide_Draw[2];
	        break;
		
		case sprRun:
	        sprPrimary = sprRun_Draw[0];
			sprSecondary = sprRun_Draw[1];
			sprOutline = sprRun_Draw[2];
	        break;
		
		case sprJump:
	        sprPrimary = sprJump_Draw[0];
			sprSecondary = sprJump_Draw[1];
			sprOutline = sprJump_Draw[2];
	        break;
			
		case sprClimb:
	        sprPrimary = sprClimb_Draw[0];
			sprSecondary = sprClimb_Draw[1];
			sprOutline = sprClimb_Draw[2];
	        break;
		
		case sprClimbStop:
	        sprPrimary = sprClimbStop_Draw[0];
			sprSecondary = sprClimbStop_Draw[1];
			sprOutline = sprClimbStop_Draw[2];
	        break;
		
		case sprClimbShoot:
			sprPrimary = sprClimbShoot_Draw[0];
			sprSecondary = sprClimbShoot_Draw[1];
			sprOutline = sprClimbShoot_Draw[2];
			break;
		
		case sprJumpShoot:
			sprPrimary = sprJumpShoot_Draw[0];
			sprSecondary = sprJumpShoot_Draw[1];
			sprOutline = sprJumpShoot_Draw[2];
			break;
		
		case sprRunShoot:
			sprPrimary = sprRunShoot_Draw[0];
			sprSecondary = sprRunShoot_Draw[1];
			sprOutline = sprRunShoot_Draw[2];
			break;
		
		case sprStandShoot:
			sprPrimary = sprStandShoot_Draw[0];
			sprSecondary = sprStandShoot_Draw[1];
			sprOutline = sprStandShoot_Draw[2];
			break;
	
		
		case sprClimbUp:
	        sprPrimary = sprClimbUp_Draw[0];
			sprSecondary = sprClimbUp_Draw[1];
			sprOutline = sprClimbUp_Draw[2];
	        break;
		
		case sprHit:
	        sprPrimary = sprHit_Draw[0];
			sprSecondary = sprHit_Draw[1];
			sprOutline = sprHit_Draw[2];
	        break;
			
		case sprStandSlash:
			sprPrimary = sprStandSlash_Draw[0];
			sprSecondary = sprStandSlash_Draw[1];
			sprOutline = sprStandSlash_Draw[2];		
			break;
			
		case sprJumpSlash:
			sprPrimary = sprJumpSlash_Draw[0];
			sprSecondary = sprJumpSlash_Draw[1];
			sprOutline = sprJumpSlash_Draw[2];		
			break;
	
		
	    default:
			sprPrimary = noone;
			sprSecondary = noone;
			sprOutline = noone;
	        break;
	}

}
