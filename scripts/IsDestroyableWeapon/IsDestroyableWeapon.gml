// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsDestroyableWeapon(playerWeapon){
	return global.weaponIndex != 6 and global.weaponIndex != 5 and global.weaponIndex != 7 and global.weaponIndex != 3 and playerWeapon.sprite_index != spr_solar_blast_b and playerWeapon.sprite_index != spr_explosion;
}