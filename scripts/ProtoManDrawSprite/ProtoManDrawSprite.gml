// Script assets have changed for v3.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005377377 for more information
function ProtoManDrawSprite(){
	if override == true and object_index == objProtoMan{
		switch (sprite_index) {	
			case sprSlam:
				sprProtoStrk = sprSlam_Draw[3];
				break;
		
			case sprJumpDart:
				sprProtoStrk = sprJumpDart_Draw[3];
				break;		
		
			case sprStandDart:
				sprProtoStrk = sprStandDart_Draw[3];
		        break;		
		
		    case sprStand:
				sprProtoStrk = sprStand_Draw[3];
		        break;
		
			case sprSlide:
				sprProtoStrk = sprSlide_Draw[3];
		        break;
		
			case sprRun:
				sprProtoStrk = sprRun_Draw[3];
		        break;
		
			case sprJump:
				sprProtoStrk = sprJump_Draw[3];
		        break;							
		
			case sprClimbShoot:
				sprProtoStrk = sprClimbShoot_Draw[3];
				break;
		
			case sprJumpShoot:
				sprProtoStrk = sprJumpShoot_Draw[3];
				break;
		
			case sprRunShoot:
				sprProtoStrk = sprRunShoot_Draw[3];
				break;
		
			case sprStandShoot:
				sprProtoStrk = sprStandShoot_Draw[3];
				break;
		
			case sprHit:
				sprProtoStrk = sprHit_Draw[3];
		        break;
			
			case sprStandSlash:
				sprProtoStrk = sprStandSlash_Draw[3];		
				break;
			
			case sprJumpSlash:
				sprProtoStrk = sprJumpSlash_Draw[3];	
				break;
	
		
		    default:
				sprProtoStrk = noone;
		        break;
		}
		
	}
}