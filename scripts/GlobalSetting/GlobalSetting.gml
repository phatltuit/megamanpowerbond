function GlobalSetting() {
	global.bossFight = false;
	global.multiBoss = false;
	global.defeat = false;
	global.Freeze = false;
	global.Paused = false;
	global.particleSystem = part_system_create();
	global.keyLeft = ord("A");
	global.keyRight = ord("D");
	global.keyUp = ord("W");
	global.keyDown = ord("S");
	//global.keySwap = ord("O");
	global.keyJump = ord("K");
	global.keySlide = ord("L");
	global.keyDash = ord("L");
	global.keyShoot = ord("J");
	global.keySlash = ord("H");
	global.keyShootBass = ord("J");
	global.keyNextWp = ord("I");
	global.keyPrevWp = ord("U");
	//global.keyOverride = ord("M");
	global.keyStart = vk_enter;
	//global.keySelect = vk_backspace;
	
	//Sound
	global.MusicVolume = 1;
	global.SFXVolume = 1;
	global.StageMusic = noone;
	
	enum Buster{
		MiniBuster,
		MegaBuster,
		GigaBuster
	}
	
	enum Support{
		RushCoil,
		RushJet,
		TrebleCoil,
		TrebleJet,
		ProtoCoil,
		ProtoJet	
	}
	
	enum BossWeapon{
		ShadowBlade,
		HomingMissile,
		QuickBoomerang	,
		SolarBlast,
		SonicSlash,
		IceSlasher,
		FlameTornado		
	}

	enum Button{
		KeyUp,KeyDown,KeyLeft,KeyRight,KeyDash,KeySlide,KeyShoot,KeyJump,KeyNxtWp,KeyPrvWp,KeySwap,KeyStart,KeySelect	
	}

	//Collsion Type
	enum CollisionType{
		Ceil, 
		Floor, 
		Left, 
		Right,
		SlideLeft,
		SlideRight,
		Enemies,
		Bosses
	}

	enum TransitionDirection{
		GoLeft,
		GoRight,
		GoUp,
		GoDown
	}

	enum Player{
		Megaman,
		Bass,
		Protoman
	}
}
