function PlayerBass() {
	if ready == false{
		BassBegin();	
	}else{	
		if block == false{		
			#region Camera View
			PlayerCamera();
			PlayerZoneSwitch();
		#endregion
		
			PlayerWeaponEquip();
			if knockBack == false{		
				if supportFly == false{
					if slam == false{					
						#region Stepping
							if keyboard_check_direct(global.keyLeft) and !place_meeting(x + xspeed-1, y,objSolid) and slash == false{
								if dash == false and climb == false{
									run = true;
									if hyperSpeed == false{						
										xspeed = -runSpd;
									}else{
										xspeed = -dashJumpSpd;
									}
								}else if dash == true{
									xspeed = -dashSpd;
								}
								image_xscale = -1;
							}else if keyboard_check_direct(global.keyRight) and !place_meeting(x + xspeed+1, y,objSolid) and slash == false{
								if dash == false and climb == false{
									run = true;
									if hyperSpeed == false{
										xspeed = runSpd;
									}else{
										xspeed = dashJumpSpd;
									}
								}else if dash == true{
									xspeed = dashSpd;
								}
								image_xscale = 1;

							}else{
								run = false;
								if dash == false{
									xspeed = 0;
								}
							}	
						#endregion
				
						#region Climbing
							if keyboard_check_direct(global.keyUp) and instance_place(x, y + vspeed - 1, objLadder) != noone{
								climb = true;
								ground = false;
								hyperSpeed = false;
								var ladderBelow = instance_place(x, y + vspeed - 1, objLadder);
								x = ladderBelow.x;
								if shoot == true{
									climbDir = 0;	
									yspeed = 0;
								}else{
									climbDir = 1;
									yspeed = -climbSpd;
									sprite_index = sprClimb;
								}
								xspeed = 0;			
							}else if keyboard_check_direct(global.keyDown) and (!keyboard_check_direct(global.keyShoot) or charge == true) and instance_place(x, y + vspeed + 1, objLadder) != noone{			
								var ladderBelow = instance_place(x, y + vspeed + 1, objLadder);
								if x < ladderBelow.x+5 and x > ladderBelow.x-5 {
									ground = false;	
									hyperSpeed = false;					
									if ladderBelow >= 0 and ladderBelow.isTop == true and climb == false{
										y = ladderBelow.bbox_top + 1;	
									}
									climb = true;
									x = ladderBelow.x;
									if shoot == true{
										climbDir = 0;	
										yspeed = 0;
									}else{
										climbDir = -1;
										yspeed = climbSpd;
										sprite_index = sprClimb;
									}
									xspeed = 0;
								}
							}else{
								if climb == true{
									if instance_place(x, y+yspeed+1, objLadder) == noone{
										climb = false;	
									}else{
										sprite_index = sprClimbStop;
										climbDir = 0;	
										yspeed = 0;					
									}
								}
							}
						#endregion	
					}
				}else{
					#region Jet Flying
						supportFlyTimer++;
						if supportFlyTimer >= 60{
							global.weaponEnergy[9] -= 1;
							supportFlyTimer = 0;
						}
						
						if keyboard_check_direct(global.keyRight){
							xspeed = runSpd;
							image_xscale = 1;																
						}else if keyboard_check_direct(global.keyLeft){
							xspeed = -runSpd;
							image_xscale = -1;																	
						}else{
							xspeed = 0;
						}
							
						if keyboard_check_direct(global.keyUp){
							yspeed = -runSpd;	
						}else if keyboard_check_direct(global.keyDown){
							yspeed = runSpd;	
						}else{
							yspeed = 0;	
						}	
					#endregion					
				}
				
				#region Dashing
					var slideBox = image_xscale > 0? bbox_right : bbox_left;
					var nearSolid = collision_rectangle(x, y-5,slideBox+image_xscale, y+8, objSolid, false, true);
					if keyboard_check_pressed(global.keySlide) and supportFly == false 
					and (nearSolid == noone or (nearSolid != noone
					and nearSolid.sprite_index == spr_gate_open)) 
					and dash == false and ground == true{
						dash = true;
						slash = false;
						airSlash = false;
						xspeed = dashSpd*image_xscale;
						mask_index = spr_bass_dash_mask;					
						alarm[2] = 25;
						if image_xscale == 1{
							box = bbox_left - 1;
						}else{
							box = bbox_right + 1;	
						}
						var dust = instance_create_depth(box,bbox_bottom, 0,objDust)
						dust.sprite_index = spr_bass_air_jump;
						dust.image_angle = image_xscale == 1?-90:90;					
						}	
				#endregion
			
				#region Shooting	
					if global.weaponIndex == 0{
						if keyboard_check_direct(global.keyShoot) and shoot == false and slash == false and airSlash == false{
							shoot = true;			
							BassShootDirection();
							alarm[3] = 10;
							BassShoot();
						}						
					}else{
						if keyboard_check_direct(global.keyShoot) and slash == false and airSlash == false and IsChargableWeapon() and global.weaponEnergy[global.weaponIndex] > 0{						
							charge = true;
							chargeAlarm-= 1;
						}
		
						if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{	
							if playChargeSfx == false{ 
								audio_play_sound(PlayerCharge, 1, false);						
								global.fadedSound = 1;
								audio_sound_gain(PlayerCharge, global.fadedSound, 0);
								playChargeSfx = true;
							}	
						
							if chargeAlarm < round(chargeLv2Limit/3){
								outlineCol = chargeLv1Color[2];	
							}else if chargeAlarm < round(chargeLv2Limit/3)*2{
								outlineCol = chargeLv1Color[1];	
							}else{
								outlineCol = chargeLv1Color[0];	
							}
						}else if chargeAlarm < 0{
							if global.fadedSound < 0.1{
								global.fadedSound = 0.1;
							}else{
								global.fadedSound -= 0.01;
							}
							audio_sound_gain(PlayerCharge, global.fadedSound, 500);					
							switch (abs(chargeAlarm/2 mod 3))
							{
								case 0: //Light blue helmet, black shirt, blue outline
								    primaryCol = chargeLv2Color[0];
								    secondaryCol = chargeLv2Color[2];
								    outlineCol = chargeLv2Color[1];
								break;
                        
								case 1: //Black helmet, blue shirt, light blue outline
								    primaryCol = chargeLv2Color[2];
								    secondaryCol = chargeLv2Color[1];
								    outlineCol = chargeLv2Color[0];
								break;
                        
								case 2: //Blue helmet, light blue shirt, blue outline
								    primaryCol = chargeLv2Color[1];
								    secondaryCol = chargeLv2Color[0];
								    outlineCol = chargeLv2Color[2];
								break;
							}	
						}
						
						if keyboard_check_released(global.keyShoot) and shoot == false and slash == false and airSlash == false{
							charge = false;
							playChargeSfx = false;								
							audio_stop_sound(PlayerCharge);
							PlayerWeaponShoot();
							chargeAlarm = 100;
							alarm[3] = 15;
						}
					}
				#endregion
				
				#region Slashing--->Retired feature					
					/*if keyboard_check_direct(global.keySlash) and global.weaponIndex == 0 and shoot == false{
						if ground == true{
							if slash == false and dash == false{
								slash = true;
								sprite_index = sprStandSlash;							
								var saber = instance_create_depth(x,y,0,objPlayerWeapon);
								saber.sprite_index = spr_bass_stand_saber;
								saber.image_xscale = image_xscale;
								saber.image_index = 0;
								saber.visible = false;
							}
							airSlash = false;
						}else{
							if airSlash == false{
								sprite_index = sprJumpSlash;
								airSlash = true;							
								var saber = instance_create_depth(x,y,0,objPlayerWeapon);
								saber.sprite_index = spr_bass_jump_saber;
								saber.image_xscale = image_xscale;
								saber.image_index = 0;
								saber.visible = false;
							}
							slash = false;
						}						
					}
								
					if (sprite_index == sprStandSlash or sprite_index == sprJumpSlash) and image_index + image_speed >= image_number{
						slash = false;
						airSlash = false;
					}*/					
				#endregion
			}
			#region Underwater condition 
				PlayerSpecialCondition();
			#endregion
		}
	
		#region Collision		
			#region SOLID		
				if !instance_exists(objZoneSwitcher){					
					//Wall when moving by effect
					if eff_xspeed != 0{
						var mySolid = collision_rectangle(bbox_left-1+eff_xspeed, bbox_top, bbox_right+1+eff_xspeed, bbox_bottom, objSolid, false, true)
						
						if mySolid >= 0{
							if mySolid.x < x{
								while !place_meeting(x-1,y,objSolid){
									x--;	
								}
								xspeed = 0;
							}else if mySolid.x > x{
								while !place_meeting(x+1,y,objSolid){
									x++;	
								}
								xspeed = 0;
							}
						}
					}else{
					//Wall
						if place_meeting(x + xspeed + sign(xspeed), y, objSolid){
						var mySolid = instance_place(x + xspeed + sign(xspeed), y, objSolid);
						if mySolid >= 0 and xspeed != 0{			
							x = mySolid.x;
							if dash == true{
								dash = false;	
								while place_meeting(x, y, objSolid){
									x -=sign(xspeed);	
								}
							}else{
								while place_meeting(x + sign(xspeed), y, objSolid){
									x -=sign(xspeed);	
								}
							}
							xspeed = 0;														
							run = false;							
						}
					}
				}

				//Floor
				mySolid = instance_place(x, y+vspeed + 1, objSolid);
				var myMVSolid = instance_place(x, y+vspeed + 1, objMovingSolid);
				if mySolid >= 0 and vspeed >= 0
				{	
					y = mySolid.y;
					while place_meeting(x, y, mySolid)
						y -= 1;
					if climb == true and climbDir == -1{
						climb = false;	
					}
					ground = true; 
					supportJump = false;
					if supportFly == true{
						supportFly = false;	
						SupporterDismiss();
					}
					if playLandSfx == false{
						audio_play_sound(PlayerLand, 1, false);
						playLandSfx = true;
					}
				}else if myMVSolid >= 0 and vspeed >= 0{	
					y = myMVSolid.y;
					while place_meeting(x, y, myMVSolid)
						y -= 1;
					if climb == true and climbDir == -1{
						climb = false;	
					}
					ground = true;  
					supportJump = false;
					if myMVSolid.hspeed != 0{							
						if (run == true or dash == true) and shoot == false{								
							xspeed = myMVSolid.hspeed+image_xscale;
						}else{
							xspeed = myMVSolid.hspeed;
						}
							
					}
					
					if playLandSfx == false{
						audio_play_sound(PlayerLand, 1, false);
						playLandSfx = true;
					}
				}else{
					moveByPlatform = false;	
					if supportFly == true{
							ground = true;		
					}else{
						ground = false;	
					}
					if dash == true
					and (instance_place(x, y+vspeed + 1, objMovingSolid) == noone 
					and instance_place(x, y+vspeed + 1, objLadder) == noone){
						dash = false;
						if vspeed >= 0 and maskDelay == false{
							maskDelay = true;
							alarm[1] = 5;
							mask_index = spr_bass_dash_mask;
						}
					}
				}
		
				//Ceil
				if supportFly == false{
					mySolid = instance_place(x, y+vspeed-2, objSolid);
					if mySolid >= 0 and vspeed <= 0
					{					
						y = mySolid.bbox_bottom + abs(bbox_top - y) + 1;
						vspeed = 0;
						supportJump = false;
					}
				}else{
					mySolid = instance_place(x, y+yspeed-2, objSolid);
					if mySolid >= 0 and yspeed < 0{					
						y = mySolid.bbox_bottom + abs(bbox_top - y) + 1;
						yspeed = 0;
						supportJump = false;
					}
				}
		
				//Top ladder		
				var myTopLadder = collision_rectangle(bbox_left, bbox_bottom+1, bbox_right,bbox_top, objLadder, false, true);		
				if myTopLadder >= 0 and myTopLadder.isTop == true and supportFly == false{
					if y > myTopLadder.bbox_top and y <= myTopLadder.y {
						sprite_index = sprClimbUp	
					}else if y <= myTopLadder.bbox_top and vspeed >= 0{					
						y = myTopLadder.y;
						while place_meeting(x, y, myTopLadder){
							y-= 1;	
						}
						climb = false;
						ground = true;		
						supportJump = false;
						vspeed = 0;
						if playLandSfx == false{
							audio_play_sound(PlayerLand, 1, false);
							playLandSfx = true;
						}
					}
				}
			}			
			#endregion
			
			
			#region Moving SOLID			
				//Wall
				if !instance_exists(objZoneSwitcher){
					//Wall
					if place_meeting(x+xspeed+sign(xspeed), y, objMovingSolid){
						var myMVSolid = instance_place(x+xspeed+sign(xspeed), y, objMovingSolid);
						if myMVSolid >= 0{
							if dash == true{
								dash = false;	
							}	
							if x > myMVSolid.x{
								side = 1;	
							}else if x < myMVSolid.x{
								side = -1;
							}else{
								side = 0;	
							}
							x = myMVSolid.x;
							
							while place_meeting(x+xspeed+sign(xspeed) + side, y , objMovingSolid){
								x +=ground?side:side*2;	
							}
							xspeed = 0;									
							run = false;							
						}
					}
				}
			
				//Ceil
				myMVSolid = instance_place(x, y+vspeed-2, objMovingSolid);
				if myMVSolid >= 0 and vspeed < 0{					
					y = myMVSolid.bbox_bottom + abs(bbox_top - y) + 1;
					vspeed = myMVSolid.vspeed<0?0:myMVSolid.vspeed;
				}
			#endregion
		#endregion
			
		#region Jumping		
			if block == false and keyboard_check_pressed(global.keyJump) and (ground == true or climb == true or (jumpTime <= 1 and jumpTime > 0)) and !place_meeting(x, y +vspeed - 2, objSolid){
				ground = false;
				supportFly = false;
				if jumpTime == 1{
					var air = instance_create_depth(x,bbox_bottom+12, 0,objDust)
					air.sprite_index = spr_bass_air_jump;
				}	
				jumpTime--;
					
				if dash == true{
					mask_index = spr_bass_normal_mask;
					hyperSpeed = true;	
				}
				if climb == true{
					keyboard_key_release(global.keyUp);
					keyboard_key_release(global.keyDown);
					climb = false;
					vspeed = 0;
				}else{
					vspeed = -jumpSpd;	
					
				}
			}
		
			if ground == false{		
				if climb == true{
					jumpTime = 2;
				}else{
					if jumpTime > 0{
						jumpTime = 1;
					}
				}
				canMinJump = true;
				if slam == true{
					gravity = 0;
					vspeed = 0
				}else{
					gravity = grav;			
				}
				playLandSfx = false;
				slash = false;
			}else{					
				hyperSpeed = false;
				gravity = 0;
				vspeed = 0;	
				jumpTime = 2;
				airSlash = false;
			}	
			
			if supportJump == false{
				if ground == false and vspeed < 0 and !keyboard_check_direct(global.keyJump) and canMinJump == true{
					canMinJump = false;	
					vspeed = 0;	
				}		
			}
		#endregion
				
		#region Sprites
			if knockBack == false{
				if supportFly == true{
					if shoot == true{
						if IsDartWeapon(){
							sprite_index = sprStandDart;
						}else{
							sprite_index = sprStandShoot[global.weaponIndex==0?shootDirection:0];
						}
					}else if slash == true{
						
					}else{
						sprite_index = sprStand;	
					}
				}else{
					if slam == false{
						if ground == true{
							if dash == true{
								if shoot == true{
									sprite_index = sprDashShoot;
								}else{
									sprite_index = sprDash;		
								}
							}else{				
								if shoot == true{
									if IsDartWeapon(){
										sprite_index = sprStandDart;
									}else{
										sprite_index = sprStandShoot[global.weaponIndex==0?shootDirection:0];
									}
								}else if slash == false{
									if run == true{
										sprite_index = sprRun;	
									}else{
										sprite_index = sprStand;
									}
								}					
							}
						}else{
							if climb == false{
								if shoot == true{
									if IsDartWeapon(){
										sprite_index = sprJumpDart;
									}else{
										sprite_index = sprJumpShoot[global.weaponIndex==0?shootDirection:0];
									}
								}else if airSlash == true{
									sprite_index = sprJumpSlash;
								}else{				
									sprite_index = sprJump;	
								}
							}else{
								if shoot == true{
									sprite_index = sprClimbShoot[global.weaponIndex==0?shootDirection:0];
								}	
							}
						}
					}else{
						sprite_index = sprSlam;		
					}
				}
			}else{
				sprite_index = sprHit;			
			}
		#endregion
	
		#region Damaging
			PlayerDamage();			
		#endregion
	
		#region Moving
			if supportFly == true{
				vspeed = yspeed;	
				hspeed = xspeed;
			}else{
				if dash == false{
					if maskDelay == false{
						mask_index = spr_bass_normal_mask;	
					}
				}else{
					if ground == true and dash == true and place_meeting(x, y - 2, objSolid){
						alarm[2] = 2;
					}
					mask_index = spr_bass_dash_mask;		
				}			
				
				if shoot == false and slash == false{
					hspeed = xspeed + eff_xspeed;
					if climb == true{
						vspeed = yspeed;
						gravity = 0;
					}
				}else{
					if ground == false{
						if climb == false{
							hspeed = xspeed + eff_xspeed;
						}else{
							hspeed = 0;
							vspeed = 0;
							gravity = 0;
						}				
					}else{
						if dash == false{	
							if slam == true{
								hspeed = xspeed;
							}else{
								hspeed = place_meeting(x, y +vspeed + 1, objMovingSolid)?(xspeed+ eff_xspeed):eff_xspeed;			
							}
						}else{
							hspeed = xspeed + eff_xspeed;	
						}
					}
				}	
			}
		#endregion
	}
}
