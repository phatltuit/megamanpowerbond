function MegamanButtonPress() {
#region Move Left/Right
	if KeyboardCheck(global.keyLeft){
		image_xscale = -1;	
		if !PlayerCollision(CollisionType.Left, objSolid){
			if slide == true{
				run = false;	
			}else{
				run = true;	
			}
		}else{
			hspeed = 0;
			run = false;		
		}
	}else if KeyboardCheck(global.keyRight){
		image_xscale = 1;	
		if !PlayerCollision(CollisionType.Right, objSolid){
			if slide == true{
				run = false;	
			}else{
				run = true;	
			}
		}else{
			hspeed = 0;
			run = false;		
		}
	}else{		
		run = false;	
	}
#endregion

#region Sliding
	if ground == true and KeyboardCheck(global.keySlide) and slide == false{
		if !PlayerCollision(CollisionType.Right, objSolid) and image_xscale == 1{
			slide = true;
			mask_index = spr_mega_man_slide_mask;
			alarm[2] = slideAlarm;		
		}else if !PlayerCollision(CollisionType.Left, objSolid) and image_xscale == -1{
			slide = true;
			mask_index = spr_mega_man_slide_mask;
			alarm[2] = slideAlarm;	
		}else{
			slide = true;
			hspeed = 0;
		}
	}
#endregion

#region Jumping
	if (ground == true or climb == true) and KeyboardCheck(global.keyJump) and !PlayerCollision(CollisionType.Ceil, objSolid){
		slide = false;	
		if climb == true{
			vspeed = 0;
			climb =false;
		}else{
			vspeed = -jumpSpd;	
		}
		ground = false;
	}
#endregion

#region Shooting
	if keyboard_check_direct(global.keyShoot){
		charge = true;
		chargeAlarm-= 1;
	}


	MegamanChargeAnimation();


	MegamanPressShoot();
#endregion

#region Climbing
		if KeyboardCheck(global.keyUp) and instance_place(x, y+vspeed - 1, objLadder) != noone{
			climb = true;
			ground = false;
			ladderBelow = instance_place(x, y+vspeed - 1, objLadder);
			x = ladderBelow.x;
			if shoot == true{
				climbDir = 0;	
			}else{
				climbDir = 1;
			}
		}else if KeyboardCheck(global.keyDown) and instance_place(x, y+vspeed + 1, objLadder) != noone{
			climb = true;
			ground = false;	
			ladderBelow = instance_place(x, y+vspeed + 1, objLadder);
			x = ladderBelow.x;
			objTopLadder.image_index = 1;
			if shoot == true{
				climbDir = 0;	
			}else{
				climbDir = -1;
			}	
		}else{
			if climb == true{
				climbDir = 0;				
			}
		}
#endregion











}
