// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ProtoManInitial(){
	//Physics variables
	runSpdOriginal = 1.45;
	jumpSpdOriginal = 5.5;
	slideSpdOriginal = 2.75;
	//overrideIndex = 0;
	
	
	runSpd = 1.45;
	jumpSpd = 5.5;
	slideSpd = 2.75;
	grav = 0.25;
	knockBackSpd = 0.2;
	climbSpd = 1.2;
	climbDir = 0;

	x_end = x;
	y_end = y;

	//State variables
	ready = false;
	beginPhase = 0;
	block = false;
	run = false;
	ground = false;
	slide = false;
	shoot = false;
	charge = false;
	stuck = false;
	damaged = false;
	knockBack = false;
	climb = false;
	openDoor = false;
	reflect = false;
	canSlash = false;


	//Sprite variables
	sprStand = spr_proto_man_idle;
	sprRun = spr_proto_man_run;
	sprJump = spr_proto_man_jump;
	sprSlide = spr_proto_man_slide;
	sprStandShoot = spr_proto_man_shoot_stand;
	sprJumpShoot = spr_proto_man_shoot_jump;
	sprRunShoot = spr_proto_man_shoot_run;
	sprClimbShoot = spr_proto_man_shoot_climb;
	sprClimb = spr_proto_man_climb;
	sprClimbStop = spr_proto_man_climb_stop;
	sprClimbUp = spr_proto_man_climb_up;
	sprHit = spr_proto_man_damaged;
	sprTele = spr_proto_man_teleporting;
	sprTransform = spr_proto_man_transforming;
	sprPose = spr_proto_man_pose;
	sprStandSlash = spr_proto_man_stand_slash;
	sprJumpSlash = spr_proto_man_jump_slash;
	
	sprStand_Draw[0] = spr_proto_man_idle_1;
	sprRun_Draw[0] = spr_proto_man_run_1;
	sprJump_Draw[0] = spr_proto_man_jump_1;
	sprSlide_Draw[0] = spr_proto_man_slide_1;
	sprStandShoot_Draw[0] = spr_proto_man_shoot_stand_1;
	sprJumpShoot_Draw[0] = spr_proto_man_shoot_jump_1;
	sprRunShoot_Draw[0] = spr_proto_man_shoot_run_1;
	sprClimbShoot_Draw[0] = spr_proto_man_shoot_climb_1;
	sprClimb_Draw[0] = spr_proto_man_climb_1;
	sprClimbStop_Draw[0] = spr_proto_man_climb_stop_1;
	sprClimbUp_Draw[0] = spr_proto_man_climb_up_1;
	sprHit_Draw[0] = spr_proto_man_damaged_1;
	sprStandSlash_Draw[0] = spr_proto_man_stand_slash_1;
	sprJumpSlash_Draw[0] = spr_proto_man_jump_slash_1;
	
	sprStand_Draw[1] = spr_proto_man_idle_2;
	sprRun_Draw[1] = spr_proto_man_run_2;
	sprJump_Draw[1] = spr_proto_man_jump_2;
	sprSlide_Draw[1] = spr_proto_man_slide_2;
	sprStandShoot_Draw[1] = spr_proto_man_shoot_stand_2;
	sprJumpShoot_Draw[1] = spr_proto_man_shoot_jump_2;
	sprRunShoot_Draw[1] = spr_proto_man_shoot_run_2;
	sprClimbShoot_Draw[1] = spr_proto_man_shoot_climb_2;
	sprClimb_Draw[1] = spr_proto_man_climb_2;
	sprClimbStop_Draw[1] = spr_proto_man_climb_stop_2;
	sprClimbUp_Draw[1] = spr_proto_man_climb_up_2;
	sprHit_Draw[1] = spr_proto_man_damaged_2;
	sprStandSlash_Draw[1] = spr_proto_man_stand_slash_2;
	sprJumpSlash_Draw[1] = spr_proto_man_jump_slash_2;
	
	
	
	sprStand_Draw[2] = spr_proto_man_idle_3;
	sprRun_Draw[2] = spr_proto_man_run_3;
	sprJump_Draw[2] = spr_proto_man_jump_3;
	sprSlide_Draw[2] = spr_proto_man_slide_3;
	sprStandShoot_Draw[2] = spr_proto_man_shoot_stand_3;
	sprJumpShoot_Draw[2] = spr_proto_man_shoot_jump_3;
	sprRunShoot_Draw[2] = spr_proto_man_shoot_run_3;
	sprClimbShoot_Draw[2] = spr_proto_man_shoot_climb_3;
	sprClimb_Draw[2] = spr_proto_man_climb_3;
	sprClimbStop_Draw[2] = spr_proto_man_climb_stop_3;
	sprClimbUp_Draw[2] = spr_proto_man_climb_up_3;
	sprHit_Draw[2] = spr_proto_man_damaged_3;
	sprStandSlash_Draw[2] = spr_proto_man_stand_slash_3;
	sprJumpSlash_Draw[2] = spr_proto_man_jump_slash_3;	
	
	sprStand_Draw[3] = spr_proto_man_idle_4;
	sprRun_Draw[3] = spr_proto_man_run_4;
	sprJump_Draw[3] = spr_proto_man_jump_4;
	sprSlide_Draw[3] = spr_proto_man_slide_4;
	sprStandShoot_Draw[3] = spr_proto_man_shoot_stand_4;
	sprJumpShoot_Draw[3] = spr_proto_man_shoot_jump_4;
	sprRunShoot_Draw[3] = spr_proto_man_shoot_run_4;
	sprClimbShoot_Draw[3] = spr_proto_man_shoot_climb_4;
	sprHit_Draw[3] = spr_proto_man_damaged_4;
	sprStandSlash_Draw[3] = spr_proto_man_stand_slash_4;	
	
	sprStandDart = spr_proto_man_stand_dart;
	sprStandDart_Draw[0] = spr_proto_man_stand_dart_1;
	sprStandDart_Draw[1] = spr_proto_man_stand_dart_2;
	sprStandDart_Draw[2] = spr_proto_man_stand_dart_3;
	sprStandDart_Draw[3] = spr_proto_man_stand_dart_4;
	
	sprJumpDart = spr_proto_man_jump_dart;
	sprJumpDart_Draw[0] = spr_proto_man_jump_dart_2;
	sprJumpDart_Draw[1] = spr_proto_man_jump_dart_1;
	sprJumpDart_Draw[2] = spr_proto_man_jump_dart_3;
	sprJumpDart_Draw[3] = spr_proto_man_jump_dart_4;
	
	sprSlam = spr_proto_man_slam;
	sprSlam_Draw[0] = spr_proto_man_slam_2;
	sprSlam_Draw[1] = spr_proto_man_slam_1;
	sprSlam_Draw[2] = spr_proto_man_slam_3;
	sprSlam_Draw[3] = spr_proto_man_slam_4;

	//image_speed = 0.6;
	sprPrimary = noone;
	sprSecondary = noone;
	sprOutline = noone;
	primaryCol = noone;
	secondaryCol = noone;
	outlineCol = noone;
	sprProtoStrk = noone;

	chargeLv1Color[0] = c_navy;
	chargeLv1Color[1] = make_color_rgb(0, 120, 248);
	chargeLv1Color[2] = c_blue;
	
	chargeLv2Color[0] = make_color_rgb(189, 189, 189);
	chargeLv2Color[1] = make_color_rgb(247, 49, 0);
	chargeLv2Color[2] = c_black; 

	//alarm
	slideAlarm = 25;
	chargeAlarm = 100;
	flashIndex = 1;
	chargeLv2Limit = 85;
	chargeLv3Limit = 100;

	//Create camera
	nextZoneXOffSet = 0;
	nextZoneYOffSet = 0;
	
}