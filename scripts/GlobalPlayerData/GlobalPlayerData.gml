// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function GlobalPlayerData(){
	
	global.weaponName = array_create(10);
	global.weaponUnlocked = array_create(10);
	global.weaponEnergy = array_create(10);
	global.weaponAvt = array_create(10);
	global.weaponColor = array_create(10);
	global.weaponIndex = 0;
	

	array_set(global.weaponName, 0,"Buster");
	array_set(global.weaponUnlocked, 0,true);
	

	array_set(global.weaponName, 1,"Q.Boomerang");
	array_set(global.weaponUnlocked, 1,true);
	array_set(global.weaponEnergy, 1,27);
	array_set(global.weaponAvt, 1,spr_weapon_quick_boomerang);
	array_set(global.weaponColor, 1,make_color_rgb(252, 116, 96));

	array_set(global.weaponName, 2,"H.Missile");
	array_set(global.weaponUnlocked, 2,true);
	array_set(global.weaponEnergy, 2,27);
	array_set(global.weaponAvt, 2,spr_weapon_homing_missile);
	array_set(global.weaponColor, 2,make_color_rgb(0, 120, 248));

	array_set(global.weaponName, 3,"S.Blade");
	array_set(global.weaponUnlocked, 3,true);
	array_set(global.weaponEnergy, 3,27);
	array_set(global.weaponAvt, 3,spr_weapon_shadow_blade);
	array_set(global.weaponColor, 3,make_color_rgb(102, 45,145));
	
	array_set(global.weaponName, 4,"S.Blast");
	array_set(global.weaponUnlocked, 4,true);
	array_set(global.weaponEnergy, 4,27);
	array_set(global.weaponAvt, 4,spr_weapon_solar_blast);
	array_set(global.weaponColor, 4,make_color_rgb(255, 160,68));
	
	array_set(global.weaponName, 5,"S.Slash");
	array_set(global.weaponUnlocked, 5,true);
	array_set(global.weaponEnergy, 5,27);
	array_set(global.weaponAvt, 5, spr_weapon_sonic_slash);
	array_set(global.weaponColor, 5,make_color_rgb(0, 168,0));
	
	array_set(global.weaponName, 6,"I.Slasher");
	array_set(global.weaponUnlocked, 6,true);
	array_set(global.weaponEnergy, 6,27);
	array_set(global.weaponAvt, 6, spr_weapon_ice_slasher);
	array_set(global.weaponColor, 6,make_color_rgb(60, 188,155));
	
	array_set(global.weaponName, 7,"F.Tornado");
	array_set(global.weaponUnlocked, 7,true);
	array_set(global.weaponEnergy, 7,27);
	array_set(global.weaponAvt, 7, spr_weapon_flame_tornado);
	array_set(global.weaponColor, 7,make_color_rgb(198, 0,0));
	
	PlayerFeatureSet();
	
	array_set(global.weaponUnlocked, 8,true);
	array_set(global.weaponEnergy, 8, 27);
	
	array_set(global.weaponUnlocked, 9,true);
	array_set(global.weaponEnergy, 9, 27);
}