// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerTileCollision(){
	spr_bbox_left = sprite_get_bbox_left(sprite_index) - sprite_get_xoffset(sprite_index);
	spr_bbox_right = sprite_get_bbox_right(sprite_index) - sprite_get_xoffset(sprite_index);
	spr_bbox_top = sprite_get_bbox_top(sprite_index) - sprite_get_yoffset(sprite_index);
	spr_bbox_bottom = sprite_get_bbox_bottom(sprite_index) - sprite_get_yoffset(sprite_index);
	
	if xspeed > 0 or eff_xspeed > 0{
		//var t1 = tilemap_get_at_pixel(tilemap, bbox_left-1, bbox_bottom) & tile_index_mask;
		var t2 = tilemap_get_at_pixel(tilemap, bbox_right+1, bbox_bottom) & tile_index_mask;
		
		if t2 != 0 {
			x = ((bbox_right & ~15) + 1) - spr_bbox_right;
			xspeed = 0;
			run = false;
		}	
	}else if xspeed < 0 or eff_xspeed < 0{
		var t1 = tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom) & tile_index_mask;
		//var t2 = tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom) & tile_index_mask;
		
		if t1 != 0{
			x = (((bbox_left + 16) & ~15) - 1) - spr_bbox_left;
			xspeed = 0;
			run = false;
		}		
	}
	
	if vspeed > 0{
		var t1 = tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom) & tile_index_mask;
		var t2 = tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom) & tile_index_mask;
		
		if t1 != 0 or t2 != 0 {
			y = ((bbox_bottom & ~ 14) - 1) - spr_bbox_bottom;
			ground = true;
		}
	}else{
		var t1 = tilemap_get_at_pixel(tilemap, bbox_left, bbox_top) & tile_index_mask;
		var t2 = tilemap_get_at_pixel(tilemap, bbox_right, bbox_top) & tile_index_mask;
		
		if t1 != 0 or t2 != 0 {
			y = (((bbox_top + 16) & ~14) + 1) - spr_bbox_top;
			vspeed = 0;
		}	
	}
	
	
	
	
}