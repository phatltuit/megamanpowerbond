// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerBulletReflect(){
	if reflect == true 
	and (place_meeting(x,y,objQuickManMainBoomerang) 
	or (place_meeting(x,y,objMagmaDragonFlame) 
	and objMagmaDragonFlame.sprite_index == spr_magma_dragon_flame_aura)){	
		vspeed = -abs(hspeed);
		hspeed = -hspeed;
		reflect = false;
	}
}