// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function EnemiesBulletReflect(){
	if object_index == objProtoMan{
		if sprite_index == spr_proto_man_idle or sprite_index == spr_proto_man_jump{
			var bullet = collision_rectangle(x+image_xscale*13,bbox_top,x+image_xscale*13,bbox_bottom, objEnemyWeapon, true, true);	
			if bullet>= 0  and reflect == false{
				if ((bullet.hspeed < 4 and bullet.hspeed > -4)
				or (bullet.speed < 4 and bullet.speed > -4)
				or (bullet.vspeed < 4 and bullet.vspeed > -4))
				and (image_xscale != sign(bullet.hspeed)
				or image_xscale != sign(bullet.speed)
				or image_xscale != sign(bullet.vspeed)){
					instance_destroy(bullet);
					audio_play_sound(EnemyBlock, 1, false);
				}
				alarm[6] = 5;
				reflect = true;
			}
		}
	}
}