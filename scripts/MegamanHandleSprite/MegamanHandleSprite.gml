// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function MegamanHandleSprite(){
	//Sprite variables
	sprStand = spr_mega_man_idle;
	sprRun = spr_mega_man_run;
	sprJump = spr_mega_man_jump;
	sprSlide = spr_mega_man_slide;
	sprStandShoot = spr_mega_man_shoot_stand;
	sprJumpShoot = spr_mega_man_shoot_jump;
	sprRunShoot = spr_mega_man_shoot_run;
	sprClimbShoot = spr_mega_man_shoot_climb;
	
	sprClimb = spr_mega_man_climb;
	sprClimbStop = spr_mega_man_climb_stop;
	sprClimbUp = spr_mega_man_climb_up;
	sprHit = spr_mega_man_damaged;
	sprTele = spr_mega_man_teleporting;
	sprTransform = spr_mega_man_transforming;
	sprPose = spr_mega_man_pose;
	sprStandSlash = spr_mega_man_stand_slash;
	sprJumpSlash = spr_mega_man_jump_slash;
	
	sprStand_Draw[0] = spr_mega_man_idle_2;
	sprRun_Draw[0] = spr_mega_man_run_2;
	sprJump_Draw[0] = spr_mega_man_jump_2;
	sprSlide_Draw[0] = spr_mega_man_slide_2;
	sprStandShoot_Draw[0] = spr_mega_man_shoot_stand_2;
	sprJumpShoot_Draw[0] = spr_mega_man_shoot_jump_2;
	sprRunShoot_Draw[0] = spr_mega_man_shoot_run_2;
	sprClimbShoot_Draw[0] = spr_mega_man_shoot_climb_2;
	sprClimb_Draw[0] = spr_mega_man_climb_2;
	sprClimbStop_Draw[0] = spr_mega_man_climb_stop_2;
	sprClimbUp_Draw[0] = spr_mega_man_climb_up_2;
	sprHit_Draw[0] = spr_mega_man_damaged_2;	
	sprStandSlash_Draw[0] = spr_mega_man_stand_slash_2;
	sprJumpSlash_Draw[0] = spr_mega_man_jump_slash_2;
	
	sprStand_Draw[1] = spr_mega_man_idle_1;
	sprRun_Draw[1] = spr_mega_man_run_1;
	sprJump_Draw[1] = spr_mega_man_jump_1;
	sprSlide_Draw[1] = spr_mega_man_slide_1;
	sprStandShoot_Draw[1] = spr_mega_man_shoot_stand_1;
	sprJumpShoot_Draw[1] = spr_mega_man_shoot_jump_1;
	sprRunShoot_Draw[1] = spr_mega_man_shoot_run_1;
	sprClimbShoot_Draw[1] = spr_mega_man_shoot_climb_1;

	sprClimb_Draw[1] = spr_mega_man_climb_1;
	sprClimbStop_Draw[1] = spr_mega_man_climb_stop_1;
	sprClimbUp_Draw[1] = spr_mega_man_climb_up_1;
	sprHit_Draw[1] = spr_mega_man_damaged_1;
	sprStandSlash_Draw[1] = spr_mega_man_stand_slash_1;
	sprJumpSlash_Draw[1] = spr_mega_man_jump_slash_1;
	
	sprStand_Draw[2] = spr_mega_man_idle_3;
	sprRun_Draw[2] = spr_mega_man_run_3;
	sprJump_Draw[2] = spr_mega_man_jump_3;
	sprSlide_Draw[2] = spr_mega_man_slide_3;
	sprStandShoot_Draw[2] = spr_mega_man_shoot_stand_3;
	sprJumpShoot_Draw[2] = spr_mega_man_shoot_jump_3;
	sprRunShoot_Draw[2] = spr_mega_man_shoot_run_3;
	sprClimbShoot_Draw[2] = spr_mega_man_shoot_climb_3;
	
	sprClimb_Draw[2] = spr_mega_man_climb_3;
	sprClimbStop_Draw[2] = spr_mega_man_climb_stop_3;
	sprClimbUp_Draw[2] = spr_mega_man_climb_up_3;
	sprHit_Draw[2] = spr_mega_man_damaged_3;
	sprStandSlash_Draw[2] = spr_mega_man_stand_slash_3;
	sprJumpSlash_Draw[2] = spr_mega_man_jump_slash_3;
	
	sprStandDart = spr_mega_man_stand_dart;
	sprStandDart_Draw[0] = spr_mega_man_stand_dart_2;
	sprStandDart_Draw[1] = spr_mega_man_stand_dart_1;
	sprStandDart_Draw[2] = spr_mega_man_stand_dart_3;
	
	sprJumpDart = spr_mega_man_jump_dart;
	sprJumpDart_Draw[0] = spr_mega_man_jump_dart_2;
	sprJumpDart_Draw[1] = spr_mega_man_jump_dart_1;
	sprJumpDart_Draw[2] = spr_mega_man_jump_dart_3;
	
	sprSlam = spr_mega_man_slam;
	sprSlam_Draw[0] = spr_mega_man_slam_2;
	sprSlam_Draw[1] = spr_mega_man_slam_1;
	sprSlam_Draw[2] = spr_mega_man_slam_3;
	
	chargeLv1Color[0] = make_color_rgb(168, 0, 32); //Light Pink
	chargeLv1Color[1] = make_color_rgb(228, 0, 88); //Medium Pink
	chargeLv1Color[2] = make_color_rgb(248, 88, 152); //Hard Pink
	
	chargeLv2Color[0] = make_color_rgb(0, 232, 216); //Light Blue
	chargeLv2Color[1] = make_color_rgb(0, 120, 248); //Blue
	chargeLv2Color[2] = c_black; //Black
}