// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossProjectilesDirectToPlayer(argument0, argument1){
	var spd = argument0;
	var shot = argument1;
	if instance_exists(objPlayer){
		shot.speed = spd;
		shot.direction = point_direction(x,y,objPlayer.x,objPlayer.y);		
	}else{
		shot.hspeed = spd*image_xscale;	
	}
}