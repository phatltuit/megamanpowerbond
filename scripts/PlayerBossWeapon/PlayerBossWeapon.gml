function PlayerBossWeapon(argument0) {
	var bulletYStart = (object_index == objProtoMan)?0.5:4;
	var bulletXStart = (image_xscale == 1?bbox_right:bbox_left)  + image_xscale*(object_index == objProtoMan?5:9);
	var recoilXStart = (object_index == objProtoMan)?10*image_xscale:20*image_xscale;
	var objWeapon = objPlayerWeapon;
	
	
	switch (argument0) {
		case BossWeapon.IceSlasher:
			if instance_number(objWeapon) < 1
			and global.weaponEnergy[global.weaponIndex] > 0{
				global.weaponEnergy[global.weaponIndex]-=1;
				var shot = instance_create_depth(x, y, 0, objWeapon);
				shot.sprite_index = spr_ice_slasher;	
				shot.image_xscale = image_xscale;
				shot.hspeed = 5*image_xscale;
				shot.alarm[0] = 20;		
				slam = true;
			}
			break;
		
	    case BossWeapon.ShadowBlade:
			if instance_number(objWeapon) < 2
			and global.weaponEnergy[global.weaponIndex] > 0{
				global.weaponEnergy[global.weaponIndex]-=1;
				var shot = instance_create_depth(bulletXStart, y - bulletYStart, 0, objWeapon);					
				var time = clamp((75-chargeAlarm)/2,20,50);
				shot.timer = time;
				shot.sprite_index = spr_shadow_blade;
				if keyboard_check_direct(global.keyUp){
					shot.vspeed = -4;
					shot.hspeed = 4*image_xscale;	
				}if keyboard_check_direct(global.keyDown){
					shot.vspeed = 4;
					shot.hspeed = 4*image_xscale;	
				}else{
					shot.hspeed = sqrt(32)*image_xscale;		
				}
				
				shot.image_xscale = image_xscale;				
			}
			
			break;
			
		case BossWeapon.HomingMissile:
			if instance_number(objWeapon) < 2 
			and global.weaponEnergy[global.weaponIndex] > 0{
				global.weaponEnergy[global.weaponIndex]-=1;
				var recoil = instance_create_depth (x+recoilXStart, y - bulletYStart, 0, objWeaponRecoil);
				recoil.sprite_index = spr_mini_recoil;
				recoil.image_xscale = image_xscale;									
				var shot = instance_create_depth(bulletXStart  + image_xscale*3, y - bulletYStart, 0, objWeapon);	
				shot.sprite_index = spr_homing_missle;
				shot.image_xscale = image_xscale;
				shot.hspeed = 2.5*image_xscale;					
			}
			break;
			
		case BossWeapon.QuickBoomerang:
			if global.weaponEnergy[global.weaponIndex] > 0{
				if !instance_exists(objTarget){					
					global.weaponEnergy[global.weaponIndex]-=1;
					i = 30;
					var centerPoint = instance_create_depth(x, y - bulletYStart, 0, objTarget);
					repeat 2{						
						var shot = instance_create_depth(x+i, y, 0, objWeapon);
						shot.sprite_index = spr_quick_boomerang;
						shot.radius = point_distance(x, y, shot.x, shot.y);
						shot.phase = point_direction(x, y, shot.x, shot.y);
						shot.target = centerPoint;
						i = -30;
					}
					i = 30;
					repeat 2{
						var shot = instance_create_depth(x, y+i, 0, objWeapon);
						shot.sprite_index = spr_quick_boomerang;
						shot.radius = point_distance(x, y, shot.x, shot.y);
						shot.phase = point_direction(x, y, shot.x, shot.y);
						shot.target = centerPoint;
						i = -30;
					}
					shoot = true;
				}else{					
					for (var i = 0; i < instance_number(objTarget); ++i) {
						if instance_find(objTarget, i).follow == true{
							instance_find(objTarget, i).follow = false;
							instance_find(objTarget, i).hspeed = 4*image_xscale;
						}
					}										
				}				
			}
			break;
			
		case BossWeapon.SolarBlast:
			if instance_number(objPlayerWeapon) < 2
			and global.weaponEnergy[global.weaponIndex] > 0{					
				if chargeAlarm <= 0{
					var shot = instance_create_depth(x, y - bulletYStart, -2, objWeapon);
					shot.sprite_index = spr_solar_blast_b;
					shot.hspeed = 4.5;
					shot.image_xscale = 1;					
									
					var shot = instance_create_depth(x, y - bulletYStart, -2, objWeapon);
					shot.sprite_index = spr_solar_blast_b;
					shot.hspeed = -4.5;						
					shot.image_xscale = -1;
					
					global.weaponEnergy[global.weaponIndex]-=4;					
				}else{
					var shot = instance_create_depth(bulletXStart, y - bulletYStart, -2, objWeapon);
					shot.sprite_index = spr_solar_blast_a;
					shot.hspeed = 2*image_xscale;
					global.weaponEnergy[global.weaponIndex]-=1;
					shot.image_xscale = image_xscale;
				}
			}			
			break;
			
		case BossWeapon.SonicSlash:
			if instance_number(objPlayerWeapon) < 1
			and global.weaponEnergy[global.weaponIndex] > 0{
				var shot = instance_create_depth(bulletXStart, y - bulletYStart, -2, objWeapon);
				shot.sprite_index = spr_sonic_slash;
				shot.image_xscale = image_xscale;
				global.weaponEnergy[global.weaponIndex]-=1;
				run = false;
			}
			break;
			
		case BossWeapon.FlameTornado:
			if instance_number(objPlayerWeapon) < 1
			and global.weaponEnergy[global.weaponIndex] > 0{
				if chargeAlarm <= 0{
					var i = -1;
					repeat 2{
						var shot = instance_create_depth(x + 40*i, y, -2, objWeapon);
						shot.sprite_index = spr_flame_tornado;
						shot.hspeed = 4*i;
						shot.image_angle = 90;
						shot.alarm[0] = 120;
						
						var shot = instance_create_depth(x + 40*i, y-48, -2, objWeapon);
						shot.sprite_index = spr_flame_tornado;
						shot.hspeed = 4*i;
						shot.image_angle = 90;
						shot.alarm[0] = 120;
						
						
						i=1;
					}
					global.weaponEnergy[global.weaponIndex]-=4;
				}else{
					var shot = instance_create_depth(x, y-2, -2, objWeapon);
					shot.sprite_index = spr_flame_tornado;	
					shot.hspeed = 3*image_xscale;
					shot.image_xscale = image_xscale;
					global.weaponEnergy[global.weaponIndex]-=1;
				}
			}
			break;
			
	    default:
	        // code here
	        break;
	}


}
