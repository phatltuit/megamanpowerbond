// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SoundList(){
	global.MusicArr = [
					MagmaDragoonStageMusic,	
					BossBattle, 
					Defeated, 
					CapcomLogo, 
					Defeated, 
					IntroStageSound, 
					PlayerSelect, 
					Title_1, 
					Title_2,
					Victory];
	global.SFXArr = [	
					sfxError,
					sfxMenuMove,
					sfxMenuSelect,
					sfxPause,	
					EnemyBlock, 
					EnergyUp, 
					HealthUp, 
					PlayerCharge, 
					PlayerDamaged, 
					PlayerDefeat, 
					PlayerHitEnemy, 
					PlayerLand,
					PlayerShootLv1,
					PlayerShootLv2,
					PlayerShootLv3,
					PlayerteleportIn,
					PlayerTeleportOut,
					PlayerVictoryPose,
					WaterSplash,
					WeaponChange];
	
}