// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerWeapon(){
	switch (sprite_index) {
		case spr_ice_slasher:	
			if instance_exists(objPlayer){				
				objPlayer.gravity = 0;
				objPlayer.xspeed = hspeed;
			}else{
				instance_destroy();	
			}
			damage = 2;
			break;
		
		case spr_flame_tornado:	
			if instance_exists(objPlayer){
				if image_angle == 90{
					if hspeed != 0{
						if x > objPlayer.x + 40{
							x = objPlayer.x - 40;	
						}else if x < objPlayer.x - 40{
							x = objPlayer.x + 40;
						}					
					}
				}
			}
			damage = 2;
			break;
		
		case spr_sonic_slash:
			if image_index + image_speed >= image_number{
				instance_destroy();	
			}
			damage = 3;
	        break;
			
		
		case spr_solar_blast_a:
			PlayerBulletReflect();
	        damage = 2;
	        break;
			
		case spr_solar_blast_b:
			PlayerBulletReflect();
	        damage = 4;
	        break;
			
			
		
	    case spr_bass_mini_buster:
			PlayerBulletReflect();
	        damage = 1;
	        break;
		case spr_bass_mega_buster_0:
		case spr_bass_mega_buster_1:
		case spr_bass_mega_buster_2:
		case spr_bass_mega_buster_3:
			PlayerBulletReflect();
			damage = 2;
			break;
		
		case spr_bass_stand_saber:
			damage = 2;		
			if instance_exists(objPlayer){
				x = objPlayer.x;
				y = objPlayer.y;
				if objPlayer.slash == false  or objPlayer.knockBack == true{
					instance_destroy();	
				}	
			}else{
				instance_destroy();	
			}
			break;
		
		case spr_bass_jump_saber:
			damage = 2;		
			if instance_exists(objPlayer){
				x = objPlayer.x;
				y = objPlayer.y;
				if objPlayer.airSlash == false or objPlayer.knockBack == true{
					instance_destroy();	
				}	
			}else{
				instance_destroy();	
			}
			break;			
		
		case spr_mega_man_mini_buster:
			PlayerBulletReflect();
	        damage = 1;
			
	        break;
		
		case spr_proto_man_mega_buster:
		case spr_mega_man_mega_buster:
	        damage = 2;
			event_inherited();
	        break;
			
		case spr_proto_man_giga_buster:
		case spr_mega_man_giga_buster:
	        damage = 3;
			event_inherited();
	        break;
		
		case spr_proto_man_stand_saber:
		case spr_mega_man_stand_saber:
			damage = 2;		
			if instance_exists(objPlayer){
				x = objPlayer.x;
				y = objPlayer.y;
				if objPlayer.slash == false  or objPlayer.knockBack == true{
					instance_destroy();	
				}	
			}else{
				instance_destroy();	
			}
			break;
		
		case spr_proto_man_jump_saber:
		case spr_mega_man_jump_saber:
			damage = 2;		
			if instance_exists(objPlayer){
				x = objPlayer.x;
				y = objPlayer.y;
				if objPlayer.airSlash == false or objPlayer.knockBack == true{
					instance_destroy();	
				}	
			}else{
				instance_destroy();	
			}
			break;
			
		case spr_shadow_blade:
			PlayerBulletReflect();
			image_angle-=image_xscale*10;
			damage = 2;			
			timer--;
			if timer <= 0{
				speed = 4;
				if instance_exists(objPlayer){
					direction = point_direction(x,y, objPlayer.x,objPlayer.y);	
				}
				
				if place_meeting(x,y,objPlayer) or !instance_exists(objPlayer){
					instance_destroy();	
				}
			}
			break;
			
		case spr_homing_missle:
			if place_meeting(x,y,objMagmaDragonFlame) and objMagmaDragonFlame.sprite_index == spr_magma_dragon_flame_aura{
				instance_destroy();	
			}
			damage = 2;				
			if target != noone and target.visible == true{
				radius = 0;
				aim = instance_create_depth(x,y,-2,objTarget);
				aim.target = target;
				speed = 4;
				image_xscale = target.x < x?-1:1;
				direction = point_direction(x,y, target.x, target.y);
				image_angle = point_direction(x,y, target.x, target.y);
				//image_angle -= image_xscale*clamp(angle_difference(image_angle, direction) * dmp, -speed, speed); 
			}else{
				radius += 16;
				if radius >= 256{
					radius = 0;	
				}	
				var myEnemy = collision_circle(x,y,radius,objEnemy,false, true);
				var myBoss = collision_circle(x,y,radius,objBoss,false, true);
				if myEnemy >= 0 and myEnemy.enable == true{
					target = myEnemy;
				}else if myBoss >= 0 and myBoss.ready == true{
					target = myBoss;
				}	
			}
			break;
			
		case spr_explosion:
			damage = 1;
			break;
			
		case spr_quick_boomerang:			
			damage = 2;	
			var enemyWp  = instance_place(x,y,objEnemyWeapon)
			if enemyWp >= 0{			
				instance_destroy(enemyWp);
				audio_play_sound(EnemyBlock, 1, false);
				instance_destroy();	
			}
			
			if target != noone{
				x = target.x + lengthdir_x(radius, phase);
				y = target.y + lengthdir_y(radius, phase);
				phase+=20;
			}else{
				instance_destroy();	
			}
			
			break;
		
		case spr_mini_power_shot:
			damage = 2;
			break;
			
		case spr_mega_power_shot:
			damage = 6;
			break;
				
		
	
	    default:
	        damage = 1;
	        break;
	}
}