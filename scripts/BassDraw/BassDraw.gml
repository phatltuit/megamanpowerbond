function BassDraw() {
	draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
	if charge == true and sprite_index != sprPose and IsChargableWeapon() {	
		BassDrawSprite();
		if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{
			draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
			draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
			draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);
		}else if chargeAlarm < 0{
			draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
			draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
			draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);
		}else{
			draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
			draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
			draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);	
		}
	}else{
		BassDrawSprite();
		if sprPrimary != noone and sprSecondary != noone and sprOutline != noone and global.weaponIndex != 8 and global.weaponIndex != 9{
			PlayerColor();
			draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
			draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
			draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);
		}
	}
	
	#region retired feature
	//if override == true{	
	//	shadow = part_type_create();
			
	//	ss[0] = spr_bass_stand_shoot_a;
	//	ss[1] = spr_bass_stand_shoot_b;
	//	ss[2] = spr_bass_stand_shoot_c;
	//	ss[3] = spr_bass_stand_shoot_d;
			
	//	js[0] = spr_bass_jump_shoot_a;
	//	js[1] = spr_bass_jump_shoot_b;
	//	js[2] = spr_bass_jump_shoot_c;
	//	js[3] = spr_bass_jump_shoot_d;
			
	//	cs[0] = spr_bass_climb_shoot_a;
	//	cs[1] = spr_bass_climb_shoot_b;
	//	cs[2] = spr_bass_climb_shoot_c;
			
	//	switch (sprite_index) {
	//		case sprStandShoot:
	//		    part_type_sprite(shadow, ss[image_index], 0, 0, 0);
	//		    break;
					
	//		case sprClimbShoot:
	//			part_type_sprite(shadow, cs[image_index], 0, 0, 0);
	//		    break;
					
	//		case sprJumpShoot:
	//			part_type_sprite(shadow, js[image_index], 0, 0, 0);
	//		    break;
					
	//		default:
	//		    part_type_sprite(shadow, sprite_index, 0, 0, 0);
	//		    break;
	//	}
		
	//	part_type_size(shadow, 1.1, 1.1, 0 ,0);		
	//	part_type_scale(shadow,image_xscale, image_yscale);
	//	part_type_life(shadow, 3, 3);
	//	part_type_blend(shadow, true);
	//	part_type_alpha1(shadow, 1);
	//	part_type_color1(shadow, c_purple);		
	//	part_particles_create_color(global.particleSystem, x, y, shadow, c_purple,10);
	//}
	#endregion
}
