function BassButtonPress() {
#region Moving Left/Right
	if KeyboardCheck(global.keyLeft) {
		image_xscale = -1;	
		if !PlayerCollision(CollisionType.Left, objSolid){
			if dash == true{
				run = false;	
			}else{
				run = true;	
			}
		}else{
			run = false;	
		}
	}else if KeyboardCheck(global.keyRight) {
		image_xscale = 1;	
		if !PlayerCollision(CollisionType.Right, objSolid){
			if dash == true{
				run = false;	
			}else{
				run = true;	
			}
		}else{
			run = false;	
		}
	}else{
		run = false;	
	}
#endregion

#region Dashing

	if ground == true and KeyboardCheck(global.keyDash) and dash == false{
		if image_xscale == 1 and !PlayerCollision(CollisionType.Left, objSolid){
			dash = true;
			alarm[2] = dashAlarm;
			mask_index = spr_bass_dash_mask;	
		}else if image_xscale == -1 and !PlayerCollision(CollisionType.Right, objSolid){
			dash = true;
			alarm[2] = dashAlarm;
			mask_index = spr_bass_dash_mask;
		}else{
			dash = false;	
			hspeed = 0;
		}
	
	}
#endregion

#region Jumping
	if (ground == true or (canDoubleJump == true and jumpTime > 0) or climb == true) and KeyboardCheck(global.keyJump) and !place_meeting(x, y - 5, objSolid){
		if dash == true{
			dash = false;
			hyperSpeed = true;
			runSpd = dashJumpSpd; 
		}
		if climb == true{
			vspeed = 0;
			climb = false;
		}else{
			vspeed = -jumpSpd;	
		}
		ground = false;	
		jumpTime-=1;
	
	}
#endregion

#region Shooting

	if keyboard_check_direct(global.keyShootBass) and shoot == false{
		shoot = true;
		image_speed = 0;
		BassShootDirection();
		if currentWeapon == BassShot.MiniShot{	
			alarm[3] = 10;
			BassShoot();
		}
	}

	if shoot == false{
		image_speed = 1;	
	}
#endregion

#region Climbing
		if KeyboardCheck(global.keyUp) and instance_place(x, y+vspeed - 1, objLadder) != noone{
			climb = true;
			ground = false;	
			ladderBelow = instance_place(x, y+vspeed - 1, objLadder);
			x = ladderBelow.x;
			if shoot == true{
				climbDir = 0;	
			}else{
				climbDir = 1;
			}
		}else if KeyboardCheck(global.keyDown) and instance_place(x, y+vspeed + 1, objLadder) != noone{
			climb = true;
			ground = false;	
			ladderBelow = instance_place(x, y+vspeed + 1, objLadder);
			x = ladderBelow.x;		
			objTopLadder.image_index = 1;
			if shoot == true{
				climbDir = 0;	
			}else{
				climbDir = -1;
			}	
		}else{
			if climb == true{
				climbDir = 0;				
			}
		}

#endregion








}
