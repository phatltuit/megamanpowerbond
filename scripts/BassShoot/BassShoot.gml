function BassShoot() {	
	var bulletX, bulletY,bulletSprite, bulletDir;
	var bulletSpd = 5;
	bulletDir = image_xscale==1?0:180;
	switch (shootDirection) {
	    case 0:
			bulletSprite = spr_bass_mega_buster_0;
			if dash == false{		
				bulletDir = image_xscale==1?0:180;
				if ground == false{
					bulletY = y - 6;
					bulletX = x + 13*image_xscale;
				}else{
					bulletDir = image_xscale==1?0:180;
					bulletY = y - 5;
					bulletX = x + 20*image_xscale;
				}
			}else{
				bulletY = y + 1;
				bulletX = x + 19*image_xscale;
			}		
	        break;
		
		case 1:	
			bulletSprite = spr_bass_mega_buster_1;
			bulletDir = image_xscale==1?45:135;
			if ground == true{		
				bulletY = y - 14;
				bulletX = x + 17*image_xscale;
			}else{
				bulletY = y - 13;
				bulletX = x + 11*image_xscale;
			}

	        break;
		
		case 3:       
			bulletSprite = spr_bass_mega_buster_3;
			bulletDir = 90;
			if ground == true{
				bulletY = y - 19;
				bulletX = x + 4*image_xscale;		
			}else{
				bulletY = y - 18;
				bulletX = x + 4*image_xscale;	
			}
	        break;
		
		case 2:	
			bulletSprite = spr_bass_mega_buster_2;
			bulletDir = image_xscale==1?315:225;
			if ground == true{
		        bulletY = y + 4;
				bulletX = x + 17*image_xscale;
			}else{
				bulletY = y + 3;
				bulletX = x + 13*image_xscale;
			}
	        break;
		
		default:
			bulletSprite = spr_bass_mega_buster_0;
			if dash == false{		
				bulletDir = image_xscale==1?0:180;
				if ground == false{
					bulletY = y - 6;
					bulletX = x + 13*image_xscale;
				}else{
					bulletY = y - 5;
					bulletX = x + 20*image_xscale;
				}
			}else{
				bulletDir = image_xscale==1?0:180;
				bulletY = y + 1;
				bulletX = x + 19*image_xscale;
			}		
	        break;
	}	
	
	bulletSprite = spr_bass_mini_buster;
	var bullet = instance_create_depth(bulletX, bulletY, 0,objPlayerWeapon);	
	bullet.speed = bulletSpd;
	bullet.direction = bulletDir;
	bullet.image_xscale = image_xscale;
	bullet.sprite_index = bulletSprite;
	audio_play_sound(PlayerShootLv1, 1,false);
}
