// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSupporters(argument0){
switch (argument0) {
    case Support.RushCoil:
		if !instance_exists(objSupporter) and global.weaponEnergy[global.weaponIndex] > 0{			
			var supporter = instance_create_depth(x,y,0, objRush);
			supporter.supportType = Support.RushCoil;			
			supporter.image_xscale = image_xscale;
			global.weaponEnergy[global.weaponIndex]-=1
		}		
        // code here
        break;

	case Support.ProtoCoil:
		if !instance_exists(objSupporter) and global.weaponEnergy[global.weaponIndex] > 0{			
			var supporter = instance_create_depth(x,y,0, objProtoTool);	
			supporter.supportType = Support.ProtoCoil; 	
			supporter.image_xscale = image_xscale;
			global.weaponEnergy[global.weaponIndex]-=1
		}		
        // code here
        break;
		
	case Support.TrebleCoil:
		if !instance_exists(objSupporter) and global.weaponEnergy[global.weaponIndex] > 0{			
			var supporter = instance_create_depth(x,y,0, objTreble);
			supporter.supportType = Support.TrebleCoil; 	
			supporter.image_xscale = image_xscale;
			global.weaponEnergy[global.weaponIndex]-=1
		}		
        // code here
        break;
	
	case Support.RushJet:
		if !instance_exists(objSupporter) and global.weaponEnergy[global.weaponIndex] > 0{ 				
			var supporter = instance_create_depth(x,y,0, objRush);
			supporter.supportType = Support.RushJet;
			supporter.image_xscale = image_xscale;
		}		
        // code here
        break;
		
	case Support.ProtoJet:
		if !instance_exists(objSupporter) and global.weaponEnergy[global.weaponIndex] > 0{ 				
			var supporter = instance_create_depth(x,y,0, objProtoTool);	
			supporter.supportType = Support.ProtoJet; 	
			supporter.image_xscale = image_xscale;
		}		
        // code here
        break;
		
	case Support.TrebleJet:
		if !instance_exists(objSupporter) and global.weaponEnergy[global.weaponIndex] > 0{ 				
			var supporter = instance_create_depth(x,y,0, objTreble);
			supporter.supportType = Support.TrebleJet;
			supporter.image_xscale = image_xscale;
		}		
        // code here
        break;
		
	
    default:
        // code here
        break;
}
}