function PlayerCameraInit() {
	//Left
	dist = 0;
	while !place_meeting(floor((x+nextZoneXOffSet)/16)*16 - dist, y, objZoneLeft) and dist <= 16 * 2000{
		dist += 16;				  
	}
	if dist >= 16 * 2000{
		show_message("Cannot find the Left side end of Section !");
	}
	sectionLeft = instance_place(floor((x+nextZoneXOffSet)/16)*16 - dist, y, objZoneLeft).x;

	//Right
	dist = 0;
	while !place_meeting(ceil((x+nextZoneXOffSet)/16)*16 + dist , y, objZoneRight) and dist <= 16 * 2000{
		dist += 16;
	}
	if dist >= 16 * 2000{
		show_message("Cannot find the Right side end of Section !");
	}
	sectionRight = instance_place(ceil((x+nextZoneXOffSet)/16)*16 + dist, y, objZoneRight).x;

	//Ceil
	dist = 0;
	while !place_meeting(x, floor((y+nextZoneYOffSet)/16)*16 - dist, objZoneCeil) and dist <= 16 * 2000{
		dist += 16;
	}
	if dist >= 16 * 2000{
		show_message("Cannot find the Up side end of Section !");
	}
	sectionCeil = instance_place(x, floor((y+nextZoneYOffSet)/16)*16 - dist, objZoneCeil).y;

	//Floor
	dist = 0;
	while !place_meeting(x, ceil((y+nextZoneYOffSet)/16)*16 + dist, objZoneFloor) and dist <= 16 * 2000{
		dist += 16;
	}
	if dist >= 16 * 2000{
		show_message("Cannot find the Down side end of Section !");
	}
	sectionFloor = instance_place(x, ceil((y+nextZoneYOffSet)/16)*16 + dist, objZoneFloor).y;
}
