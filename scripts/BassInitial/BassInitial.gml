function BassInitial() {
	//Physics variables
	runSpd = 1.45;
	jumpSpd = 5.5;
	dashSpd = 2.75;
	grav = 0.25;
	knockBackSpd = 0.2;
	jumpTime = 2;
	dashJumpSpd = 2.75;
	climbSpd = 1.2;
	climbDir = 0;

	x_end = x;
	y_end = y;


	//State variables
	ready = false;
	block = false;
	beginPhase = 0;
	run = false;
	ground = false;
	dash = false;
	shoot = false;
	stuck = false;
	damaged = false;
	knockBack = false;
	climb = false;
	canDoubleJump = true;
	hyperSpeed = false;
	openDoor = false;
	canSlash = false;


	//Sprite variables
	sprStand = spr_bass_stand;
	sprRun = spr_bass_run;
	sprJump = spr_bass_jump;
	sprDash = spr_bass_dash;
	sprDashShoot = spr_bass_dash_shoot;	
	
	sprClimb = spr_bass_climb;
	sprClimbStop = spr_bass_climb_stop;
	sprClimbUp = spr_bass_climb_up;
	sprHit = spr_bass_damaged;
	sprTele = spr_bass_teleporting;
	sprTransform = spr_bass_transforming;
	sprPose = spr_bass_pose;
	sprStandSlash = spr_bass_stand_slash;
	sprJumpSlash = spr_bass_jump_slash;
	//image_speed = 0.6;

	sprStandShoot[0] = spr_bass_stand_shoot_a;
	sprStandShoot[1] = spr_bass_stand_shoot_b;
	sprStandShoot[2] = spr_bass_stand_shoot_c;
	sprStandShoot[3] = spr_bass_stand_shoot_d;
	
	sprJumpShoot[0] = spr_bass_jump_shoot_a;
	sprJumpShoot[1] = spr_bass_jump_shoot_b;
	sprJumpShoot[2] = spr_bass_jump_shoot_c;
	sprJumpShoot[3] = spr_bass_jump_shoot_d;
	
	sprClimbShoot[0] = spr_bass_climb_shoot_a;
	sprClimbShoot[1] = spr_bass_climb_shoot_b;
	sprClimbShoot[2] = spr_bass_climb_shoot_c;
	
	
	//Primary
	sprStand_Draw[0] = spr_bass_stand_1;
	sprRun_Draw[0] = spr_bass_run_1;
	sprJump_Draw[0] = spr_bass_jump_1;
	sprDash_Draw[0] = spr_bass_dash_1;
	sprDashShoot_Draw[0] = spr_bass_dash_shoot_1;		
	sprClimb_Draw[0] = spr_bass_climb_1;
	sprClimbStop_Draw[0] = spr_bass_climb_stop_1;
	sprClimbUp_Draw[0] = spr_bass_climb_up_1;
	sprHit_Draw[0] = spr_bass_damaged_1;	
	sprStandSlash_Draw[0] = spr_bass_stand_slash_1;
	sprJumpSlash_Draw[0] = spr_bass_jump_slash_1;
	sprStandShoot_Draw[0][0] = spr_bass_stand_shoot_a_1;
	sprStandShoot_Draw[0][1] = spr_bass_stand_shoot_b_1;
	sprStandShoot_Draw[0][2] = spr_bass_stand_shoot_c_1;
	sprStandShoot_Draw[0][3] = spr_bass_stand_shoot_d_1;
	sprJumpShoot_Draw[0][0] = spr_bass_jump_shoot_a_1;
	sprJumpShoot_Draw[0][1] = spr_bass_jump_shoot_b_1;
	sprJumpShoot_Draw[0][2] = spr_bass_jump_shoot_c_1;
	sprJumpShoot_Draw[0][3] = spr_bass_jump_shoot_d_1;
	sprClimbShoot_Draw[0][0] = spr_bass_climb_shoot_a_1;
	sprClimbShoot_Draw[0][1] = spr_bass_climb_shoot_b_1;
	sprClimbShoot_Draw[0][2] = spr_bass_climb_shoot_c_1;
	
	//Secondary
	sprStand_Draw[1] = spr_bass_stand_2;
	sprRun_Draw[1] = spr_bass_run_2;
	sprJump_Draw[1] = spr_bass_jump_2;
	sprDash_Draw[1] = spr_bass_dash_2;
	sprDashShoot_Draw[1] = spr_bass_dash_shoot_2;		
	sprClimb_Draw[1] = spr_bass_climb_2;
	sprClimbStop_Draw[1] = spr_bass_climb_stop_2;
	sprClimbUp_Draw[1] = spr_bass_climb_up_2;
	sprHit_Draw[1] = spr_bass_damaged_2;	
	sprStandSlash_Draw[1] = spr_bass_stand_slash_2;
	sprJumpSlash_Draw[1] = spr_bass_jump_slash_2;
	sprStandShoot_Draw[1][0] = spr_bass_stand_shoot_a_2;
	sprStandShoot_Draw[1][1] = spr_bass_stand_shoot_b_2;
	sprStandShoot_Draw[1][2] = spr_bass_stand_shoot_c_2;
	sprStandShoot_Draw[1][3] = spr_bass_stand_shoot_d_2;
	sprJumpShoot_Draw[1][0] = spr_bass_jump_shoot_a_2;
	sprJumpShoot_Draw[1][1] = spr_bass_jump_shoot_b_2;
	sprJumpShoot_Draw[1][2] = spr_bass_jump_shoot_c_2;
	sprJumpShoot_Draw[1][3] = spr_bass_jump_shoot_d_2;
	sprClimbShoot_Draw[1][0] = spr_bass_climb_shoot_a_2;
	sprClimbShoot_Draw[1][1] = spr_bass_climb_shoot_b_2;
	sprClimbShoot_Draw[1][2] = spr_bass_climb_shoot_c_2;

	//Outline
	sprStand_Draw[2] = spr_bass_stand_3;
	sprRun_Draw[2] = spr_bass_run_3;
	sprJump_Draw[2] = spr_bass_jump_3;
	sprDash_Draw[2] = spr_bass_dash_3;
	sprDashShoot_Draw[2] = spr_bass_dash_shoot_3;		
	sprClimb_Draw[2] = spr_bass_climb_3;
	sprClimbStop_Draw[2] = spr_bass_climb_stop_3;
	sprClimbUp_Draw[2] = spr_bass_climb_up_3;
	sprHit_Draw[2] = spr_bass_damaged_3;	
	sprStandSlash_Draw[2] = spr_bass_stand_slash_3;
	sprJumpSlash_Draw[2] = spr_bass_jump_slash_3;
	sprStandShoot_Draw[2][0] = spr_bass_stand_shoot_a_3;
	sprStandShoot_Draw[2][1] = spr_bass_stand_shoot_b_3;
	sprStandShoot_Draw[2][2] = spr_bass_stand_shoot_c_3;
	sprStandShoot_Draw[2][3] = spr_bass_stand_shoot_d_3;
	sprJumpShoot_Draw[2][0] = spr_bass_jump_shoot_a_3;
	sprJumpShoot_Draw[2][1] = spr_bass_jump_shoot_b_3;
	sprJumpShoot_Draw[2][2] = spr_bass_jump_shoot_c_3;
	sprJumpShoot_Draw[2][3] = spr_bass_jump_shoot_d_3;
	sprClimbShoot_Draw[2][0] = spr_bass_climb_shoot_a_3;
	sprClimbShoot_Draw[2][1] = spr_bass_climb_shoot_b_3;
	sprClimbShoot_Draw[2][2] = spr_bass_climb_shoot_c_3;
	
	sprStandDart = spr_bass_stand_dart;
	sprStandDart_Draw[0] = spr_bass_stand_dart_1;
	sprStandDart_Draw[1] = spr_bass_stand_dart_2;
	sprStandDart_Draw[2] = spr_bass_stand_dart_3;
	
	sprJumpDart = spr_bass_jump_dart;
	sprJumpDart_Draw[0] = spr_bass_jump_dart_2;
	sprJumpDart_Draw[1] = spr_bass_jump_dart_1;
	sprJumpDart_Draw[2] = spr_bass_jump_dart_3;
	
	sprSlam = spr_bass_dash;
	sprSlam_Draw[0] = spr_bass_dash_2;
	sprSlam_Draw[1] = spr_bass_dash_1;
	sprSlam_Draw[2] = spr_bass_dash_3;

	sprPrimary = noone;
	sprSecondary = noone;
	sprOutline = noone;
	primaryCol = noone;
	secondaryCol = noone;
	outlineCol = noone;
	
	chargeLv1Color[0] = make_color_rgb(168, 0, 168);
	chargeLv1Color[1] = make_color_rgb(228, 0, 198);
	chargeLv1Color[2] = make_color_rgb(248, 90, 248);
	
	chargeLv2Color[0] = make_color_rgb(248, 152, 56);
	chargeLv2Color[1] = make_color_rgb(112, 112, 112);
	chargeLv2Color[2] = c_black;

	//Weapon
	enum BassShot{
		MiniShot,
		TripleShot
	}
	currentWeapon = BassShot.MiniShot;

	
	
	//alarm
	charge = false;
	dashAlarm = 20;
	flashIndex = 1;
	shootDirection = 180;
	chargeAlarm = 100;
	chargeLv2Limit = 85;
	chargeLv3Limit = 100;

	//Create camera
	nextZoneXOffSet = 0;
	nextZoneYOffSet = 0;
	




}
