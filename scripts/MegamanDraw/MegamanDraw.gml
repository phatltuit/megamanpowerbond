function MegamanDraw() {
	draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
	if charge == true and sprite_index != sprPose and IsChargableWeapon() {	
		if adapter == false{
			MegamanDrawSprite();
			ProtoManDrawSprite();
			if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{			
				draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
				draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
				draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);
				ProtoManDrawMaskSprite();
			}else if chargeAlarm < 0{
			
				draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
				draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
				draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);
				ProtoManDrawMaskSprite();
			}else{
			
				draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
				draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
				draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);	
				ProtoManDrawMaskSprite();
			}
		}else{
			if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{
				draw_sprite_ext(sprite_index, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);	
			}else if chargeAlarm < 0{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,primaryCol,image_alpha);
			}else{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha)	
			}
		}
	}else{
		if adapter == false{
			MegamanDrawSprite();
			ProtoManDrawSprite();				
			if sprPrimary != noone and sprSecondary != noone and sprOutline != noone and global.weaponIndex != 8 and global.weaponIndex != 9{
				PlayerColor();
				draw_sprite_ext(sprPrimary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
				draw_sprite_ext(sprSecondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
				draw_sprite_ext(sprOutline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, outlineCol, image_alpha);
			}
			//if sprProtoStrk != noone{
			//	PlayerColor();
			//}
			ProtoManDrawMaskSprite();	
		}else{
				
		}
		
		
	}
	
	if override == true{	
		shadow = part_type_create();
		part_type_sprite(shadow, sprite_index, 1 , 0, 0);
		if global.weaponIndex == 0{
			var part_color = (object_index==objMegaman)?c_blue:c_red;
		}else{
			var part_color = global.weaponColor[global.weaponIndex];
		}
		part_type_size(shadow, 1.1, 1.1, 0,0);
		part_type_scale(shadow,image_xscale, image_yscale);
		part_type_life(shadow, 10, 10);
		part_type_alpha1(shadow, 0.8);	
		part_type_blend(shadow, true);
		part_type_color1(shadow, part_color);
		var rd = irandom(4);
		if rd == 0 or rd == 1{
			part_particles_create_color(global.particleSystem, x, y, shadow, part_color, 1);
		}
	}
}
