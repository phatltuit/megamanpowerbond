// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ShowWeaponIcon(){
	if quickEquip == true{
		if global.weaponIndex == 0{
			var icon = (object_index == objMegaman)?spr_weapon_mega_buster:(object_index == objProtoMan?spr_weapon_proto_buster:spr_weapon_bass_buster);
		}else{
			var icon = global.weaponAvt[global.weaponIndex];
		}
		draw_sprite_ext(icon,-1,x-8,y-25,1,image_yscale,image_angle,image_blend, image_alpha);
		quickEquipTime--;
		if quickEquipTime <= 0{
			quickEquipTime = 120;
			quickEquip = false;
		}
	}
}