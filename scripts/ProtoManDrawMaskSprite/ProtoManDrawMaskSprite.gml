// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ProtoManDrawMaskSprite(){
	if object_index == objProtoMan and override == true and sprProtoStrk != noone{
		draw_sprite_ext(sprProtoStrk, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);	
	}
}