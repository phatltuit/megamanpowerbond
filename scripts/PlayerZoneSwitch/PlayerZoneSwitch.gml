function PlayerZoneSwitch() {
	//Zone go right
	if (place_meeting(x + hspeed - 6, y, objZoneGoRight) and x > sectionRight - 8){
		nextZoneXOffSet = 96;
		nextZoneYOffSet = 0;		
		x = sectionRight - 8;		
		PlayerCameraInit();			
		with instance_create_depth(x, y, 0, objZoneSwitcher){
			dir = TransitionDirection.GoRight;				
			objPlayer.block = true;
		}	
	}
	//Zone go left
	else if (place_meeting(x -hspeed + 6, y, objZoneGoLeft) and x < sectionLeft + 8){	
		x = sectionLeft + 8;	
		nextZoneXOffSet = -96;
		nextZoneYOffSet = 0;
		PlayerCameraInit();		
		with instance_create_depth(x, y, 0, objZoneSwitcher){
			dir = TransitionDirection.GoLeft;						
			objPlayer.block = true;
		}			
	}
	//Zone go up
	else if place_meeting(x, y + hspeed - 6, objZoneGoUp) and y < sectionCeil + 6{
		nextZoneYOffSet = -96;
		nextZoneXOffSet = 0;
		y = sectionCeil + 6;
		PlayerCameraInit();
		block = true;
		with instance_create_depth(x, y, 0, objZoneSwitcher){
			dir = TransitionDirection.GoUp;	
		}	
	}
	//Zone go down
	else if place_meeting(x, y + hspeed + 6, objZoneGoDown) and y > sectionFloor - 6{
		nextZoneYOffSet = 96;	
		nextZoneXOffSet = 0;
		y = sectionFloor - 6;
		PlayerCameraInit();
		block = true;
		with instance_create_depth(x, y, 0, objZoneSwitcher){
			dir = TransitionDirection.GoDown;	
		}	
	}



}
