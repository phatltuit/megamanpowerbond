function PlayerDamage() {
	if slam == false{
		var myEnemy = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objEnemy, false, true)
		if myEnemy >= 0 and myEnemy.enable == true  and damaged == false and knockBack == false{
			damaged = true;	
			knockBack = true;
			alarm[4] = 25;
			alarm[5] = 70;	
			if object_index == objProtoMan and override == true{
				hp -= myEnemy.damageToPlayer*2;
			}else if object_index == objMegaman and adapter == true{
				reducedDamage = ceil(myEnemy.damageToPlayer/2);
				if reducedDamage == 0{
					hp -= 1;	
				}else{
					hp-= reducedDamage;	
				}
			}else{
				hp -= myEnemy.damageToPlayer;
			}
			xspeed = -knockBack*image_xscale;
			slash = false;
			airSlash = false;
			audio_play_sound(PlayerDamaged, 1,false);
		}
		
		var myBoss = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objBoss, false, true)
		if myBoss >= 0 and myBoss.ready == true and damaged == false and knockBack == false{
			damaged = true;	
			knockBack = true;
			alarm[4] = 25;
			alarm[5] = 70;	
			if object_index == objProtoMan and override == true{
				hp -= myBoss.damageToPlayer*2;
			}else{
				hp -= myBoss.damageToPlayer;
			}
			xspeed = -knockBack*image_xscale;
			slash = false;
			airSlash = false;
			audio_play_sound(PlayerDamaged, 1,false);
		}

		var myBossWp = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objBossWeapon, false, true)
		if myBossWp >= 0 and damaged == false and knockBack == false{
			damaged = true;	
			knockBack = true;
			alarm[4] = 25;
			alarm[5] = 70;	
			hp -= myBossWp.damageToPlayer;
			xspeed = -knockBack*image_xscale;
			slash = false;
			airSlash = false;
			audio_play_sound(PlayerDamaged, 1,false);
		}
	
		var myEnemyWp = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objEnemyWeapon, false, true)
		if myEnemyWp >= 0 and damaged == false and knockBack == false{
			damaged = true;	
			knockBack = true;
			alarm[4] = 25;
			alarm[5] = 75;	
			hp -= myEnemyWp.damageToPlayer;
			if eff_xspeed == 0{
				xspeed = -knockBack*image_xscale;
			}
			slash = false;
			airSlash = false;
			audio_play_sound(PlayerDamaged, 1,false);
		}
		

		if damaged == true{				
			if flashIndex == 1{					
				flashIndex = 0;
				image_alpha = 0;
			}else{
				flashIndex = 1;	
				image_alpha = 1;
			}
		}else{
			image_alpha = 1;	
		}	
	}
	
	if place_meeting(x,y,objSpike){
		hp -= 27;	
	}
		
	if hp <= 0 and defeat == false{  
		defeat = true;
		var i = 0;
		if block == false{
			block = true;
			run = false;
			gravity = 0;
			if object_index == objBass{
				dash = false;
			}else{				
				slide = false;
			}
			vspeed = 0;
			hspeed = 0;
			yspeed = 0;
			xspeed = 0;
			visible = false;
			audio_play_sound(PlayerDefeat, 1, false);
		}
		repeat 8
		{
			var explosionID = instance_create_depth(x, y, 1,objPlayerExplosion);
			explosionID.direction = i;
			explosionID.speed = 1.5;
                
			i += 45;
		}
        
		i = 0;
		repeat 8
		{
			var explosionID = instance_create_depth(x, y, 1, objPlayerExplosion);
			explosionID.direction = i;
			explosionID.speed = 2.5;
                
			i += 45;
		}	
		PlayerGotDefeat();
	}
}
