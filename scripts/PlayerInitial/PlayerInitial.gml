function PlayerInitial(argument0) {
	xspeed = 0;
	yspeed = 0;
	eff_xspeed = 0;
	maskDelay = false;
	moveByPlatform = false;
	supportJump = false;
	supportFly = false;
	supportFlyTimer = 0;
	wallHit = false;
	defeat = false;
	character = argument0;
	

	underWater = false;
	breathTime = 120;
	quickEquip = false;
	quickEquipTime = 90;
	slam = false;
	//Sound variable
	playLandSfx = true;
	playChargeSfx = false;
	playPoseSfx = false;
	playTeleOutSfx = false;
	adapter = false;	
	depth = -1;
	global.checkPointX = xstart;
	global.checkPointY = ystart;
	
		
	switch (argument0) {
	    case Player.Megaman:
	        MegamanInitial();
	        break;
		
		case Player.Bass:
	        BassInitial();
	        break;	
		
		case Player.Protoman:	
			ProtoManInitial();
			break;
		
	    default:
			
	        break;
	}



}
