// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSpecialCondition(){
	if place_meeting(x,y,objWater){
		underWater = true;		
	}else{
		underWater = false;
	}
	
	if underWater == true{
		breathTime--;
		if breathTime <= 0{
			breathTime = 120;
			instance_create_depth(x+2*image_xscale,y-8, -2,objBubble);
		}
	}
	
	#region deprecated feature
	/*if keyboard_check_pressed(global.keyOverride) and object_index == objProtoMan{	
		global.weaponIndex = 0;
		if override == false{
			override = true;
			global.override = true;
			if object_index == objProtoMan{
				global.fadedSound = 1;
				chargeAlarm = -1;
			}
		}else{
			override = false;
			global.override = false;
			if object_index == objProtoMan{
				global.fadedSound = 1;
				chargeAlarm = 100;	
				charge = false;
			}
		}
	}*/
	#endregion
	
	if override == true{		
		if object_index == objProtoMan{
			chargeAlarm--;	
			charge = true;
		}
		if underWater == true{
			runSpd = 1.6;
			grav = 0.15;
			jumpSpd = 5.85;
			image_speed = 1.4;	
			if object_index != objBass{
				slideSpd = 2.9;					
			}else{				
				dashSpd = 2.9;	
			}
		}else{				
			runSpd = 2;
			jumpSpd = 6;
			grav = 0.25;
			image_speed = 1.65;
			if object_index != objBass{
				slideSpd = 3.25;	
			}else{				
				dashSpd = 3.25;	
			}
		}
	}else{
		if underWater == true{
			grav = 0.15;
			runSpd = 1;
			jumpSpd = 5.5;	
			image_speed = 0.5;
			if object_index != objBass{
				slideSpd = 2.25;					
			}else{
				dashSpd = 2.25;					
			}
		}else{			
			grav = 0.25;
			runSpd = 1.45;
			jumpSpd = 5.5;			
			image_speed = 1;	
			if object_index != objBass{
				slideSpd = 2.75;				
			}else{
				dashSpd = 2.75;	
			}
		}
	}
	
}