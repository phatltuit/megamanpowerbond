function BassDrawSprite() {
	switch (sprite_index) {			
		case sprJumpDart:
			sprPrimary = sprJumpDart_Draw[0];
			sprSecondary = sprJumpDart_Draw[1];
			sprOutline = sprJumpDart_Draw[2];
			break;
		
		case sprStandDart:
			sprPrimary = sprStandDart_Draw[0];
			sprSecondary = sprStandDart_Draw[1];
			sprOutline = sprStandDart_Draw[2];
	        break;
			
		
	    case sprStand:
	        sprPrimary = sprStand_Draw[0];
			sprSecondary = sprStand_Draw[1];
			sprOutline = sprStand_Draw[2];
	        break;
		
		case sprDash:
	        sprPrimary = sprDash_Draw[0];
			sprSecondary = sprDash_Draw[1];
			sprOutline = sprDash_Draw[2];
	        break;
			
		case sprDashShoot:
	        sprPrimary = sprDashShoot_Draw[0];
			sprSecondary = sprDashShoot_Draw[1];
			sprOutline = sprDashShoot_Draw[2];
	        break;
		
		case sprRun:
	        sprPrimary = sprRun_Draw[0];
			sprSecondary = sprRun_Draw[1];
			sprOutline = sprRun_Draw[2];
	        break;
		
		case sprJump:
	        sprPrimary = sprJump_Draw[0];
			sprSecondary = sprJump_Draw[1];
			sprOutline = sprJump_Draw[2];
	        break;
			
		case sprClimb:
	        sprPrimary = sprClimb_Draw[0];
			sprSecondary = sprClimb_Draw[1];
			sprOutline = sprClimb_Draw[2];
	        break;
		
		case sprClimbStop:
	        sprPrimary = sprClimbStop_Draw[0];
			sprSecondary = sprClimbStop_Draw[1];
			sprOutline = sprClimbStop_Draw[2];
	        break;	
		
		case sprClimbUp:
	        sprPrimary = sprClimbUp_Draw[0];
			sprSecondary = sprClimbUp_Draw[1];
			sprOutline = sprClimbUp_Draw[2];
	        break;
		
		case sprHit:
	        sprPrimary = sprHit_Draw[0];
			sprSecondary = sprHit_Draw[1];
			sprOutline = sprHit_Draw[2];
	        break;
			
		case sprStandSlash:
			sprPrimary = sprStandSlash_Draw[0];
			sprSecondary = sprStandSlash_Draw[1];
			sprOutline = sprStandSlash_Draw[2];		
			break;
			
		case sprJumpSlash:
			sprPrimary = sprJumpSlash_Draw[0];
			sprSecondary = sprJumpSlash_Draw[1];
			sprOutline = sprJumpSlash_Draw[2];		
			break;
		
		case sprStandShoot[0]:
			sprPrimary = sprStandShoot_Draw[0][0];
			sprSecondary = sprStandShoot_Draw[1][0];
			sprOutline = sprStandShoot_Draw[2][0];
			break;
			
		case sprStandShoot[1]:
			sprPrimary = sprStandShoot_Draw[0][1];
			sprSecondary = sprStandShoot_Draw[1][1];
			sprOutline = sprStandShoot_Draw[2][1];
			break;
			
		case sprStandShoot[2]:
			sprPrimary = sprStandShoot_Draw[0][2];
			sprSecondary = sprStandShoot_Draw[1][2];
			sprOutline = sprStandShoot_Draw[2][2];
			break;
			
		case sprStandShoot[3]:
			sprPrimary = sprStandShoot_Draw[0][3];
			sprSecondary = sprStandShoot_Draw[1][3];
			sprOutline = sprStandShoot_Draw[2][3];
			break;

		case sprJumpShoot[0]:
			sprPrimary = sprJumpShoot_Draw[0][0];
			sprSecondary = sprJumpShoot_Draw[1][0];
			sprOutline = sprJumpShoot_Draw[2][0];
			break;
			
		case sprJumpShoot[1]:
			sprPrimary = sprJumpShoot_Draw[0][1];
			sprSecondary = sprJumpShoot_Draw[1][1];
			sprOutline = sprJumpShoot_Draw[2][1];
			break;
			
		case sprJumpShoot[2]:
			sprPrimary = sprJumpShoot_Draw[0][2];
			sprSecondary = sprJumpShoot_Draw[1][2];
			sprOutline = sprJumpShoot_Draw[2][2];
			break;
			
		case sprJumpShoot[3]:
			sprPrimary = sprJumpShoot_Draw[0][3];
			sprSecondary = sprJumpShoot_Draw[1][3];
			sprOutline = sprJumpShoot_Draw[2][3];
			break;
		
		case sprClimbShoot[0]:
			sprPrimary = sprClimbShoot_Draw[0][0];
			sprSecondary = sprClimbShoot_Draw[1][0];
			sprOutline = sprClimbShoot_Draw[2][0];
			break;
		
		case sprClimbShoot[1]:
			sprPrimary = sprClimbShoot_Draw[0][1];
			sprSecondary = sprClimbShoot_Draw[1][1];
			sprOutline = sprClimbShoot_Draw[2][1];
			break;
			
		case sprClimbShoot[2]:
			sprPrimary = sprClimbShoot_Draw[0][2];
			sprSecondary = sprClimbShoot_Draw[1][2];
			sprOutline = sprClimbShoot_Draw[2][2];
			break;
		
		
	    default:
			sprPrimary = noone;
			sprSecondary = noone;
			sprOutline = noone;
	        break;			
	}
	
}
