function BassBegin() {
	switch (beginPhase) {
		case 0:
		   sprite_index = sprTele;
			//gravity = grav;	
			vspeed = 8;
			x_end = x;
			y_end = y;
			y = sectionCeil - 224*3; //camera_get_view_y(view_camera[0]) - 224*2;
			beginPhase = 1;
			image_speed = 0;
			image_index = 0;
			PlayerReadyText();
		    break;	    			
		case 1:
			if round(y) >= round(y_end) and round(x) == round(x_end){
				y = y_end;
				image_speed = 1;
				vspeed = 0;
			}
			if image_index + image_speed >= image_number{
				sprite_index = sprTransform;
				audio_play_sound(PlayerteleportIn, 1, false);
				beginPhase = 2;
			}		
			break;
			
		case 2:
			if image_index + image_speed >= image_number and sprite_index == sprTransform{
				ready = true;
				beginPhase = 0;
			}
			break;		
	}


}
