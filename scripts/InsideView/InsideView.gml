function InsideView() {
	return x >= camera_get_view_x(view_camera[0]) 
	and y >= camera_get_view_y(view_camera[0]) 
	and x <= camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) 
	and y <= camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0]);
}
