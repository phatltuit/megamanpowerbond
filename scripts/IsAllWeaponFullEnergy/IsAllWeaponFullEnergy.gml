// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsAllWeaponFullEnergy(){
	for (var i = 1; i < 10; ++i) {
		if global.weaponEnergy[i] < 27{
			return false;
		}
	}
	return true;
}