// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerWeaponShoot(){
	switch (global.weaponIndex) {
		case 9:		
			if object_index == objMegaman{
				PlayerSupporters(Support.RushJet);	
			}else if object_index == objProtoMan{
				PlayerSupporters(Support.ProtoJet);	
			}else{
				PlayerSupporters(Support.TrebleJet);	
			}
			break;
		
		case 8:			
			if object_index == objMegaman{
				PlayerSupporters(Support.RushCoil);	
			}else if object_index == objProtoMan{
				PlayerSupporters(Support.ProtoCoil);	
			}else{
				PlayerSupporters(Support.TrebleCoil);	
			}
			break;
		
		case 7:			
			PlayerBossWeapon(BossWeapon.FlameTornado);			
			shoot = true;			
			break;
			
		case 6:
			if slam == false{
				PlayerBossWeapon(BossWeapon.IceSlasher);						
			}
			break;
		
		case 5:
			PlayerBossWeapon(BossWeapon.SonicSlash);			
			shoot = true;
			break;
		
		case 4:
			PlayerBossWeapon(BossWeapon.SolarBlast);			
			shoot = true;
			break;
	
		case 3:			
			PlayerBossWeapon(BossWeapon.ShadowBlade);			
			shoot = true;
			break;
										
		case 2:
			PlayerBossWeapon(BossWeapon.HomingMissile);
			shoot = true;
			break;
		
		case 1:
			PlayerBossWeapon(BossWeapon.QuickBoomerang);
			break;
		default:
			// code here
			break;
	}
}