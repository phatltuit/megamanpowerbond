// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsInView(argument0){	
	return argument0.x >= camera_get_view_x(view_camera[0]) 
	and argument0.y >= camera_get_view_y(view_camera[0]) 
	and argument0.x <= camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) 
	and argument0.y <= camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0]);
}