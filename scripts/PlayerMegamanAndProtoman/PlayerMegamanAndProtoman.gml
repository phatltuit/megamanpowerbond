function PlayerMegamanAndProtoman() {
	if ready == false{
		MegamanAndProtomanBegin();	
	}else{				
		if block == false{
			#region Camera View
				PlayerCamera();
				PlayerZoneSwitch();
			#endregion
			if knockBack == false{	
				if supportFly == false{							
					if slam == false{
						#region Stepping
						if keyboard_check_direct(global.keyLeft) and !place_meeting(x + xspeed-1, y,objSolid) and slash == false{
							if slide == false and climb == false{
								run = true; 
								xspeed = -runSpd;
							}else if slide == true{
								xspeed = -slideSpd;
							}
							image_xscale = -1;
						}else if keyboard_check_direct(global.keyRight) and !place_meeting(x + xspeed+ 1, y,objSolid) and slash == false{
							if slide == false and climb == false{
								run = true; 
								xspeed = runSpd;
							}else if slide == true{
								xspeed = slideSpd;
							}
							image_xscale = 1;
						}else{
							run = false;
							if slide == false{
								xspeed = 0;
							}
						}	
					#endregion
					
						#region Climbing					
						if keyboard_check_direct(global.keyUp) and instance_place(x, y+vspeed - 1, objLadder) != noone and shoot == false{						
							var ladderBelow = instance_place(x, y+vspeed - 1, objLadder);							
							x = ladderBelow.x;
							SupporterDismiss();
							climb = true;
							if shoot == true{
								climbDir = 0;	
							}else{
								climbDir = 1;
								sprite_index = sprClimb;
							}
							yspeed = -climbSpd;			
						}else if keyboard_check_direct(global.keyDown) and instance_place(x, y+vspeed + 1, objLadder) != noone and shoot == false{							
							var ladderBelow = instance_place(x, y + vspeed + 1, objLadder);
							if x < ladderBelow.x+5 and x > ladderBelow.x-5 {
								ground = false;	
								if ladderBelow >= 0 and ladderBelow.isTop == true and climb == false{
									y = ladderBelow.bbox_top + 1;	
								}
								climb = true;
								x = ladderBelow.x;	
								SupporterDismiss();
								if shoot == true{
									climbDir = 0;	
								}else{
									climbDir = -1;
									sprite_index = sprClimb;
								}
								yspeed = climbSpd;
							}
						}else{
							if climb == true{
								if instance_place(x, y+yspeed+1, objLadder) == noone{
									climb = false;	
								}else{
									sprite_index = sprClimbStop;
									climbDir = 0;	
									yspeed = 0;					
								}
							}
						}				
					#endregion
					}
				}else{						
					#region Jet Flying
						supportFlyTimer++;
						if supportFlyTimer >= 60{
							global.weaponEnergy[9] -= 1;
							supportFlyTimer = 0;
						}
					
						if keyboard_check_direct(global.keyRight){
							xspeed = runSpd;
							image_xscale = 1;																
						}else if keyboard_check_direct(global.keyLeft){
							xspeed = -runSpd;
							image_xscale = -1;																	
						}else{
							xspeed = 0;
						}
							
						if keyboard_check_direct(global.keyUp){
							yspeed = -runSpd;	
						}else if keyboard_check_direct(global.keyDown){
							yspeed = runSpd;	
						}else{
							yspeed = 0;	
						}	
					#endregion
				}
								
				if adapter == true{
					#region Punching
						if keyboard_check_direct(global.keyShoot) and slash == false and airSlash == false 
							and IsChargableWeapon(){						
							charge = true;
							chargeAlarm--;
						}
						if charge == true{
							if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{
								if playChargeSfx == false{ 
								audio_play_sound(PlayerCharge, 1, false);						
								global.fadedSound = 1;
								audio_sound_gain(PlayerCharge, global.fadedSound, 0);
								playChargeSfx = true;
								}	
						
								if chargeAlarm < round(chargeLv2Limit/3){
									outlineCol = chargeLv1Color[2];	
								}else if chargeAlarm < round(chargeLv2Limit/3)*2{
									outlineCol = chargeLv1Color[1];	
								}else{
									outlineCol = chargeLv1Color[0];	
								}	
							}else if chargeAlarm < 0 {
								switch (abs(chargeAlarm/2 mod 3)){
									case 0: //Light blue helmet, black shirt, blue outline
								        primaryCol = make_color_rgb(216,40, 0);								        
									    break;
                        
									case 1: //Black helmet, blue shirt, light blue outline
									    primaryCol = make_color_rgb(248,248,248);									    
									break;
                        
									case 2: //Blue helmet, light blue shirt, blue outline
									    primaryCol = c_black;									    
									break;							
								}	
							}
						}
						if keyboard_check_released(global.keyShoot) and shoot == false and slash == false and airSlash == false{
							charge = false;
							playChargeSfx = false;								
							audio_stop_sound(PlayerCharge);		
							if global.weaponIndex == 0{
								if chargeAlarm >= 0{
									image_index = 0;
									var shot = instance_create_depth(x+10*image_xscale,y,0,objPlayerWeapon);
									shot.sprite_index = spr_mini_power_shot;
									shot.hspeed = 3*image_xscale;
									shot.image_xscale = image_xscale;
									shot.alarm[0] = 25;
									shoot = true;
									alarm[3] = 15;
								}else{
									image_index = 0;
									var shot = instance_create_depth(x+10*image_xscale,y,0,objPlayerWeapon);
									shot.sprite_index = spr_mega_power_shot;
									shot.hspeed = 3*image_xscale;
									shot.image_xscale = image_xscale;
									shot.alarm[0] = 35;
									shoot = true;
								}
								chargeAlarm = 100;
							}
							
						}
						
						if shoot == true and image_index + image_speed >= image_number{
							shoot = false;
						}
					#endregion
				}else{
					#region Switch Weapon
						PlayerWeaponEquip();
					#endregion
					
					#region Sliding	
						var slideBox = image_xscale > 0? bbox_right : bbox_left;
						var nearSolid = collision_rectangle(x, y-6,slideBox + image_xscale, y+7, objSolid, false, true);
						if keyboard_check_pressed(global.keySlide) and supportFly == false
						and (nearSolid == noone or (nearSolid != noone and nearSolid.sprite_index == spr_gate_open))  and slide == false and ground == true{
							slide = true;
							slash = false;
							airSlash = false;
							xspeed = slideSpd*image_xscale;
							mask_index = spr_mega_man_slide_mask;
							alarm[2] = 25; 
							var box;
							if image_xscale == 1{
								box = bbox_left - 1;;	
							}else{
								box = bbox_right + 1;	
							}
							var dust = instance_create_depth(box,bbox_bottom, 0,objDust);						
							dust.image_xscale = image_xscale;
						}
					#endregion
					
					#region Shooting	
						if keyboard_check_direct(global.keyShoot) and slash == false and airSlash == false 
							and IsChargableWeapon(){						
							charge = true;
							chargeAlarm-= 1;
						}
						
						if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{	
							if playChargeSfx == false{ 
								audio_play_sound(PlayerCharge, 1, false);						
								global.fadedSound = 1;
								audio_sound_gain(PlayerCharge, global.fadedSound, 0);
								playChargeSfx = true;
							}	
						
							if chargeAlarm < round(chargeLv2Limit/3){
								outlineCol = chargeLv1Color[2];	
							}else if chargeAlarm < round(chargeLv2Limit/3)*2{
								outlineCol = chargeLv1Color[1];	
							}else{
								outlineCol = chargeLv1Color[0];	
							}
						}else if chargeAlarm < 0{
							if global.fadedSound < 0.1{
								global.fadedSound = 0.1;
							}else{
								global.fadedSound -= 0.01;
							}
							audio_sound_gain(PlayerCharge, global.fadedSound, 500);					
							switch (abs(chargeAlarm/2 mod 3))
							{
							    case 0: //Light blue helmet, black shirt, blue outline
							        primaryCol = chargeLv2Color[0];
							        secondaryCol = chargeLv2Color[2];
							        outlineCol = chargeLv2Color[1];
							    break;
                        
							    case 1: //Black helmet, blue shirt, light blue outline
							        primaryCol = chargeLv2Color[2];
							        secondaryCol = chargeLv2Color[1];
							        outlineCol = chargeLv2Color[0];
							    break;
                        
							    case 2: //Blue helmet, light blue shirt, blue outline
							        primaryCol = chargeLv2Color[1];
							        secondaryCol = chargeLv2Color[0];
							        outlineCol = chargeLv2Color[2];
							    break;
							}	
						}
		
						if keyboard_check_released(global.keyShoot) and slash == false and airSlash == false{
							charge = false;
							playChargeSfx = false;	
							audio_stop_sound(PlayerCharge);					
							if slide == false{
								if global.weaponIndex == 0{
									if chargeAlarm > chargeLv2Limit and chargeAlarm < chargeLv3Limit {
										MegamanAndProtomanWeapon(Buster.MiniBuster);	
										shoot = true;
									}else if chargeAlarm <= chargeLv2Limit and chargeAlarm > 0{
										MegamanAndProtomanWeapon(Buster.MegaBuster);	
										shoot = true;
									}else if chargeAlarm <= 0{
										MegamanAndProtomanWeapon(Buster.GigaBuster);
										shoot = true;
									}	
									alarm[3] = 15;
								}else{
									PlayerWeaponShoot();								
									alarm[3] = 15;
								}
							}else{
								shoot = false;	
							}
							if object_index == objProtoMan and override == true{
								chargeAlarm = -1;
							}else{
								chargeAlarm = 100;
							}
		
						}	
					#endregion
				}	
				
				#region Slashing
					if canSlash == true{
						if keyboard_check_direct(global.keySlash) and global.weaponIndex == 0 and shoot == false{
							if ground == true{
								if slash == false and slide == false{
									slash = true;
									sprite_index = sprStandSlash;							
									var saber = instance_create_depth(x,y,0,objPlayerWeapon);
									saber.sprite_index = object_index == objMegaman?spr_mega_man_stand_saber:spr_proto_man_stand_saber;
									saber.image_xscale = image_xscale;
									saber.image_index = 0;
									saber.visible = false;
								}
								airSlash = false;
							}else{
								if airSlash == false{
									sprite_index = sprJumpSlash;
									airSlash = true;			
									var saber = instance_create_depth(x,y,0,objPlayerWeapon);
									saber.sprite_index = object_index == objMegaman?spr_mega_man_jump_saber:spr_proto_man_jump_saber;
									saber.image_xscale = image_xscale;
									saber.image_index = 0;
									saber.visible = false;
								}
								slash = false;
							}						
						}
				
						if (sprite_index == sprStandSlash or sprite_index == sprJumpSlash) and image_index + image_speed >= image_number{
							slash = false;
							airSlash = false;
						}	
					}
				#endregion
				
				#region Reflect bullet only for Protoman
					EnemiesBulletReflect();
				#endregion
			}
			#region Underwater condition 
				PlayerSpecialCondition();
			#endregion
		}
		
		#region Collision
			#region SOLID	
				if !instance_exists(objZoneSwitcher){
					//Wall when moving by effect
					if eff_xspeed != 0{
						var mySolid = collision_rectangle(bbox_left-1+eff_xspeed, bbox_top, bbox_right+1+eff_xspeed, bbox_bottom, objSolid, false, true)
						
						if mySolid >= 0{
							if mySolid.x < x{
								while !place_meeting(x-1,y,objSolid){
									x--;	
								}
								xspeed = 0;
							}else if mySolid.x > x{
								while !place_meeting(x+1,y,objSolid){
									x++;	
								}
								xspeed = 0;
							}
						}
					}else{					
						//Wall
						if place_meeting(x + xspeed + sign(xspeed), y, objSolid){
							var mySolid = instance_place(x + xspeed + sign(xspeed), y, objSolid);
							if mySolid >= 0 and xspeed != 0{		
								x = mySolid.x;
								if slide == true{
									slide = false;	
									while place_meeting(x, y, objSolid){									
										x -=sign(xspeed);	
									}								
								}else{
									while place_meeting(x + sign(xspeed), y, objSolid){
										x -=sign(xspeed);	
									}
								}
							
								xspeed = 0;						
								//hspeed = 0;							
								run = false;							
							}
						}
					}

					//Floor
					var mySolid = instance_place(x, y+vspeed + 1, objSolid);
					var myMVSolid = instance_place(x, y+vspeed + 1, objMovingSolid);
					if mySolid >= 0 and vspeed >= 0{	
						y = mySolid.y;
						while place_meeting(x, y, mySolid)
							y -= 1;
						if climb == true and climbDir == -1{
							climb = false;	
						}
						supportJump = false;
						ground = true;  
						if supportFly == true{
							supportFly = false;	
							SupporterDismiss();
						}
						if playLandSfx == false{
							audio_play_sound(PlayerLand, 1, false);
							playLandSfx = true;
						}
					}else if myMVSolid >= 0 and vspeed >= 0{	
						y = myMVSolid.y;
						while place_meeting(x, y, myMVSolid)
							y -= 1;
						if climb == true and climbDir == -1{
							climb = false;	
						}
						ground = true;  
						supportJump = false;
						if myMVSolid.hspeed != 0{
							moveByPlatform = true;							
							if run == true or slide == true{								
								xspeed = myMVSolid.hspeed+image_xscale;
							}else{
								xspeed = myMVSolid.hspeed;
							}							
						}
						if playLandSfx == false{
							audio_play_sound(PlayerLand, 1, false);
							playLandSfx = true;
						}
					}else{							
						moveByPlatform = false;	
						if supportFly == true{
							ground = true;		
						}else{							
							ground = false;	
						}
						if slide == true and instance_place(x, y+vspeed + 1, objLadder) == noone{
							slide = false;
							if vspeed >= 0 and maskDelay == false{
								maskDelay = true;
								alarm[1] = 5;
								mask_index = spr_mega_man_slide_mask;
							}				
						}
					}						
					
					//Ceil
					if supportFly == false{
						mySolid = instance_place(x, y+vspeed-2, objSolid);
						if mySolid >= 0 and vspeed <= 0
						{					
							y = mySolid.bbox_bottom + abs(bbox_top - y) + 1;
							vspeed = 0;
						}
					}else{
						mySolid = instance_place(x, y+yspeed-2, objSolid);
						if mySolid >= 0 and yspeed <= 0
						{					
							y = mySolid.bbox_bottom + abs(bbox_top - y) + 1;
							yspeed = 0;
						}	
					}
		
					//Top ladder		
					var myTopLadder = collision_rectangle(bbox_left, bbox_bottom+1, bbox_right,bbox_top, objLadder, false, true);		
					if myTopLadder >= 0 and myTopLadder.isTop == true and supportFly == false{
						if y > myTopLadder.bbox_top and y <= myTopLadder.y {
							sprite_index = sprClimbUp	
						}else if y <= myTopLadder.bbox_top and vspeed >= 0{					
							y = myTopLadder.y;
							while place_meeting(x, y, myTopLadder){
								y = round(y - 1);	
							}
							climb = false;
							ground = true;	
							supportJump = false;
							vspeed = 0;
							if playLandSfx == false{
								audio_play_sound(PlayerLand, 1, false);
								playLandSfx = true;
							}
						}
					}
					
					if bbox_right + 1 > camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) and xspeed > 0{
						//x = camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) - sprite_get_bbox_right(sprite_index) + sprite_get_xoffset(sprite_index);
						xspeed = 0;
						run = false;
						slide = false;
					}else if bbox_left - 1 < camera_get_view_x(view_camera[0]) and xspeed < 0{
						//x = camera_get_view_x(view_camera[0]) + sprite_get_bbox_left(sprite_index) - sprite_get_xoffset(sprite_index);	
						xspeed = 0;
						run = false;
						slide = false;
					}
				}	
			#endregion
			#region MovingSolid	
				//Ceil				
				
				myMVSolid = instance_place(x, y+vspeed-2, objMovingSolid);
				if myMVSolid >= 0 and vspeed < 0
				{					
					y = myMVSolid.bbox_bottom + abs(bbox_top - y) + 1;
					vspeed = myMVSolid.vspeed<0?0:myMVSolid.vspeed;
					supportJump = false;
				}
			#endregion
		#endregion
		
		#region Jumping				
			if block == false and keyboard_check_pressed(global.keyJump) and (ground == true or climb == true) and !place_meeting(x, y + vspeed - 2, objSolid){				
				ground = false;
				supportFly = false;
				if climb == true{
					keyboard_key_release(global.keyUp);
					keyboard_key_release(global.keyDown);
					climb = false;
					vspeed = 0;
				}else{
					vspeed = -jumpSpd;			
				}
			}			
					
			if ground == false{					
				canMinJump = true;		
				if slam == true{
					gravity = 0;
					vspeed = 0;
				}else{
					gravity = grav;			
				}
				playLandSfx = false;
				slash = false;
			}else{	
				airSlash = false;
				gravity = 0;
				vspeed = 0;
			}		
			
			if supportJump == false{	
				if ground == false and vspeed < 0 and !keyboard_check_direct(global.keyJump) and canMinJump == true{
					canMinJump = false;	
					vspeed = 0;						
				}		
			}
		#endregion
			
		#region Sprites			
			if knockBack == false{	
				if adapter == true{
					if shoot == true{						
						if climb == true{
							sprite_index = spr_mega_adap_climb_punch;	
						}else if ground == false{
							sprite_index = spr_mega_adap_jump_punch;	
						}else{
							sprite_index = spr_mega_adap_punch;	
						}
					}else{
						if ground == false{
							if climb == false{
								sprite_index = sprJump;	
							}
						}else{
							if run == false{
								sprite_index = sprStand;	
							}else{
								sprite_index = sprRun;		
							}
						}
					}
				}else{
					if supportFly == true{
						if shoot == true{
							if IsDartWeapon(){
								sprite_index = sprStandDart;
							}else{
								sprite_index = sprStandShoot;										
							}
						}else{
							sprite_index = sprStand;	
						}
					}else{
						if slam == false{
							if ground == true{
								if slide == true{
									sprite_index = sprSlide;		
								}else{
									if run == true{
										if shoot == true{
											if IsDartWeapon(){
												sprite_index = sprStandDart;
											}else{
												sprite_index = sprRunShoot;
											}
										}else if slash == false{
											sprite_index = sprRun;	
										}
									}else{
										if shoot == true{
											if IsDartWeapon(){
												sprite_index = sprStandDart;
											}else{
												sprite_index = sprStandShoot;										
											}
										}else if slash == false{
											sprite_index = sprStand;
										}
									}
								}
							}else{
								if climb == false{
									if shoot == true{
										if IsDartWeapon(){
											sprite_index = sprJumpDart;
										}else{
											sprite_index = sprJumpShoot;
										}
									}else if airSlash == true{
										sprite_index = sprJumpSlash;								
									}else{				
										sprite_index = sprJump;	
									}
								}else{
									if shoot == true{
										sprite_index = sprClimbShoot;
									}
								}
							}
						}else{
							sprite_index = sprSlam;			
						}
					}
				}				
			}else{
				sprite_index = sprHit;			
			}
			
		#endregion
	
		#region Damaging
			PlayerDamage();
		#endregion
	
		#region Moving		
			if supportFly == true{
				vspeed = yspeed;	
				hspeed = xspeed;
			}else{				
				if slide == false{
					if maskDelay == false{
						mask_index = object_index==objMegaman?spr_mega_man_normal_mask:spr_proto_man_normal_mask;	
					}
				}else{
					if ground == true and slide == true and place_meeting(x + round(xspeed/2), y  - 2, objSolid){
						alarm[2] = 2;
					}
					mask_index = object_index==objMegaman?spr_mega_man_slide_mask:spr_proto_man_slide_mask;		
				}		
			
				if climb == true{
					vspeed = yspeed;
					gravity = 0;
				}
				if slash == true{
					hspeed = eff_xspeed;
				}else if shoot == true{
					if (IsDartWeapon() or adapter == true) and ground == true{
						hspeed = eff_xspeed;
					}else{
						hspeed = xspeed  + eff_xspeed;
					}
				}else{
					hspeed = xspeed  + eff_xspeed;
				}
			}		
		
		#endregion	
	}
}
