function BassCollisionProcess() {
	//Mask index
	if dash == true{
		mask_index = spr_bass_dash_mask;
	}else{
		mask_index = spr_bass_normal_mask;	
	}

	//floor
	var mySolid;
	mySolid = instance_place(x, y+vspeed + 1, objSolid);
	if mySolid >= 0 and vspeed >= 0
	{
	    y = mySolid.y;	
	    while place_meeting(x, y, mySolid)
	        y -= 1;
		if climb == true and climbDir == -1{
			climb = false;	
		}
	    ground = true;
	}else{
		ground = false;		
	}



	//Top solid
	var myTopLadder;
	myTopLadder = instance_place(x, y+vspeed + 1, objTopLadder);
	if myTopLadder >= 0 and vspeed >= 0 and myTopLadder.image_index == 0 and y <= myTopLadder.y - (bbox_bottom - y)
	{
	    y = myTopLadder.y;	
	    while place_meeting(x, y, myTopLadder)
	        y -= 1;
		climb = false;
	    ground = true;
	}




	//Ceiling
	mySolid = instance_place(x, y+vspeed, objSolid);
	if mySolid >= 0 and vspeed < 0
	{
	    y = mySolid.y + 16 + (y - (bbox_top-1));
	    vspeed = 0;
	}




}
