// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function PlayerStep(){
	weaponIndex = global.weaponIndex;
	if global.Freeze == false{
		switch (character) {
			case Player.Megaman:
			    PlayerMegamanAndProtoman();
			    break;
		
			case Player.Bass:
			    PlayerBass();
			    break;	
		
			case Player.Protoman:	
				PlayerMegamanAndProtoman();
				break;
		
			default:
			    PlayerMegamanAndProtoman();
			    break;
		}
		
	}else{		
		AfterDefeatBoss();		
	}
}
	