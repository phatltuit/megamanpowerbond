function KeyboardCheck(argument0) {
	key = argument0;
	switch (key) {
	    case global.keyLeft:
	        return keyboard_check_direct(global.keyLeft);
	        break;
		case global.keyRight:
			return keyboard_check_direct(global.keyRight);
	        break;
		case global.keyUp:
	        return keyboard_check_direct(global.keyUp);
	        break;
		case global.keyDown:
			return keyboard_check_direct(global.keyDown);
	        break;
		case global.keyDash:
		case global.keySlide:
			return keyboard_check_pressed(global.keySlide);        
	        break;
		case global.keyJump:
			return keyboard_check_pressed(global.keyJump);        
	        break;
		case global.keyShoot:
			return keyboard_check_released(global.keyShoot);
			break;
	 
	        break;
	    default:
	        return false;
	        break;
	}


}
