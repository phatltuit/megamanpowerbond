function MegamanChargeAnimation() {
	if chargeAlarm <= chargeLv2Limit and chargeAlarm >= 0{
		if chargeAlarm < round(chargeLv2Limit/3){
			outlineCol = make_color_rgb(168, 0, 32);	
		}else if chargeAlarm < round(chargeLv2Limit/3)*2{
			outlineCol = make_color_rgb(228, 0, 88);	
		}else{
			outlineCol = make_color_rgb(248, 88, 152);	
		}
	}else if chargeAlarm < 0{
		switch (abs(chargeAlarm/2 mod 3))
	    {
	        case 0: //Light blue helmet, black shirt, blue outline
	            primaryCol = make_color_rgb(0, 232, 216);
	            secondaryCol = c_black;
	            outlineCol = make_color_rgb(0, 120, 248);
	        break;
                        
	        case 1: //Black helmet, blue shirt, light blue outline
	            primaryCol = c_black;
	            secondaryCol = make_color_rgb(0, 120, 248);
	            outlineCol = make_color_rgb(0, 232, 216);
	        break;
                        
	        case 2: //Blue helmet, light blue shirt, blue outline
	            primaryCol = make_color_rgb(0, 120, 248);
	            secondaryCol = make_color_rgb(0, 232, 216);
	            outlineCol = c_black;
	        break;
	    }
	
	}


}
