// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerGotDefeat(){
	global.defeat = true;
	global.bossFight = false;
	objGLOBAL_SETTING.alarm[0] = 380;
	objGLOBAL_SETTING.alarm[1] = 180;	
	global.previousCharacter = character;
	global.life--;
	GlobalPlayerData();
	instance_destroy();
}