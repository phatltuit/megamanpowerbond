// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossBaseDamage(){
	var playerWeapon = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objPlayerWeapon,false, true);
	if playerWeapon >= 0 and playerWeapon.damage != 0{
		if hit == false{			
			if(IsBossWeaknessDamage(object_index, playerWeapon.sprite_index)){
				hp-=5;
			}else{
				if IsTrueDamageWeapon(playerWeapon.sprite_index){
					hp-=playerWeapon.damage;	
				}else{
					hp-=1;	
				}
			}
			alarm[0] = 100;		
			hit = true;			
		}
			
		if hitTimer == 5{
			audio_stop_sound(PlayerHitEnemy);
			audio_play_sound(PlayerHitEnemy, 1, false);
			hitTimer--;
		}else if hitTimer < 5 and hitTimer > 5{
			hitTimer --;	
		}else{
			hitTimer = 5;
		}
		
		if IsDestroyableWeapon(playerWeapon){
			audio_play_sound(PlayerHitEnemy, 1, false);
			instance_destroy(playerWeapon);
		}
	}else{
		hitTimer = 20;	
	}

	if hit == true{	
		if hitIndex == 1{
			hitIndex = 0;
			image_alpha = 0;
		}else{
			hitIndex = 1;	
			image_alpha = 1;
		}
	}

	if hp <= 0{   
		audio_stop_sound(BossBattle);
		var i = 0;
		if ready == true{	
			ready = false;
			gravity = 0;
			vspeed = 0;
			hspeed = 0;	
			visible = false;
			audio_play_sound(PlayerDefeat, 1, false);
		}
		repeat 8
		{
		    var explosionID = instance_create_depth(x, y, 1,objPlayerExplosion);
		    explosionID.direction = i;
		    explosionID.speed = 1.5;
                
		    i += 45;
		}
        
		i = 0;
		repeat 8
		{
		    var explosionID = instance_create_depth(x, y, 1, objPlayerExplosion);
		    explosionID.direction = i;
		    explosionID.speed = 2.5;
                
		    i += 45;
		}	
		if instance_exists(objPlayer){	
			if global.multiBoss == false{
				global.Freeze = true;
				objPlayer.defeatBoss = true;
				global.bossFight = false;
			}else{
				instance_create_depth(x,y,0,objPowerOrb);
			}
		}
		instance_destroy(objBossWeapon);
		instance_destroy();
	}
}