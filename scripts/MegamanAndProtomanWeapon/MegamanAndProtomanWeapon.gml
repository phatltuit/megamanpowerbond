function MegamanAndProtomanWeapon(argument0) {
	var bulletYStart = (object_index == objMegaman)?4:1;
	if object_index == objMegaman{
		if sprite_index != sprStandShoot{
			var bulletXStart = 15*image_xscale;	
		}else{
			var bulletXStart = 20*image_xscale;	
		}		
	}else{
		var bulletXStart = 10*image_xscale;
	}	
	var objWeapon = objPlayerWeapon;
	
	switch (argument0) {
	    case Buster.MiniBuster:
			if instance_number(objWeapon) < 3{
				var shot = instance_create_depth (x + bulletXStart, y - bulletYStart, 0, objWeapon);
				var recoil = instance_create_depth (x + bulletXStart, y - bulletYStart, 0, objWeaponRecoil);
				recoil.sprite_index = spr_mini_recoil;
				recoil.image_xscale = image_xscale;
				shot.sprite_index = spr_mega_man_mini_buster;
				shot.image_xscale = image_xscale;
				shot.hspeed = 5.5*image_xscale;			
				audio_play_sound(PlayerShootLv1, 1, false);
			}
			break;
		case Buster.MegaBuster:
			if instance_number(objWeapon) < 1{
				var shot = instance_create_depth(x + bulletXStart, y - bulletYStart, 0, objWeapon);
				var recoil = instance_create_depth (x + bulletXStart, y - bulletYStart, 0, objWeaponRecoil);
				recoil.sprite_index = spr_mini_recoil;
				recoil.image_xscale = image_xscale;
				shot.sprite_index = (object_index == objMegaman)?spr_mega_man_mega_buster:spr_proto_man_mega_buster;
				shot.image_xscale = image_xscale;
				shot.hspeed = 5.5*image_xscale;			
				audio_play_sound(PlayerShootLv2, 1, false);
			}
			break;
			
		case Buster.GigaBuster:
			if (instance_number(objPlayerWeapon) < 1) or (override == true and object_index == objProtoMan and instance_number(objPlayerWeapon) < 3){
				var shot = instance_create_depth(x + bulletXStart, y - bulletYStart, 0, objWeapon);
				var recoil = instance_create_depth (x + bulletXStart, y - bulletYStart, 0, objWeaponRecoil);
				recoil.sprite_index = spr_mini_recoil;
				recoil.image_xscale = image_xscale;
				shot.sprite_index = (object_index == objMegaman)?spr_mega_man_giga_buster:spr_proto_man_giga_buster;
				shot.image_xscale = image_xscale;
				shot.hspeed = 5.5*image_xscale;			
				audio_play_sound(PlayerShootLv3, 1, false);
			}
			break;
		
	    default:
	        // code here
	        break;
	}


}
