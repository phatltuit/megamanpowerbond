function BassMove() {
	if climb == false{
		if !PlayerCollision(CollisionType.SlideRight, objSolid) and image_xscale == 1{
			if knockBack == false{
				if ground == true{
					if dash == false{
						if run == true{
							if shoot == true{
								hspeed = 0;
							}else{
								hspeed = runSpd;
							}
						}else{
							hspeed = 0;	
						}
					}else{
						if PlayerCollision(CollisionType.Ceil, objSolid){					
							alarm[2] = 2;
						}
						hspeed = dashSpd;
					}
				}else{
					if run == true{
						hspeed = runSpd;
					}else{
						hspeed = 0;	
					}		
				}
			}else{
				if !PlayerCollision(CollisionType.Left, objSolid){
					hspeed = -knockBackSpd;	
				}else{
					hspeed = 0;	
				}		
			}
	
		}else if !PlayerCollision(CollisionType.SlideLeft, objSolid) and image_xscale == -1{
			if knockBack == false{
				if ground == true{
					if dash == false{
						if run == true{
							if shoot == true{
								hspeed = 0;
							}else{
								hspeed = -runSpd;
							}
						}else{
							hspeed = 0;	
						}
					}else{
						if PlayerCollision(CollisionType.Ceil, objSolid){					
							alarm[2] = 2;
						}
						hspeed = -dashSpd;		
					}
				}else{
					if run == true{
						hspeed = -runSpd;
					}else{
						hspeed = 0;	
					}		
				}
			}else{
				if !PlayerCollision(CollisionType.Right, objSolid){
					hspeed = knockBackSpd;	
				}else{
					hspeed = 0;	
				}
			}		
		}else{
			if PlayerCollision(CollisionType.Ceil, objSolid) and dash == true{
				alarm[2] = 2;				
			}else{
				dash = false;	
			}
	
			hspeed = 0;
	
		}
	}else{
		jumpTime = 2;
		run = false;
		hspeed = 0;
		myLadder = instance_place(x, y+vspeed - climbDir, objLadder);
		if climbDir == 1 and myLadder >= 0{		
			vspeed = -climbSpd;		
		}else if climbDir == -1 and myLadder >= 0{		
			vspeed = climbSpd;			
		}else if climbDir == 0 and myLadder >= 0{		
			vspeed = 0;
		}	
	}






}
