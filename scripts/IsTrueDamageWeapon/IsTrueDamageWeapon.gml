// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsTrueDamageWeapon(argument0){
	var arr = [spr_mega_man_mini_buster,
	spr_mega_man_mega_buster,
	spr_mega_man_giga_buster,
	spr_bass_mega_buster_0,
	spr_bass_mega_buster_1,
	spr_bass_mega_buster_2,
	spr_bass_mega_buster_3,
	spr_bass_mini_buster,
	spr_proto_man_giga_buster,
	spr_proto_man_mega_buster,
	spr_solar_blast_b,
	spr_bass_stand_saber,
	spr_bass_jump_saber,
	spr_proto_man_stand_saber,
	spr_proto_man_stand_saber,
	spr_mega_man_stand_saber,
	spr_proto_man_jump_saber,
	spr_mega_man_jump_saber,
	spr_explosion,
	spr_mini_power_shot,
	spr_mega_power_shot
	]
	
	for (var i = 0; i < array_length(arr); ++i) {
	    if argument0 == arr[i] 
			return true;
	}
	return false;
}