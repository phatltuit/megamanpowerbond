// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SupporterDismiss(){
	if instance_exists(objSupporter){
		objSupporter.goback = true;	
		objSupporter.vspeed = 0;
		objSupporter.sprite_index = objSupporter.Dismiss;
	}
}