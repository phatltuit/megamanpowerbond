// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerHud(){	
	draw_sprite_ext(spr_player_health_bar,hp,camera_get_view_x(view_camera[0]) + 8, camera_get_view_y(view_camera[0]) + 8,1,image_yscale,image_angle,image_blend,0.85);
	if global.weaponIndex != 0 and global.weaponIndex != 9{
		draw_sprite_ext(spr_player_energy_bar,global.weaponEnergy[global.weaponIndex],camera_get_view_x(view_camera[0]) + 18, camera_get_view_y(view_camera[0]) + 8,1,image_yscale,image_angle,image_blend,0.85);
		draw_sprite_ext(spr_player_energy_bar_primary,global.weaponEnergy[global.weaponIndex],camera_get_view_x(view_camera[0]) + 18, camera_get_view_y(view_camera[0]) + 8,1,image_yscale,image_angle,global.weaponColor[global.weaponIndex],0.85);
	}
	if instance_exists(objSupporter){
		if objSupporter.supportType == Support.RushJet or objSupporter.supportType == Support.TrebleJet or objSupporter.supportType == Support.ProtoJet{
			draw_sprite_ext(spr_player_energy_bar,global.weaponEnergy[9],camera_get_view_x(view_camera[0]) + 28, camera_get_view_y(view_camera[0]) + 8,1,image_yscale,image_angle,image_blend,0.85);
			draw_sprite_ext(spr_player_energy_bar_primary,global.weaponEnergy[9],camera_get_view_x(view_camera[0]) + 28, camera_get_view_y(view_camera[0]) + 8,1,image_yscale,image_angle,global.weaponColor[9],0.85);
		}
	}
}