// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function DeactiveOrActiveObject(){
	for (var i = 0; i < instance_number(objRain); ++i) {
	    with instance_find(objRain, i){
			if InsideView()	== false{
				instance_deactivate_object(instance_find(objRain, i));	
			}else{
				instance_activate_object(instance_find(objRain, i));	
			}
		}
	}
}