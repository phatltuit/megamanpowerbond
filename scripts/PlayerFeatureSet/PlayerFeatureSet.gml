// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerFeatureSet(){
	if instance_exists(objPlayer){
		if objPlayer.object_index == objMegaman{
			global.lifeAvt = spr_menu_mega_life;
			global.specialForm = spr_weapon_menu_power_adapter;
			global.normalForm = spr_weapon_menu_mega_normal_form;
			array_set(global.weaponAvt, 0,spr_weapon_mega_buster);
			array_set(global.weaponName, 8,"Rush Coil");
			array_set(global.weaponName, 9,"Rush Jet");
			array_set(global.weaponColor, 8,make_color_rgb(216, 40,0));	
			array_set(global.weaponColor, 9,make_color_rgb(216, 40,0));
			array_set(global.weaponAvt, 8, spr_weapon_rush_coil);
			array_set(global.weaponAvt, 9, spr_weapon_rush_jet);
		}else if objPlayer.object_index == objProtoMan{
			global.lifeAvt = spr_menu_proto_life;
			global.specialForm = spr_weapon_menu_proto_strike;
			global.normalForm = spr_weapon_menu_proto_normal_form;
			array_set(global.weaponName, 8,"Proto Coil");
			array_set(global.weaponName, 9,"Proto Jet");
			array_set(global.weaponAvt, 0,spr_weapon_proto_buster);
			array_set(global.weaponColor, 8,make_color_rgb(216, 40,0));
			array_set(global.weaponColor, 9,make_color_rgb(216, 40,0));
			array_set(global.weaponAvt, 8, spr_weapon_proto_coil);
			array_set(global.weaponAvt, 9, spr_weapon_proto_jet);		
		}else{
			global.lifeAvt = spr_menu_bass_life;
			global.specialForm = spr_weapon_menu_bass_boost;
			global.normalForm = spr_weapon_menu_bass_normal_form;
			array_set(global.weaponName, 8,"Treble Coil");
			array_set(global.weaponName, 9,"Treble Jet");
			array_set(global.weaponAvt, 0,spr_weapon_bass_buster);
			array_set(global.weaponColor, 8,make_color_rgb(80, 48,152));
			array_set(global.weaponColor, 9,make_color_rgb(80, 48,152));
			array_set(global.weaponAvt, 8, spr_weapon_treble_coil);
			array_set(global.weaponAvt, 9, spr_weapon_treble_jet);
		}
	}
}