function BossInsideView() {
	if instance_exists(objBoss){
		return objBoss.x >= camera_get_view_x(view_camera[0]) 
		and objBoss.y >= camera_get_view_y(view_camera[0]) 
		and objBoss.x <= camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) 
		and objBoss.y <= camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0]);
	}
	return false;
}
