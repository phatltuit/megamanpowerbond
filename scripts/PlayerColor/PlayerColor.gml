// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerColor(){
	switch (global.weaponIndex) {
		case 9:
		case 8:
	    case 0:
			if object_index == objMegaman{
		        primaryCol = make_color_rgb(0, 120, 248);
				secondaryCol = make_color_rgb(0, 232, 216);
				outlineCol = c_black;
				
				chargeLv2Color[0] = make_color_rgb(0, 232, 216);
				chargeLv2Color[1] = make_color_rgb(0, 120, 248);
				chargeLv2Color[2] = c_black;
			}else if object_index == objProtoMan{
				primaryCol = make_color_rgb(247, 49, 0);					
				secondaryCol = make_color_rgb(189, 189, 189);
				outlineCol = c_black;
				
				chargeLv2Color[0] = make_color_rgb(189, 189, 189);
				chargeLv2Color[1] = make_color_rgb(247, 49, 0);
				chargeLv2Color[2] = c_black;
			}else if object_index == objBass{
				primaryCol = make_color_rgb(112, 112, 112);
				secondaryCol = make_color_rgb(248, 152, 56);
				outlineCol = c_black;
				
				chargeLv2Color[0] = make_color_rgb(248, 152, 56);
				chargeLv2Color[1] = make_color_rgb(112, 112, 112);
				chargeLv2Color[2] = c_black;
			}
	        break;
		
		case 1:
			primaryCol = make_color_rgb(252, 116, 96);
			secondaryCol = make_color_rgb(252, 216, 168);
			outlineCol = c_black;
	        break;
			
		case 2:
			primaryCol = make_color_rgb(0, 88, 248);
			secondaryCol = make_color_rgb(255, 163, 71);
			outlineCol = c_black;
		    break;
		
		case 3:
			primaryCol = make_color_rgb(102, 45,145);
			secondaryCol = make_color_rgb(189, 189,189);
			outlineCol = c_black;
			
			chargeLv2Color[0] = make_color_rgb(189, 189,189);
			chargeLv2Color[1] = make_color_rgb(102, 45,145);
			chargeLv2Color[2] = c_black;
		    break;
			
		case 4:				
			primaryCol = make_color_rgb(255, 160,68);
			secondaryCol = c_white;//make_color_rgb(0, 120,248);
			outlineCol = c_black;
			
			chargeLv2Color[0] = c_white;//make_color_rgb(0, 120,248);
			chargeLv2Color[1] = make_color_rgb(255, 160,68);
			chargeLv2Color[2] = c_black;
		    break;
			
		case 5:				
			primaryCol = make_color_rgb(0, 168,0);
			secondaryCol = make_color_rgb(255, 163, 71);
			outlineCol = c_black;
			
			chargeLv2Color[0] = make_color_rgb(255, 163, 71);
			chargeLv2Color[1] = make_color_rgb(0, 168,0);
			chargeLv2Color[2] = c_black;
		    break;	
			
		case 6:				
			primaryCol = make_color_rgb(88, 248,152);
			secondaryCol = make_color_rgb(0, 120, 248);
			outlineCol = c_black;
			
			chargeLv2Color[0] = make_color_rgb(0, 120, 248);
			chargeLv2Color[1] = make_color_rgb(88, 248,152);
			chargeLv2Color[2] = c_black;
		    break;			
			
		case 7:				
			primaryCol = make_color_rgb(198, 0,0);
			secondaryCol = make_color_rgb(239, 93, 33);
			outlineCol = c_black;
			
			chargeLv2Color[0] = make_color_rgb(239, 93, 33);
			chargeLv2Color[1] = make_color_rgb(198, 0,0);
			chargeLv2Color[2] = c_black;
		    break;			
		
		
		
	    default:
	        if object_index == objMegaman{
		        primaryCol = make_color_rgb(0, 120, 248);
				secondaryCol = make_color_rgb(0, 232, 216);
				outlineCol = c_black;
				
				chargeLv2Color[0] = make_color_rgb(0, 232, 216);
				chargeLv2Color[1] = make_color_rgb(0, 120, 248);
				chargeLv2Color[2] = c_black;
			}else if object_index == objProtoMan{				
				primaryCol = make_color_rgb(247, 49, 0);					
				secondaryCol = make_color_rgb(189, 189, 189);
				outlineCol = c_black;
				
				chargeLv2Color[0] = make_color_rgb(189, 189, 189);
				chargeLv2Color[1] = make_color_rgb(247, 49, 0);
				chargeLv2Color[2] = c_black;
			}else if object_index == objBass{
				primaryCol = make_color_rgb(112, 112, 112);
				secondaryCol = make_color_rgb(248, 152, 56);
				outlineCol = c_black;
				
				chargeLv2Color[0] = make_color_rgb(248, 152, 56);
				chargeLv2Color[1] = make_color_rgb(112, 112, 112);
				chargeLv2Color[2] = c_black;
			}
	        break;
	}
}