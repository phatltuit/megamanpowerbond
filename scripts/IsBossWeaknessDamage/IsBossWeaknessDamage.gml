// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsBossWeaknessDamage(argument0 , argument1){
	var boss = argument0;
	var weap = argument1;
	switch (boss) {
	    case objShadowMan:
			return weap == spr_solar_blast_a or weap == spr_solar_blast_b;
	        break;
			
		case objQuickMan:
			return weap == spr_shadow_blade;
			break;
			
		case objSlashMan:
			return weap == spr_homing_missle;
			break;
			
		case objNapalmMan:
			return weap == spr_weapon_quick_boomerang;
			break;
			
		case objIceMan:
			return weap == spr_flame_tornado;
			break;
			
		case objMagmaDragon:
			return weap == spr_sonic_slash;
			break;
			
		case objPharaohMan:
			return weap == spr_ice_slasher;
			
	    default:
	        return false;
	        break;
	}
	return false;
}