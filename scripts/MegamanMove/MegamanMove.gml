function MegamanMove() {
	if climb == false{
		if !PlayerCollision(CollisionType.Right, objSolid) and image_xscale == 1{
			if knockBack == false{
				if ground == true{
					if slide == false{
						if run == true{
							hspeed = runSpd;
						}else{
							hspeed = 0;	
						}
					}else{
						if PlayerCollision(CollisionType.Ceil, objSolid){					
							alarm[2] = 2;
						}
						hspeed = slideSpd;
					}
				}else{
					if run == true{
						hspeed = runSpd;
					}else{
						hspeed = 0;	
					}		
				}
			}else{
				//if !PlayerCollision(CollisionType.Left, objSolid){
				if place_free(x + hspeed - 1, y){
					hspeed = -knockBackSpd;	
				}else{
					hspeed = 0;	
				}		
			}		
		}else if !PlayerCollision(CollisionType.Left, objSolid) and image_xscale == -1{
			if knockBack == false{
				if ground == true{
					if slide == false{
						if run == true{
							hspeed = -runSpd;
						}else{
							hspeed = 0;	
						}
					}else{
						if PlayerCollision(CollisionType.Ceil, objSolid){					
							alarm[2] = 2;
						}
						hspeed = -slideSpd;		
					}
				}else{
					if run == true{
						hspeed = -runSpd;
					}else{
						hspeed = 0;	
					}		
				}
			}else{
				if !PlayerCollision(CollisionType.Right, objSolid){
					hspeed = knockBackSpd;	
				}else{
					hspeed = 0;	
				}
			}		
		}else{
			if PlayerCollision(CollisionType.Ceil, objSolid) and slide == true{
				alarm[2] = 2;				
			}else{
				slide = false;	
			}	
			hspeed = 0;	
		}
	}else{
		run = false;	
		hspeed = 0;
		myLadder = instance_place(x, y + vspeed - climbDir, objLadder);
		if climbDir == 1 and myLadder >= 0{		
			vspeed = -climbSpd;		
		}else if climbDir == -1 and myLadder >= 0{
			vspeed = climbSpd;		
		}else if climbDir == 0 and myLadder >= 0{
			vspeed = 0;
		}	
	}



}
