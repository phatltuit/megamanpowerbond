// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function CeilCollision(){
	//Floor
	dist = 0;
	while !place_meeting(x, bbox_top - dist, objSolid) and dist <= 16 * 2000{
		dist += 16;
	}
	if dist >= 16 * 2000{
		show_message("Cannot find the Up side end of Section !");
	}
	sectionCeil = instance_place(x, bbox_top - dist, objSolid).y;
}