// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function AfterDefeatBoss(){	
	override = false;
	audio_stop_sound(PlayerCharge);
	hspeed = 0;
	image_alpha = 1;
	if sprite_index == sprPose and round(image_index) == 3{
		if playPoseSfx	== false{
			playPoseSfx = true;
			audio_play_sound(PlayerVictoryPose, 1,false);
		}	
	}
	
	if sprite_index == sprPose and round(image_index) == image_number-2{
		if playTeleOutSfx	== false{
			playTeleOutSfx = true;
			audio_play_sound(PlayerTeleportOut, 1,false);
		}	
	}
	
	if sprite_index == sprPose and image_speed+image_index >=image_number{
		image_index = image_number-1;
		vspeed = -8;
		if y < camera_get_view_y(view_camera[0])-camera_get_view_height(view_camera[0]){
			global.Freeze = false;
			global.weaponIndex = 0;
			global.bossFight = false;
			for (var i = 0; i < 10; ++i) {
				array_set(global.weaponEnergy, i,27);				
			}
			room_restart();	
		}
	}else{
		var mySolid = instance_place(x, y+vspeed + 1, objSolid);
		if mySolid >= 0 and vspeed >= 0
		{	
			y = mySolid.y;
			while place_meeting(x, y, mySolid)
				y -= 1;
			ground = true;    
			vspeed = 0;
			if playLandSfx == false{
				audio_play_sound(PlayerLand, 1, false);
				playLandSfx = true;
			}
		}else{
			ground = false;
		}
	
	
		if ground == false{					
			gravity = grav;
			playLandSfx = false;
			sprite_index = sprJump;
		}else{	
			gravity = 0;		
			hspeed = 0;
			if defeatBoss == true{
				alarm[11] = 480;
				alarm[10] = 220;
				defeatBoss = false;
				sprite_index = sprStand;
			}
			
			if instance_exists(objBoss){
				if objBoss.ready == false{
					sprite_index = sprStand;
				}
			}
		}
	}	
}