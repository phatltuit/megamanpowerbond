/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		var box;
		if image_xscale > 0
			box = bbox_right 
		else 
			box = bbox_left;
	
		switch (phase) {
			case 0:
				TurnToPlayer();
				sprite_index = spr_ninja_joe_draw_sword;
				if collision_rectangle(x , bbox_top,x+image_xscale*64,bbox_bottom, objPlayer, false, true) != noone or  collision_rectangle(x-25 , bbox_top,x+25,bbox_bottom, objPlayerWeapon, false, true) != noone{			
					phase = 1;
				}
				break;
			
		    case 1:
		        if attack == false{
					TurnToPlayer();
					sprite_index = spr_ninja_joe_run;
					distance = abs(abs(x - objPlayer.x)-20);
					attack = true;
					timer = distance/2.5;
					hspeed = 2.5*image_xscale;
				}
				timer--;
				if timer <= 0{
					attack = false;
					phase = 2;
					hspeed = 0;
				}
		        break;
			
			case 2:
				 if attack == false{
					sprite_index = spr_ninja_joe_draw_sword;					
					attack = true;
				 }
				 
				 if sprite_index == spr_ninja_joe_draw_sword and image_index + image_speed >= image_number{
					var sword = instance_create_depth(x,y,0, objEnemyWeapon);
					sword.image_xscale = image_xscale;
					sword.sprite_index = spr_ninja_joe_slash_mask;
					sword.visible = false;
					sword.alarm[0] = 35;
					sprite_index = spr_ninja_joe_slash;	
				 }
				 
				 if sprite_index == spr_ninja_joe_slash and image_index + image_speed >= image_number{
					sprite_index = spr_ninja_joe_block;	
					alarm[3] = 120;
				 }
				 if sprite_index == spr_ninja_joe_block{
					TurnToPlayer();
					var projectile = instance_place(x,y, objPlayerWeapon);
					if  projectile >= 0{
						instance_destroy(projectile);
						audio_play_sound(EnemyBlock,1, false);
					}
				 }
			
				break;
		    default:
		        // code here
		        break;
		}	
	}
}

