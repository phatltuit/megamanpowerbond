/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		var box;
		if image_xscale > 0
			box = bbox_right 
		else 
			box = bbox_left;
		
	
		if attack == false and timer == 240{
			sprite_index = spr_bomb_joe_aim;			
			attack = true;
			timer--;
			TurnToPlayer();
		}
		
		if attack == true{
			if timer == 120 or timer == 180{			
				bomb = instance_create_depth(box, y-5, 0,objEnemyWeapon);
				bomb.sprite_index = spr_bomb_joe_weapon;
				bomb.image_xscale = image_xscale;		
				sprite_index = spr_bomb_joe_shoot;	
				bomb.alarm[0] = 140;
				if instance_exists(objPlayer){
					TurnToPlayer();
					bomb.direction = point_direction(x,y, objPlayer.x,objPlayer.y);
					bomb.speed = 3.5;
				}else{
					hspeed = 3.5*image_xscale;	
				}
			}else if timer == 150{
				sprite_index = spr_bomb_joe_aim;		
			}
		
			timer--;
			if timer < 90{
				sprite_index = spr_bomb_joe_stand;	
				TurnToPlayer();
				if timer <= 0{
					timer = 240;	
					attack = false;
				}
			}
			if sprite_index == spr_bomb_joe_stand{
				TurnToPlayer();
				var projectile = instance_place(x,y, objPlayerWeapon);
				if  projectile >= 0{
					instance_destroy(projectile);
					audio_play_sound(EnemyBlock,1, false);
				}
			}
		}
	}
}else{
	speed = 0;		
}

