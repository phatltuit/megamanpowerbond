/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_shadow_man_pose and image_index + image_speed >= image_number{
	image_index = image_number - 1;	
}

if ready == true{	
	event_inherited();	
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;
	}
	switch (phase) {
		case -1:
			TurnToPlayer();
			if ground == false{
				sprite_index = spr_shadow_man_jump;
			}else{
				if phaseOn == false{					
					hspeed = 0;
					sprite_index = spr_shadow_man_idle;
					alarm[2] = nextPhaseDelay;
					phaseOn = true;					
				}	
			}			
			break;
			if ultimate == true{
				nextPhaseDelay = 5;	
			}
			
		case 0:			
			lastPhase = 0;		
			if phaseOn == false{
				TurnToPlayer();
				sprite_index = spr_shadow_man_run;
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x)/2;
				}else{
					dist = 0;
				}
				if dist != 0{
					timer = abs(dist/4);
				}else{
					timer = 5;
				}				
				hspeed = 4*image_xscale;
				phaseOn = true;
			}
			timer--;
			if timer <= 0 and jump == false{				
				sprite_index = spr_shadow_man_jump;
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x);
				}else{
					dist = 0;
				}
				vspeed = -sqrt(abs(2*grav*120));
                var airtime = abs(2*vspeed/grav);
				hspeed = (dist/airtime)*image_xscale;								
				alarm[4] = airtime/2;
				jump = true;
			}
			
			if attack == true{				
				if ground == false{
					if sprite_index == spr_shadow_man_jump_shuriken and image_index + image_speed >= image_number{
						image_index = image_number - 1;	
						if jumpAttack == false{
							TurnToPlayer();
							var i = -20;
							repeat 3{
								var wp = instance_create_depth(x,y,-1, objShadowManWeapon);
								wp.sprite_index = spr_shadow_man_shuriken;
								wp.speed = 4.5;
								if instance_exists(objPlayer){
									wp.direction = point_direction(x,y,objPlayer.x+i,objPlayer.y);
								}else{
									vspeed = 4;
									hspeed = image_xscale*4;
								}
								i+=20;
							}
													
							jumpAttack = true;
							gravity = 0;
							vspeed = 0;
						}
					}
				}else{
					phase = -1;
					phaseOn = false;
					attack = false;
					jumpAttack = false;	
					jump = false;
					nextPhaseDelay = 10;
				}
			}
			
			break;	
			
			
		case 1:
			lastPhase = 1;
			if phaseOn == false{
				TurnToPlayer();
				sprite_index = spr_shadow_man_dashing_slash;
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x)*2;
				}else{
					dist = 0;
				}
				if dist != 0{
					timer = abs(dist/5);
				}else{
					timer = 5;
				}				
				hspeed = 5*image_xscale;
				phaseOn = true;				
			}
			timer--;
			if timer<=0{
				phase = -1;
				phaseOn = false
				nextPhaseDelay = 25;
			}		
			break;
			
		
		case 2:
			lastPhase = 2;
			TurnToPlayer();
			if phaseOn == false{
				timer = 3;
				phaseOn = true;
				sprite_index = spr_shadow_man_stand_shuriken;
			}
			if attack == false{
				TurnToPlayer();
				var wp = instance_create_depth(x + image_xscale*3,y-8, -1, objShadowManWeapon);	
				wp.sprite_index = spr_shadow_man_kunai;
				wp.image_xscale = image_xscale;
				wp.hspeed = image_xscale*4;
				
				var wp = instance_create_depth(x + image_xscale*5,y, -1, objShadowManWeapon);	
				wp.sprite_index = spr_shadow_man_kunai;
				wp.image_xscale = image_xscale;
				wp.hspeed = image_xscale*4;
				
				var wp = instance_create_depth(x + image_xscale*3,y-16, -1, objShadowManWeapon);	
				wp.sprite_index = spr_shadow_man_kunai;
				wp.image_xscale = image_xscale;
				wp.hspeed = image_xscale*4;
				alarm[3] = 40;
				attack = true;
				timer--;
			}
			if timer<=0{
				phase = -1;
				phaseOn = false;
				attack = false;
				nextPhaseDelay = 10;
			}
			break;			
			
			case 3:
				TurnToPlayer();
				if phaseOn == false{					
					phaseOn = true;
					timer = 2;
					alarm[5] = 180;
					sprite_index = spr_shadow_man_tele;
				}
				timer--;
				if timer <= 0{
					if irandom(1) == 0{
						dist = 35;
					}else{
						dist = -35;	
					}
					timer = 2;
				}
				if instance_exists(objPlayer){
					x = objPlayer.x + dist;
				}else{
					x = xstart;	
				}
				mask_index = noone;
				lastPhase = 3;		
				nextPhaseDelay = 30;
				break;
				
		case 4:
			if hp <= 14{
				ultimate = true;
				if phaseOn == false{
					TurnToPlayer();
					vspeed = -sqrt(abs(2*grav*120));
	                timer = abs(2*vspeed/grav)/2;
					sprite_index = spr_shadow_man_final_jutsu;
					phaseOn = true;
				}
				timer--;
				if timer<=0{
					vspeed = 0;
					gravity = 0;
					if timer > -60{
						if hitIndex == 1{
							hitIndex = 0;
							image_alpha = 0;
						}else{
							hitIndex = 1;	
							image_alpha = 1;
						}	
					}else{
						image_alpha = 0;
						if attack == false{		
						if rd == 0{
							rd = 1;	
						}else{
							rd = 0;	
						}
						x_pos[0] = camera_get_view_x(view_camera[0]);
						x_pos[1] = camera_get_view_x(view_camera[0])+256;
						var X = x_pos[rd];
						var Y = irandom(112)+camera_get_view_y(view_camera[0]);
						var shot = instance_create_depth(X, Y,0,objBossWeapon);
						if instance_exists(objPlayer){
							shot.direction = point_direction(X,Y,objPlayer.x, objPlayer.y);
							shot.image_yscale = rd==1?-1:1;
							shot.image_angle = point_direction(X,Y,objPlayer.x, objPlayer.y);
							shot.speed = 5.5;								
						}else{
							phase = -1;
							attack = false;
							phaseOn = false;
							count = 12;
							nextPhaseDelay = 10;
							image_alpha = 1;
						}
						shot.sprite_index = spr_shadow_man_dashing_slash;	
						shot.image_blend = c_purple;
						shot.image_alpha = 1;
						alarm[6] = 60;
						count--;
						attack = true;
					}
					}
					
					
					if count <= 0 {
						phase = -1;
						attack = false;
						phaseOn = false;
						count = 12;
						nextPhaseDelay = 10;
						image_alpha = 1;
					}	
				}
				
			}else{
				phase = -1;
				attack = false;
				phaseOn = false;
				nextPhaseDelay = 10;
				count = 12;
				image_alpha = 1;
			}
			lastPhase = 4;	
			break;				
	    default:
	        phase = 0;
	        break;
	}
}