/// @description Insert description here
// You can write your code in this editor
if instance_exists(objPlayer){
	y = ystart - 120;
	x = objPlayer.x;	
}else{
	y = ystart - 60;
	x = xstart;	
}


if instance_exists(objPlayer){
	var wp = instance_create_depth(x,y,-1, objShadowManWeapon);
	wp.sprite_index = spr_shadow_man_shuriken;
	wp.speed = 4;
	wp.direction = point_direction(x,y,objPlayer.x,objPlayer.y);
}else{
	var wp = instance_create_depth(x,y,-1, objShadowManWeapon);
	wp.sprite_index = spr_shadow_man_shuriken;
	vspeed = 4;
}
mask_index = spr_shadow_man_mask;	
phaseOn = false;
phase = -1;
