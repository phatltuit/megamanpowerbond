{
  "spriteId": {
    "name": "spr_bat_flapping",
    "path": "sprites/spr_bat_flapping/spr_bat_flapping.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": {
    "name": "spr_bat_mask",
    "path": "sprites/spr_bat_mask/spr_bat_mask.yy",
  },
  "persistent": false,
  "parentObjectId": {
    "name": "objEnemy",
    "path": "objects/objEnemy/objEnemy.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":2,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":1,"eventType":2,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":2,"eventType":2,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":50,"eventType":7,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":40,"eventType":7,"collisionObjectId":null,"parent":{"name":"objBat","path":"objects/objBat/objBat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "ENEMIES",
    "path": "folders/Objects/ENEMIES.yy",
  },
  "resourceVersion": "1.0",
  "name": "objBat",
  "tags": [],
  "resourceType": "GMObject",
}