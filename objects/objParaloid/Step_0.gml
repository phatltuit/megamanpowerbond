/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	TurnToPlayer();
	if objPlayer.ready == true{
		switch (phase) {
		    case 0:
		        if actionOn == false{
					speed = 4;
					actionOn = true;
					rd_dist = irandom(45)+20;
					direction = point_direction(x,y, objPlayer.x - rd_dist*image_xscale, objPlayer.y);
					alarm[3] = point_distance(x,y, objPlayer.x - rd_dist*image_xscale, objPlayer.y)/4;
				}
		        break;
				
			case 1:
				y = objPlayer.y-5;
				hspeed = objPlayer.hspeed;	
				if !(x >= camera_get_view_x(view_camera[0]) + 8
					and y >= camera_get_view_y(view_camera[0]) + 8
					and x <= camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) - 8 
					and y <= camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0]) - 8){
					hspeed = 0;				
				}
				timer++;
				if timer >= 120{
					timer = 0;
					var shot = instance_create_depth(x + 9*image_xscale,y, 0, objEnemyWeapon);
					shot.sprite_index = spr_para_loid_lazer;
					shot.image_xscale = image_xscale;
					shot.hspeed = image_xscale*2;
					hspeed = 0;
				}
				break;
		    default:
		        // code here
		        break;
		}
		
		
		
	}
}else{
	speed = 0;		
}

