/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{	
		if vspeed == 0{
			if run == false{
				sprite_index = spr_ice_teck_walk;				
				hspeed = 0.5*image_xscale;				
				run = true;
				alarm[2] = irandom(120)+1;				
			}
		}
		
		if sprite_index == spr_ice_teck_attack and image_index + image_speed >= image_number{
			var shot = instance_create_depth(x,bbox_bottom, 0,objEnemyWeapon);
			shot.sprite_index = spr_ice_teck_ice_shard;
			shot.gravity = 0.2;
			sprite_index = spr_ice_teck_idle;
		}
	}
}else{
	speed = 0;		
}

