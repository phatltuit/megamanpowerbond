/// @description Insert description here
// You can write your code in this editor
if ready == true{	
	event_inherited();
	var box = image_xscale==1?bbox_right:bbox_left;
	switch (phase) {
		case -1://Delay between phase
			TurnToPlayer();
			if phaseOn == false{
				if lastPhase == 2{
					alarm[2] = ultimate==true?10:30;			
				}else if lastPhase == 3{
					alarm[2] = 1;		
				}else{
					alarm[2] = ultimate==true?10:20;	
				}
				phaseOn = true;
			}
			
			hspeed = 0;
			if ground == false{
				vspeed = 0;
			}
			break;
		
		case 0://Jump and shoot 3 time
			TurnToPlayer();
			if phaseOn == false{
				if instance_exists(objPlayer){
					dist = abs(x-objPlayer.x)/3;	
				}else{
					dist = 0	
				}								
				phaseOn = true;
			}
			if ground == true{
				sprite_index = spr_quick_man_idle;
			}else{
				if attack == true{
					sprite_index = spr_quick_man_jump_atk;
				}else{
					sprite_index = spr_quick_man_jump;
				}
			}
			
			if ground == true{
				if jumpCount>0{
					var jumpH = jumpHeight[jumpCount-1];
					vspeed = -sqrt(abs(2*grav*jumpH));
	                var airtime = abs(2*vspeed/grav)
					hspeed = (dist/airtime)*image_xscale;
					attack = true;
					jumpCount--;
				}else{
					phase = -1;
					jumpCount = 3;
					phaseOn = false;
				}
			}
			
			if ground == false and attack == true and floor(vspeed) == 0{				
				var projectile = array_create(3);
				array_set(projectile, 0, instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang));
				if jumpCount == 0 or ultimate == true{
					projectile[1] = instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang);
					projectile[2] = instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang);
					if instance_exists(objPlayer){
						for (var i = 0; i < 3; ++i) {
						    projectile[i].speed = 5;
							if i == 0{
								projectile[i].direction = point_direction(projectile[i].x, projectile[i].y, objPlayer.x, objPlayer.y);
							}else if i == 1{
								projectile[i].direction = point_direction(projectile[i].x, projectile[i].y, objPlayer.x+10, objPlayer.y);
							}else if i == 2{
								projectile[i].direction = point_direction(projectile[i].x, projectile[i].y, objPlayer.x-10, objPlayer.y);	
							}
						}
					}
				}else{
					if instance_exists(objPlayer){
						array_get(projectile, 0).speed = 5;
						array_get(projectile, 0).direction = point_direction(projectile[0].x, projectile[0].y, objPlayer.x, objPlayer.y);
					}
				}
				attack = false;
			}
			
			lastPhase = 0;;
			break;
			
		case 1://Low jump to player
			lastPhase = 1;
			if phaseOn == false{
				if instance_exists(objPlayer){
					dist = abs(x-objPlayer.x)/3;	
				}else{
					dist = 0	
				}		
				phaseOn = true;
				jumpCount = 5;
			}
			if ground == true{
				sprite_index = spr_quick_man_idle;
			}else{				
				sprite_index = spr_quick_man_jump;				
			}
			
			if ground == true{
				if jumpCount>0{
					vspeed = -sqrt(abs(2*grav*(ultimate==true?3:7)));
		            var airtime = abs(2*vspeed/grav)
					hspeed = (dist/airtime)*image_xscale;
					attack = true;
					jumpCount--;
				}else{
					phase = -1;
					jumpCount = 3;
					phaseOn = false;
				}
			}
			
			if place_meeting(x + hspeed + 2*image_xscale, y, objSolid){
				hspeed = 0;
			}
			break;
			
		case 2://Throw 2 boomerang
			lastPhase = 2;
			if phaseOn == false{
				sprite_index = spr_quick_man_charge;
				
				timer = ultimate==true?20:40;
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x)+15;	
				}else{
					dist = 70;	
				}
				attack = false;
				phaseOn = true;
			}
			
			if sprite_index == spr_quick_man_charge{
				timer--;	
				if timer <= 0{
					timer = 40;
					sprite_index = spr_quick_man_stand_atk;
					attack = true;
				}
			}
			
			if sprite_index == spr_quick_man_stand_atk and attack == true{
				b1 = instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang);
				b1.gravity = grav;
				b1.vspeed = -sqrt(abs(2*b1.gravity*60));
			    var airtime = abs(2*b1.vspeed/b1.gravity);
				b1.hspeed = (dist/airtime)*image_xscale;
				b1.alarm[0] = airtime;
				
				b2 = instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang);
				b2.gravity = -grav;
				b2.vspeed = sqrt(abs(2*b2.gravity*60));//Reverse direction of kinematic equation
			    var airtime = abs(2*b2.vspeed/b2.gravity);
				b2.hspeed = (dist/airtime)*image_xscale;
				b2.alarm[0] = airtime;
				
				if ultimate == true{
					b3 = instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang);
					b3.gravity = grav;
					b3.vspeed = -sqrt(abs(2*b3.gravity*90));
				    var airtime = abs(2*b3.vspeed/b3.gravity);
					b3.hspeed = ((dist+20)/airtime)*image_xscale;
					b3.alarm[0] = airtime;
				
					b4 = instance_create_depth(box, bbox_top+12,0, objQuickManSecondBoomerang);
					b4.gravity = -grav;
					b4.vspeed = sqrt(abs(2*b4.gravity*90));//Reverse direction of kinematic equation
				    var airtime = abs(2*b4.vspeed/b4.gravity);
					b4.hspeed = ((dist+20)/airtime)*image_xscale;
					b4.alarm[0] = airtime;		
				}
				
				phase = -1;
				attack = false;
				phaseOn = false;
			}
			break;
		
		case 3://Increase speed, create boomerang shield
			lastPhase = 3;
			if hp <= 14 and ultimate == false{
				if phaseOn == false{
					sprite_index = spr_quick_man_teleport;
					phaseOn = true;
					ultimate = true;
					timer = 40;
					var b1 = instance_create_depth(x,y+25, 0, objQuickManMainBoomerang);
					b1.target = objQuickMan;
					var b2 = instance_create_depth(x,y-25, 0, objQuickManMainBoomerang);
					b2.target = objQuickMan;
					var b3 = instance_create_depth(x+25,y, 0, objQuickManMainBoomerang);
					b3.target = objQuickMan;
					var b4 = instance_create_depth(x-25,y, 0, objQuickManMainBoomerang);
					b4.target = objQuickMan;
					alarm[3] = 120;
				}
			}else{
				attack = false;
				phaseOn = false;
				timer--;
				if timer<=0{
					phase = -1;
					timer = 40;
				}
			}
			break;
		
		
	    default:
	        phase = 0;
	        break;
	}
}