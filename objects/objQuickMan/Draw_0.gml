/// @description Insert description here
// You can write your code in this editor
draw_self();
if ultimate == true and phase != 3{
	dash_shadow = part_type_create();
	part_type_sprite(dash_shadow, sprite_index, 0 , 0, 1);
	part_type_size(dash_shadow, 1, 1, 0 ,0);
	part_type_life(dash_shadow, 10, 20);
	part_type_blend(dash_shadow, true);
	part_type_alpha3(dash_shadow, 1,0.5,0.3);
	part_type_color1(dash_shadow, c_red);
	if image_index mod 2 == 0{
		part_particles_create_color(global.particleSystem, x, y, dash_shadow, c_red,1);	
	}else{
		part_particles_create_color(global.particleSystem, x+2, y, dash_shadow, c_red,1);
		part_particles_create_color(global.particleSystem, x-2, y, dash_shadow, c_red,1);
		part_particles_create_color(global.particleSystem, x, y+2, dash_shadow, c_red,1);
		part_particles_create_color(global.particleSystem, x, y-2, dash_shadow, c_red,1);
	}
}
event_inherited();
