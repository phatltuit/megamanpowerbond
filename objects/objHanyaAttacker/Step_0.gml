/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{	
		grav = 0;
		if run == false{
			sprite_index = spr_hanya_attacker_patrol;				
			hspeed = spd*image_xscale;				
			run = true;
			alarm[2] = timer;				
		}
		
		if run == true and sprite_index == spr_hanya_attacker_turn and image_index + image_speed >= image_number{
			sprite_index = spr_hanya_attacker_patrol;
			if image_xscale == -1{
				image_xscale = 1;	
			}else{
				image_xscale = -1;	
			}				
			run = false;
		}
		
		var playerWeapon = collision_rectangle(x,bbox_top,x+image_xscale*24, bbox_bottom, objPlayerWeapon, false,true);
		if playerWeapon >= 0 and image_xscale != playerWeapon.image_xscale{			
			instance_destroy(playerWeapon);		
			audio_play_sound(EnemyBlock, 1,false);
				
		}
	}	
	
}else{
	speed = 0;	
	if InsideView(){
		if actionOn == false{
			actionOn = true;
			alarm[5] = 20;
		}
	}
}


