/// @description Insert description here
// You can write your code in this editor
if enable == true{
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;	
	}
	sprite_index = spr_bunby_tank_shoot_b;
	var shot = instance_create_depth(box, y-8, 0,objEnemyWeapon);
	shot.sprite_index = spr_bunby_tank_rocket;
	shot.hspeed = 3*image_xscale;
	shot.image_xscale = image_xscale;
	alarm[4] = 20;
	image_speed = 0;
}