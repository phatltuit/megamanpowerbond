{
  "spriteId": {
    "name": "spr_para_loid",
    "path": "sprites/spr_para_loid/spr_para_loid.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": {
    "name": "spr_para_loid_mask",
    "path": "sprites/spr_para_loid_mask/spr_para_loid_mask.yy",
  },
  "persistent": false,
  "parentObjectId": {
    "name": "objEnemy",
    "path": "objects/objEnemy/objEnemy.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":2,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":1,"eventType":2,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":2,"eventType":2,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":50,"eventType":7,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":40,"eventType":7,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":3,"eventType":2,"collisionObjectId":null,"parent":{"name":"objParaloid","path":"objects/objParaloid/objParaloid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "ENEMIES",
    "path": "folders/Objects/ENEMIES.yy",
  },
  "resourceVersion": "1.0",
  "name": "objParaloid",
  "tags": [],
  "resourceType": "GMObject",
}