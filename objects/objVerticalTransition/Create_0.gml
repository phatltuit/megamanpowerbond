/// @description Insert description here
// You can write your code in this editor
event_inherited();
instance_create_depth(x,y,0,objZoneFloor);
instance_create_depth(x,y,0,objZoneCeil);
xIndex = x + 8;
for (var i = 0; i < 16; ++i) {
    instance_create_depth(xIndex,y-8, 11,objZoneGoDown);
	instance_create_depth(xIndex,y+8, 11,objZoneGoUp);
	xIndex+=16;
}

