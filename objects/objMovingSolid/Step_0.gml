/// @description Insert description here
// You can write your code in this editor
if vspeed < 0{
	platform_direction = MovingSolidDirection.up;
} else if vspeed > 0 {
	platform_direction = MovingSolidDirection.down;
}

if place_meeting(x, y-1, objPlayer){
	if platform_direction == MovingSolidDirection.up {
		objPlayer.y = ceil(bbox_top-(objPlayer.bbox_bottom - objPlayer.y)-1-speed);		
	} else if platform_direction == MovingSolidDirection.down{
		objPlayer.y = ceil(bbox_top-(objPlayer.bbox_bottom - objPlayer.y)-1+speed);
	}
}