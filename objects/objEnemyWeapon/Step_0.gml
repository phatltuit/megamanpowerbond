/// @description Insert description here
// You can write your code in this editor
if sprite_index == spr_acid_joe_weapon{
	if place_meeting(x,y,objSolid) or place_meeting(x,y, objPlayer){
		hspeed = 0;
		sprite_index = spr_acid_joe_weapon_explosion;	
	}
}
if sprite_index == spr_acid_joe_weapon_explosion and image_index + image_speed >= image_number{
	instance_destroy();
}

if sprite_index == spr_bomb_joe_weapon{
	if place_meeting(x,y,objSolid){
		move_bounce_solid(true);
	}
	if place_meeting(x,y, objPlayer){
		instance_destroy();	
	}
}

if sprite_index == spr_bomb_joe_weapon_explosion and image_index + image_speed >= image_number{
	instance_destroy();
}