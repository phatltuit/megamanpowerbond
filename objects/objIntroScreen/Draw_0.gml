/// @description Insert description here
// You can write your code in this editor
draw_self();
if timer >= 80{
	draw_sprite_ext(spr_intro_screen_logo, 0,x,y,image_xscale,image_yscale, image_angle, image_blend,timer/600);
}
if timer >= 220{
	draw_sprite_ext(spr_intro_screen_3_player, 0,x,y,image_xscale,image_yscale, image_angle, image_blend,timer/600);	
}
if timer >= 360{
	draw_sprite_ext(spr_intro_screen_small_desc, 0,x,y,image_xscale,image_yscale, image_angle, image_blend,timer/600);	
}

if timer >= 560{
	draw_sprite_ext(spr_intro_screen_title, 0,x,y,image_xscale,image_yscale, image_angle, image_blend,timer/600);	
}

if timer >= 460 and timer < 640{
	draw_sprite_ext(spr_intro_screen_title_elec_wave, irandom(sprite_get_number(spr_intro_screen_title_elec_wave)-1),x,y-15,image_xscale,image_yscale, image_angle, image_blend,1);	
	draw_sprite_ext(spr_intro_screen_title_elec_wave, irandom(sprite_get_number(spr_intro_screen_title_elec_wave)-1),x,y+15,image_xscale,image_yscale, image_angle, image_blend,1);	
}



if timer >= 700 {
	if start == false{
		draw_sprite_ext(spr_intro_screen_start_option, 0,x,y,image_xscale,image_yscale, image_angle, image_blend,timer mod 5?1:0);	
	}else{
		draw_sprite_ext(spr_intro_screen_2_option, 0,x,y,image_xscale,image_yscale, image_angle, image_blend,1);		
		draw_sprite_ext(spr_intro_screen_arrow, 0,4,option[option_index],image_xscale,image_yscale, image_angle, image_blend,1);
	}
}

draw_text(x,y,timer);