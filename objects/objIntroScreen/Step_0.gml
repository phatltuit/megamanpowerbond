/// @description Insert description here
// You can write your code in this editor
timer++;
if timer == 80{
	audio_stop_sound(music);
	audio_play_sound(music, 1, true);
}
if start == false{
	if keyboard_check_released(global.keyStart){
		timer = 2000;
		start = true;	
		if !audio_is_playing(music){
			audio_play_sound(music, 1, true);
		}
		audio_play_sound(sfxMenuSelect, 1, false);
	}			
}else{
	if keyboard_check_pressed(global.keyUp){
		option_index--;
		audio_stop_sound(sfxMenuMove);
		audio_play_sound(sfxMenuMove, 1, false);
	}else if keyboard_check_pressed(global.keyDown){
		audio_stop_sound(sfxMenuMove);
		audio_play_sound(sfxMenuMove, 1, false);
		option_index++;
	}
	if option_index < 0{
		option_index = 2;	
	}else if option_index > 2{
		option_index = 0;	
	}
	
	if keyboard_check_pressed(vk_escape){
		game_end();	
	}
	
	if keyboard_check_released(global.keyStart) or keyboard_check_released(global.keyShoot){
		switch (option_index) {
		    case 0:				
		        // go to player choose screen
				audio_stop_sound(music);
				audio_play_sound(sfxMenuSelect, 1, false);
				room_goto(PLAYER_SELECT);
		        break;
				
			case 1:
		        room_goto(OPTION);
				audio_stop_sound(music);
				audio_play_sound(sfxMenuSelect, 1, false);
		        break;
				
			case 2:
		        // exit game
				game_end();
		        break;
		}
			
	}
}

