/// @description Insert description here
// You can write your code in this editor
if instance_exists(objTeleportEnd){	
	var destination = noone;
	for (var i = 0; i < instance_number(objTeleportEnd); ++i) {
		var dest = instance_find(objTeleportEnd, i);
	    if dest != noone and dest.index == index{
			destination = dest;
			break;
		}
	}
	if destination == noone{
		global.Freeze = false;	
		show_message("There's no teleport end point !");
	}else{
		objPlayer.x = destination.x;
		objPlayer.y = destination.y;
		with objPlayer{
			PlayerCameraInit();		
		}
		global.Freeze = false;
		destination.receivePlayer = true;
		destination.alarm[0] = 90;
		tele = false;
		
	}
	//Stopsound
	//Playsound
}else{
	global.Freeze = false;	
	on = false;
	show_message("There's no teleport end point !");
}