/// @description Insert description here
// You can write your code in this editor
if surface == true{
	var myPlayer = collision_rectangle(x-8,y-8,x+8,y-7, objPlayer,false,true);
	if myPlayer >= 0 and myPlayer.vspeed != 0{
		if instance_number(objWaterSplash) < 1{
			instance_create_depth(myPlayer.x, bbox_top, 0, objWaterSplash);	
			audio_stop_sound(WaterSplash);
			audio_play_sound(WaterSplash, 1, false);
		}
	}
}