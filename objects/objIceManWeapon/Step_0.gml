/// @description Insert description here
// You can write your code in this editor
if sprite_index == spr_ice_man_ice_ball_gathering and image_index + image_speed >= image_number{
	sprite_index = spr_ice_man_ice_ball;
	mask_index = spr_ice_man_ice_ball_mask;		
}
if sprite_index == spr_ice_man_ice_ball{	
	if !place_meeting(x,y,objPlayer) and !place_meeting(x,y,objIceMan){
		if ground == true{
			var snowBallSolid = instance_create_depth(x, y, -1, objIceManSolid);
			snowBallSolid.visible = true;
			snowBallSolid.sprite_index = spr_ice_man_ice_ball;
			snowBallSolid.mask_index = spr_ice_man_ice_ball_mask;
			snowBallSolid.image_speed = 0;
			instance_destroy();
		}	
	}
	BaseCollision();	
}

if sprite_index == spr_ice_man_ice_pillar{
	if image_index + image_speed >= image_number{
		instance_destroy();	
	}
}
