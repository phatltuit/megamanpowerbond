/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		var box;
		if image_xscale > 0
			box = bbox_right 
		else 
			box = bbox_left;
		
	
		if attack == false and timer == 240{
			sprite_index = spr_acid_joe_aim;			
			attack = true;
			timer--;
			TurnToPlayer();
		}
		
		if attack == true{
			if timer == 120 or timer == 180{			
				flame = instance_create_depth(box, y-5, 0,objEnemyWeapon);
				flame.sprite_index = spr_acid_joe_weapon;
				flame.image_xscale = image_xscale;		
				sprite_index = spr_acid_joe_shoot;	
				flame.hspeed = 2.5*image_xscale;
			}else if timer == 150{
				sprite_index = spr_acid_joe_aim;		
			}
			
		
			timer--;
			if timer < 90{
				sprite_index = spr_acid_joe_stand;	
				TurnToPlayer();
				if timer <= 0{
					timer = 240;	
					attack = false;
				}
			}
		}
	}
}else{
	speed = 0;		
}

