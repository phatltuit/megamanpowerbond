/// @description Insert description here
// You can write your code in this editor
if global.weaponIndex == 2 and sprite_index != spr_explosion{
	var objWeapon = objPlayerWeapon;
	var expl = instance_create_depth(x,y,-1,objWeapon);
	expl.sprite_index = spr_explosion;
	expl.alarm[0] = 5;
	instance_destroy(objTarget);
}
if instance_exists(objPlayer){
	if sprite_index == spr_ice_slasher{
		objPlayer.slam = false;
		objPlayer.xspeed = 0;
	}
}