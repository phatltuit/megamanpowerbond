/// @description Insert description here
// You can write your code in this editor
EnemyCollision();
if enable == true{
	var projectile = instance_place(x, y, objPlayerWeapon);
	if projectile >= 0  and sprite_index != spr_ninja_joe_block and sprite_index != spr_bomb_joe_stand{
		if projectile.damage >= hp{
			audio_stop_sound(PlayerHitEnemy);
			audio_play_sound(PlayerHitEnemy, 1, false);			
			instance_create_depth(x, y, 0, objEnemyExplosion);
			//if global.weaponIndex != 0 and global.weaponIndex != 3 and projectile.sprite_index != spr_solar_blast_b{
			if projectile.sprite_index != spr_mega_man_giga_buster
			and projectile.sprite_index != spr_proto_man_giga_buster
			and projectile.sprite_index != spr_proto_man_mega_buster
			and projectile.sprite_index != spr_mega_man_mega_buster
			and IsDestroyableWeapon(projectile){
				instance_destroy(projectile);	
			}			
			hp-= projectile.damage;
			enable = false;
			visible = false;
			//if object_index == objBunbyTank{
			//	instance_create_depth(x,y-14,0, objBunbyGyro);
			//}
		}else{
			
			//if projectile.sprite_index != spr_explosion and global.weaponIndex != 3 and projectile.sprite_index != spr_solar_blast_b{
			if IsDestroyableWeapon(projectile){
				audio_stop_sound(PlayerHitEnemy);
				audio_play_sound(PlayerHitEnemy, 1, false);
				if projectile.sprite_index == spr_mega_power_shot or projectile.sprite_index == spr_mini_power_shot{
					hp-= projectile.damage*2;
					hspeed = projectile.image_xscale*4;					
					alarm[11] = 10;					
				}else{
					hp-= projectile.damage;
				}
				alarm[0] = 1;
				instance_destroy(projectile);
			}
			else{
				if hitTimer == 5{
					audio_stop_sound(PlayerHitEnemy);
					audio_play_sound(PlayerHitEnemy, 1, false);
					hitTimer--;					
					hp-= projectile.damage;	
					alarm[0] = 1;
				}else if hitTimer < 5 and hitTimer > 0{
					hitTimer --;	
				}else{
					hitTimer = 5;
				}	
			}	
		}
	}else{
		hitTimer = 20;
	}		
}





