/// @description Insert description here
// You can write your code in this editor
if isKeyListen == false{
	if keyboard_check_pressed(global.keyUp){	
		audio_stop_sound(sfxMenuMove);
		audio_play_sound(sfxMenuMove, 1, false);
		row--;
		if row < 0{
			row = 7;
		}
	
	}
	if keyboard_check_pressed(global.keyDown){
		audio_stop_sound(sfxMenuMove);
		audio_play_sound(sfxMenuMove, 1, false);
		row++;
		if row > 7{
			row = 0;
		}
	
	}

	if row != 5 and row != 6{
		if keyboard_check_pressed(global.keyRight){
			audio_stop_sound(sfxMenuMove);
			audio_play_sound(sfxMenuMove, 1, false);
			col++;
			if col > 1{
				col = 0;			
			}	
		}
		if keyboard_check_pressed(global.keyLeft){
			audio_stop_sound(sfxMenuMove);
			audio_play_sound(sfxMenuMove, 1, false);
			col--;
			if col < 0{
				col = 1;			
			}
		}
	}else{
		if keyboard_check_pressed(global.keyRight){
			audio_stop_sound(sfxMenuMove);
			audio_play_sound(sfxMenuMove, 1, false);
			if row == 5{
				musicXcol = clamp(musicXcol+1, 0, 10);
				global.MusicVolume = musicXcol/10;
				for (var i = 0; i < array_length(global.MusicArr); ++i) {
				    audio_sound_gain(global.MusicArr[i], global.MusicVolume, 1); 
				} 
			}else{
				sfxXcol = clamp(sfxXcol+1, 0, 10);	
				global.SFXVolume =sfxXcol/10;
				for (var i = 0; i < array_length(global.SFXArr); ++i) {
				    audio_sound_gain(global.SFXArr[i], global.SFXVolume, 1); 
				}
			}
		}
		if keyboard_check_pressed(global.keyLeft){
			audio_stop_sound(sfxMenuMove);
			audio_play_sound(sfxMenuMove, 1, false);
			if row == 5{
				musicXcol = clamp(musicXcol-1, 0, 10);
				global.MusicVolume = musicXcol/10;
				for (var i = 0; i < array_length(global.MusicArr); ++i) {
				    audio_sound_gain(global.MusicArr[i], global.MusicVolume, 1); 
				} 
			}else{
				sfxXcol = clamp(sfxXcol-1, 0, 10);	
				global.SFXVolume =sfxXcol/10;
				for (var i = 0; i < array_length(global.SFXArr); ++i) {
				    audio_sound_gain(global.SFXArr[i], global.SFXVolume, 1); 
				}
			}
		}	
	}

	if keyboard_check_released(global.keyStart){
		switch (row) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
				alarm[1] = 4;
				wait = true;
				isKeyListen = true;
				keyboard_lastchar = "";
				break;
		
		    case 7:
				if col == 0{
					audio_play_sound(sfxMenuSelect, 1, false);
					global.keyUp = keyUp;
					global.keyDown = keyDown;
					global.keyLeft = keyLeft;
					global.keyRight = keyRight;
					global.keyStart = keyStart;
					global.keyShoot = keyShoot;
					global.keyJump = keyJump;
					global.keySlide = keySlide;
					global.keyNextWp = keyNextWp;
					global.keyPrevWp = keyPrevWp;	        
				}else{
					alarm[0] = 2;
					audio_play_sound(sfxError, 1, false);
				}
		        break;
		}	
	}
}else{
	if wait == false{
		var _key = keyboard_lastchar;
		show_debug_message(_key);
		
		if _key != ""{
			audio_stop_sound(sfxMenuSelect);
			audio_play_sound(sfxMenuSelect, 1, false);
			if col == 0{
				switch (row) {
					case 0:		
						global.keyUp = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;
					case 1:
						keyDown = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;
					case 2:
						keyLeft = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;
					case 3:
						keyRight = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;			
					case 4:
						keyStart = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);
						isKeyListen = false;
						break;
				}
			}else{
				switch (row) {
					case 0:		
						keyShoot = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;
					case 1:
						keyJump = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;
					case 2:
						keySlide = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;
					case 3:
						keyNextWp = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);	
						isKeyListen = false;
						break;			
					case 4:
						keyPrevWp = ord(string_upper(_key));//chr(global._fhinputKeys[_key]);
						isKeyListen = false;
						break;
				}
			}
		}
	}
}

selector.y = y + Y[row];
if row < 5{
	selector.x = x + X[col]-10;
}else if row == 5 or row == 6{
	selector.x = x + X[0]-10;	
}else{
	selector.x = x + col==0?58:140;	
}