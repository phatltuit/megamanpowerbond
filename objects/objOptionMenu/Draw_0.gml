/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_set_font(OptionTxt);

draw_text_color(x+X[0]+52, y+Y[0]-4, global._fhinputKeys[keyUp], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 0, 0));
draw_text_color(x+X[0]+52, y+Y[1]-4, global._fhinputKeys[keyDown], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 0, 1));
draw_text_color(x+X[0]+52, y+Y[2]-4, global._fhinputKeys[keyLeft], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 0, 2));
draw_text_color(x+X[0]+52, y+Y[3]-4, global._fhinputKeys[keyRight], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 0, 3));
draw_text_color(x+X[0]+52, y+Y[4]-4, global._fhinputKeys[keyStart], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 0, 4));

draw_text_color(x+X[1]+77, y+Y[0]-4, global._fhinputKeys[keyShoot], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 1, 0));
draw_text_color(x+X[1]+77, y+Y[1]-4, global._fhinputKeys[keyJump], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 1, 1));
draw_text_color(x+X[1]+77, y+Y[2]-4, global._fhinputKeys[keySlide], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 1, 2));
draw_text_color(x+X[1]+77, y+Y[3]-4, global._fhinputKeys[keyNextWp], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 1, 3));
draw_text_color(x+X[1]+77, y+Y[4]-4, global._fhinputKeys[keyPrevWp], c_white,c_white,c_white,c_white, InputKeySelect(col,row, 1, 4));

draw_sprite(spr_option_menu_sound_scroll, 0,x+77+musicXcol*10,y+Y[5]+1);

draw_sprite(spr_option_menu_sound_scroll, 0,x+77+sfxXcol*10,y+Y[6]+1);
if isKeyListen == true{
	draw_sprite(spr_option_menu_input_key, 0,x,y);	
}
