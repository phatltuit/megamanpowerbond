/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		speed = 0.5;
		direction = point_direction(x, y, objPlayer.x, objPlayer.y);
	}
}else{
	speed = 0;		
}

