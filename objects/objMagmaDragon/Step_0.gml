/// @description Insert description here
// You can write your code in this editor
if ready == true{	
	event_inherited();	
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;
	}
	switch (phase) {
		case -1:	
			if ground == false{
				sprite_index = spr_magma_dragon_land; 				
			}else{
				TurnToPlayer();
				sprite_index = spr_magma_dragon_idle; 	
				if phaseOn == false{
					phaseOn = true;
					alarm[2] = nextPhaseDelay;
				}
			}			
			break;
		
		case 0:
			if phaseOn == false{
				TurnToPlayer();
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x);
				}else{
					dist = abs(x-(camera_get_view_x(view_camera[0])+camera_get_view_width(view_camera[0])/2));
				}
				phaseOn = true;
				vspeed = -sqrt(2*grav*80);
				timer[0] = abs(2*vspeed/grav);
				hspeed = (dist/timer[0])*image_xscale;
				sprite_index = spr_magma_dragon_move;
			}
			timer[0]--;
			if timer[0] <= 0{
				if  timer[0] >-10 and  timer[0] <= 0{
					sprite_index = spr_magma_dragon_idle;
				}
				hspeed = 0;					
				if timer[0] <= -10{
					if timer[0] == 10{	
						TurnToPlayer();
						sprite_index = spr_magma_dragon_upper_cut;		
					}
					if image_index + image_speed >= image_number and attack == false{
						image_index = image_number-1;
						sprite_index = spr_magma_dragon_upper_cut_on_air;		
						attack = true;
						i = bbox_left;
						repeat 2{
							var shot = instance_create_depth(i,y, 0, objMagmaDragonFlame);
							shot.sprite_index = spr_magma_dragon_flame_tornado_a;
							shot.alarm[0] = 60;
							i = bbox_right;
						}
						timer[1] = 30;
						vspeed = -8;
					}
					timer[1]--;
					if timer[1] <= 0{
						phase = -1;
						nextPhaseDelay = 60;
						attack = false;
						phaseOn = false;
					}
				}
				
			}
			lastPhase = 0;
			break;
			
		case 1:
			if phaseOn == false{
				TurnToPlayer();
				if count == 0{
					sprite_index = spr_magma_dragon_stand_palm;
				}else{
					sprite_index = spr_magma_dragon_crouch_palm;	
				}				
				image_index = 0;
				phaseOn = true;
				timer = 30;
			}
			
			if image_index + image_speed >= image_number{
				timer--;
				image_index = image_number-1;
				if attack == false{
					attack = true;
					var shot = instance_create_depth(x,y+(count==0?-5:3), 0, objMagmaDragonFlame);
					shot.sprite_index = spr_magma_dragon_flame_tornado_b;
					shot.hspeed = 6*image_xscale;
					shot.image_xscale = image_xscale;
					count++;
				}		
			}
			
			if timer<=0{
				phaseOn = false;
				attack = false;
				if count >= 2{
					phase = -1;
					nextPhaseDelay = 10;
					count = 0;
				}
			}
			
			lastPhase = 1;
			break;
			
	    default:
	        phase = 0;
	        break;
	}
}