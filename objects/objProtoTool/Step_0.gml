/// @description Insert description here
// You can write your code in this editor
if goback == false{	
	if canTransform == false{	
		if collision_rectangle(camera_get_view_x(view_camera[0]), bbox_top,camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]),bbox_bottom, objPlayer, false, true) != noone{
			canTransform = true;		
		}
	}else{
		if y > camera_get_view_y(view_camera[0])+ camera_get_view_height(view_camera[0]) +13 
		and x > camera_get_view_x(view_camera[0])+16 	
		and x < camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0])-16{
			instance_destroy();	
		}
		
		BaseCollision();
		if ground == true and transform == false{
			transform = true;
			sprite_index = spr_proto_tool_transform;
		}			
		
		if supportType == Support.ProtoCoil{		
			
			if sprite_index == spr_proto_tool_transform and image_index+image_speed >= image_number{
				alarm[1] = 300;
				sprite_index = spr_proto_coil_ready;
			}
			if sprite_index == spr_proto_coil_ready{
				var myPlayer = collision_rectangle(bbox_left,bbox_top-1, bbox_right,bbox_top, objPlayer, false, true);
				if myPlayer >=0 and myPlayer.vspeed > 0 and myPlayer.y < y {
					while place_meeting(x,y-1, myPlayer){
						myPlayer.y-=1;		
					}			
					myPlayer.vspeed = -sqrt(2*myPlayer.grav*100);
					airtime = abs(2*myPlayer.vspeed/myPlayer.grav);
					myPlayer.alarm[7] = airtime/2;
					myPlayer.supportJump = true;
					sprite_index = spr_proto_coil_push_up;	
					alarm[1] = airtime/2;
				}	
			}
		}else if supportType == Support.ProtoJet{			
			if sprite_index == spr_proto_tool_transform and image_index+image_speed >= image_number{
				sprite_index = spr_proto_jet_fly;
				mask_index = spr_proto_jet_fly_mask;
				grav = 0;
			}
			if sprite_index == spr_proto_jet_fly{
				var player = instance_place(x,y-1, objPlayer);
				if player >= 0 and player.y < y and player.ground == false and player.vspeed >= 0 and player.climb == false{
					if player.supportFly == false{
						player.supportFly = true;
						dist = abs(point_direction(x,y,player.x,player.y));						
					}
				}
				
				if instance_exists(objPlayer){
					if objPlayer.supportFly == true and objPlayer.ground == true and objPlayer.climb == false{
						depth = objPlayer.depth - 1;
						image_xscale = objPlayer.image_xscale;					
						x = objPlayer.x + objPlayer.hspeed;	
						y = objPlayer.bbox_bottom + abs(y - bbox_top) + objPlayer.vspeed;
						timer=180;
					}else{
						timer--;
						if timer <= 0{
							goback = true;	
							sprite_index = spr_proto_tool_dismiss;
						}
						if global.weaponEnergy[9] <= 0{
							global.weaponEnergy[9] = 0;
							goback = true;	
							sprite_index = Dismiss;
							objPlayer.supportFly = false;
						}
					}
				}else{
					goback = true;	
					sprite_index = spr_proto_tool_dismiss;
				}
			}
		}
	}
}else{	
	if sprite_index == spr_proto_tool_dismiss and image_index+image_speed >= image_number{
		image_index = image_number-1;
		grav = 0.25;
		gravity_direction = 90;
		gravity = grav*2;
	}
	
	if y < camera_get_view_y(view_camera[0])-24{
		instance_destroy();	
	}
}