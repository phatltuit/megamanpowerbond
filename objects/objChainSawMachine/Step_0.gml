/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		var box;
		if image_xscale > 0
			box = bbox_right 
		else 
			box = bbox_left;
	
		switch (phase) {
				case 1:
					sprite_index = spr_chain_saw_robot_walk;
					if patrol == false{
						timer = 60;
						patrol = true;
						hspeed = 0.5;
					}
					timer--;
					if timer<=0{
						image_xscale = -image_xscale;
						hspeed = 0.5*image_xscale;
						timer = 120;
					}
					var player = collision_rectangle(xstart, bbox_top, xstart+64*image_xscale, bbox_bottom, objPlayer, false, true);
					var projectile = collision_rectangle(bbox_right+20, bbox_top, bbox_left-20, bbox_bottom, objPlayerWeapon, false, true);
					if player >= 0 or projectile >=0{
						phase = 2;
						timer = 180;
					}
					break;					
					
					
				case 2:
					if timer == 180{
						TurnToPlayer();
						dist = 0;
						if instance_exists(objPlayer){
							dist = abs(x-objPlayer.x);
						}
						hspeed = 1*image_xscale;
						chasingTimer = dist>30?abs(dist-20):dist;
						timer--;	
						sprite_index = spr_chain_saw_robot_rush;	
					}
					chasingTimer--;
					if chasingTimer<= 0 and chasingTimer >= -20{
						hspeed = 0;
						sprite_index = spr_chain_saw_robot_attack_ready;			
					}					
					if chasingTimer < -20 and chasingTimer>-80{
						if attack == false{
							attack = true;
							saw = instance_create_depth(x,y,0,objEnemyWeapon);
							saw.visible = false;
							saw.image_xscale = image_xscale;
							saw.sprite_index = spr_chain_saw_robot_attack_mask;
							saw.alarm[0] = 60;
						}
						sprite_index = spr_chain_saw_robot_attack;							
					}
					
					if chasingTimer < -80{
						timer = 180;	
						instance_destroy(saw);
						attack = false;
					}
					break;
			
		    default:
		        // code here
		        break;
		}	
	}
}

