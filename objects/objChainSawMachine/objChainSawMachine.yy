{
  "spriteId": {
    "name": "spr_chain_saw_robot_stand",
    "path": "sprites/spr_chain_saw_robot_stand/spr_chain_saw_robot_stand.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": {
    "name": "spr_chain_saw_robot_mask",
    "path": "sprites/spr_chain_saw_robot_mask/spr_chain_saw_robot_mask.yy",
  },
  "persistent": false,
  "parentObjectId": {
    "name": "objEnemy",
    "path": "objects/objEnemy/objEnemy.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"objChainSawMachine","path":"objects/objChainSawMachine/objChainSawMachine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"objChainSawMachine","path":"objects/objChainSawMachine/objChainSawMachine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":2,"collisionObjectId":null,"parent":{"name":"objChainSawMachine","path":"objects/objChainSawMachine/objChainSawMachine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":1,"eventType":2,"collisionObjectId":null,"parent":{"name":"objChainSawMachine","path":"objects/objChainSawMachine/objChainSawMachine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":50,"eventType":7,"collisionObjectId":null,"parent":{"name":"objChainSawMachine","path":"objects/objChainSawMachine/objChainSawMachine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":40,"eventType":7,"collisionObjectId":null,"parent":{"name":"objChainSawMachine","path":"objects/objChainSawMachine/objChainSawMachine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "ENEMIES",
    "path": "folders/Objects/ENEMIES.yy",
  },
  "resourceVersion": "1.0",
  "name": "objChainSawMachine",
  "tags": [],
  "resourceType": "GMObject",
}