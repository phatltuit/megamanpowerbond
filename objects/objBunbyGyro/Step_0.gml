/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		TurnToPlayer();
		if ground == true{
			if actionOn == false{
				actionOn = true;
				alarm[2] = 60;
			}
		}
		
		if sprite_index == spr_bunby_gyro_accel and image_index + image_speed >= image_number{
			sprite_index = spr_bunby_gyro_fly;
		}
		if sprite_index == spr_bunby_gyro_fly{
			TurnToPlayer();
			grav = 0;
			speed = 0.5;
			direction = point_direction(x, y, objPlayer.x, objPlayer.y);
		}
	}
}else{
	speed = 0;	
	instance_destroy();
}

