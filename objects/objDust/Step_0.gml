/// @description Insert description here
// You can write your code in this editor
if sprite_index == spr_bass_air_jump{
	if instance_exists(objPlayer){
		if image_angle != 90 and image_angle != -90{
			x = objPlayer.x;
			y = objPlayer.bbox_bottom + 12;
		}else{
			if objPlayer.image_xscale == 1{
				box = objPlayer.bbox_left - 1;
			}else{
				box = objPlayer.bbox_right + 1;	
			}
			x = box;	
		}
	}else{
		instance_destroy();	
	}
}