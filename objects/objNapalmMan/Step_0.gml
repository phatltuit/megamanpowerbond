/// @description Insert description here
// You can write your code in this editor
if ready == true{	
	event_inherited();	
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;
	}
	switch (phase) {
		case -1:	      
			if phaseOn == false{
				TurnToPlayer();
				phaseOn = true;
				alarm[4] = ultimate==true?10:25;
				sprite_index = spr_napalm_man_stand;
			}
	        break;
		
		//Shoot 2 kinematic bomb to player
	    case 0:
	        sprite_index = spr_napalm_man_stand_grenade;
			TurnToPlayer();
			if image_index + image_speed >= image_number{
				image_index = image_number - 1;
				if phaseOn == false{
					phaseOn = true;					
					
					var g1 = instance_create_depth(box-image_xscale*4, bbox_top - 8, 0,objNapalmManExplodeGrenade);
					if instance_exists(objPlayer){
						dist = abs(box-image_xscale*4 - objPlayer.x+10);
					}else{
						dist = 0;	
					}
	                g1.vspeed = -sqrt(abs(2*g1.gravity*140));
	                var airtime = abs(2*g1.vspeed/g1.gravity);
					g1.hspeed = (dist/airtime)*image_xscale;	
					
					var g2 = instance_create_depth(box, bbox_top - 8, 0,objNapalmManExplodeGrenade);
					if instance_exists(objPlayer){
						dist = abs(box - objPlayer.x);		
					}else{
						dist = 0;	
					}
	                g2.vspeed = -sqrt(abs(2*g2.gravity*120));
	                var airtime = abs(2*g2.vspeed/g2.gravity);
					g2.hspeed = (dist/airtime)*image_xscale;	
										
					
					alarm[3] = 15;
					lastPhase = 0;
				}
			}
	        break;
		
		//Dash to player
		case 1:
	        if phaseOn == false{
				//Calculate distance and rush to player
				var spd = rushSpd;
				TurnToPlayer();
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x);
					var time = round(dist/spd);
				}else{
					dist = 0;
					var time = 1;
				}	
				wallHit = false;				
				alarm[3] = time+10;
				sprite_index = spr_napalm_man_rush;
				hspeed = spd*image_xscale;
				phaseOn = true;
				
			}
			
			lastPhase = 1;				
	        break;
		
		//Jump and shoot
		case 2:
			lastPhase = 2;
			if phaseOn == false{
				TurnToPlayer();
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x);
					if dist <5{
						dist = 0;	
					}
				}else{
					dist = 0;	
				}
				// velocity required to reach jump height derived with kinematics equations:
                vspeed = -sqrt(abs(2*grav*140));
                var airtime = abs(2*vspeed/grav);
				hspeed = (dist/airtime)*image_xscale;				
				alarm[3] = airtime+10;
				alarm[5] = airtime/3;
				sprite_index = spr_napalm_man_jump_missile;
				phaseOn = true;				
			}
			
			if instance_exists(objPlayer){
				if airShoot == true{
					airShoot = false;
					
					var g1 = instance_create_depth(box-image_xscale*4, bbox_top - 8, 0,objNapalmManRocket);
					g1.speed = 3.5;
					g1.direction = point_direction(g1.x, g1.y, objPlayer.x-20, objPlayer.y);
					g1.image_angle = -direction;
					var g2 = instance_create_depth(box, bbox_top - 8, 0,objNapalmManRocket);
					g2.speed = 3.5;
					g2.direction = point_direction(g2.x, g2.y, objPlayer.x+20, objPlayer.y);	
					g2.image_angle = -direction;
				}
			}					
			break;
			
		case 3:
			lastPhase = 3;
			TurnToPlayer();
			if hp<=14{
				ultimate = true;
				if ground == true{				
					if phaseOn == false{
						sprite_index = spr_napalm_man_jump;
						vspeed = -sqrt(abs(2*grav*100));
						var airtime = abs(2*vspeed/grav);
						alarm[6] = airtime/2;
						airShoot = false;
						phaseOn = true;
						ultimateType = irandom(1);
						airShootCount = 4+ultimateType;
					}else{
						sprite_index = spr_napalm_man_stand;	
					}
				
					if airShootCount <= 0{
						phase = -1;	
						airShootCount = 4;
						phaseOn = false;
					}
				}
			
				if ground == false and airShoot == true{
					gravity = 0;
					vspeed = 0;
					if airShootCount > 0{
						if attack == false{
							if ultimateType == 0 and instance_exists(objPlayer){
								sprite_index = spr_napalm_man_fly_missile;
								var g1 = instance_create_depth(box-image_xscale*4, bbox_top - 8, 0,objNapalmManHomingMissleB);
								g1.direction = point_direction(g1.x, g1.y, objPlayer.x, objPlayer.y);
								g1.image_angle = point_direction(g1.x, g1.y, objPlayer.x, objPlayer.y);
								g1.speed = 4;
								attack = true;
								airShootCount--;
								alarm[7] = 20;
							}else{
								sprite_index = spr_napalm_man_fly_missile;
								var g1 = instance_create_depth(box-image_xscale*4, bbox_top - 8, 0,objNapalmManHomingMissleA);
								g1.gravity = grav;
								g1.vspeed = -sqrt(abs(2*g1.gravity*30));
				                var airtime = abs(2*g1.vspeed/g1.gravity);
								g1.hspeed = ((airShootCount*20)/airtime)*image_xscale;
								g1.image_angle = 270;
								
								var g2 = instance_create_depth(box-image_xscale*4, bbox_top - 8, 0,objNapalmManHomingMissleA);
								g2.gravity = grav;
								g2.vspeed = -sqrt(abs(2*g2.gravity*30));
				                var airtime = abs(2*g2.vspeed/g2.gravity);
								g2.hspeed = -((airShootCount*20)/airtime)*image_xscale;		
								g2.image_angle = 270;
								attack = true;
								airShootCount--;
								alarm[7] = 40;								
							}
						}
					}else{
						airShoot = false;
					}
				}
			}else{
				phase = -1;
				phaseOn = false;	
			}
				
			break;
		
	    default:
	        phase = 0;
	        break;
	}
}