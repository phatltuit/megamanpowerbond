/// @description Insert description here
// You can write your code in this editor
global.defeat = false;
//Restart Player
if global.checkPointX == xstart and global.checkPointY == ystart{
	room_restart();		
}else{	
	switch (global.previousCharacter) {
		case Player.Bass:
		    instance_create_depth(global.checkPointX, global.checkPointY, 0, objBass);
		    break;
			
		case Player.Megaman:
		    instance_create_depth(global.checkPointX, global.checkPointY, 0, objMegaman);
		    break;
			
		case Player.Protoman:
		    instance_create_depth(global.checkPointX, global.checkPointY, 0, objProtoMan);
		    break;
			
		default:
		    // code here
		    break;
	}
	
	//Restart Boss
	for (var i = 0; i < instance_number(objBoss); ++i) {
		var boss = instance_find(objBoss, i);
		x_origin = boss.xstart;
		y_origin = boss.ystart;
		type = boss.object_index;
		instance_destroy(boss);
		instance_create_depth(x_origin, y_origin, 0, type);
	}
}
	
		
//}
