/// @description Insert description here
// You can write your code in this editor
if keyboard_check_pressed(global.keyStart){
	if !instance_exists(objOptionMenu){
		if global.Paused == false{			
			if instance_exists(objPlayer){
				global.character = objPlayer.object_index;
				global.Paused = true;
				global.hp = objPlayer.hp;
				instance_deactivate_all(true);
				instance_create_depth(x,y,-1000,objWeaponMenu);
			}else{
				global.character = noone	
			}		
		}else{
			if objWeaponMenu.eBoost == false and objWeaponMenu.wBoost == false{
				global.Paused = false;			
				instance_activate_all();
				instance_destroy(objWeaponMenu);
				if instance_exists(objZoneSwitcher){
					with objZoneSwitcher	{
						if global.weaponIndex != 0{
							event_perform_object(objPlayer, ev_create, 0);
							PlayerInitial(objPlayer.character);
						}
					}
				}
			
				objPlayer.hp = global.hp;
				if objPlayer.object_index == objProtoMan{
					objPlayer.override = global.override;	
				}
			
				#region deprecated
				//if global.adapter == true { 
				//	if objPlayer.object_index == objMegaman{
				//		objPlayer.adapter = true;		
				//		objPlayer.sprJump = spr_mega_adap_jump;
				//		objPlayer.sprStand = spr_mega_adap_idle;
				//		objPlayer.sprRun = spr_mega_adap_run;
				//		objPlayer.sprClimb = spr_mega_adap_climb;
				//		objPlayer.sprClimbUp = spr_mega_adap_climb_up;
				//		objPlayer.sprClimbStop = spr_mega_adap_climb_stop;
				//		objPlayer.sprHit = spr_mega_adap_hit;					
				//	}
				//}else{
				//	if objPlayer.object_index == objMegaman{
				//		with objMegaman{
				//			adapter = false;
				//			MegamanHandleSprite();	
				//		}
				//	}
				//}
				#endregion
			
			}
		}
	}
}

for (var i = 0; i < 10; ++i) {
	if global.weaponEnergy[i] <= 0{
		 global.weaponEnergy[i] = 0;			
	}
}	



