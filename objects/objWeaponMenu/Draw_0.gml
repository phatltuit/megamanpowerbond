/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_set_font(WeaponMenu);

for (var i = 0; i < 10; ++i) {
	var color = (i == global.weaponIndex)?c_aqua:c_white;
	if global.weaponUnlocked[i] == true{
		if i == 0{				
			var busterAvt = global.weaponAvt[i];
			draw_sprite_ext(busterAvt,global.weaponIndex==i?-1:0,x+X[0], y+Y[i],image_xscale,image_yscale,image_angle,image_blend,(i == global.weaponIndex)?irandom(1):1);
		}else{
			draw_sprite_ext(global.weaponAvt[i],global.weaponIndex==i?-1:0,x+X[0], y+Y[i],image_xscale,image_yscale,image_angle,image_blend,(i == global.weaponIndex)?irandom(1):1);	
		}			
		draw_text_color(x+X[0]+18, y+Y[i]- 11, global.weaponName[i], color, color, color, color, 1);
		draw_sprite_ext(i==0?spr_weapon_menu_default_energy_bar:spr_weapon_menu_energy_bar, global.weaponEnergy[i], x+X[0]+18, y+Y[i]-2,image_xscale,image_yscale,image_angle,image_blend,(i == global.weaponIndex)?irandom(1):1);	    
		if i != 0{
			draw_sprite_ext(spr_weapon_menu_energy_bar_primary, global.weaponEnergy[i], x+X[0]+18, y+Y[i]-2,image_xscale,image_yscale,image_angle,global.weaponColor[i],(i == global.weaponIndex)?irandom(1):1);		
		}
	}
}

draw_sprite_ext(spr_player_health_bar, global.hp,x+X[1]+36, y+10,image_xscale,image_yscale,image_angle,image_blend,1);

draw_sprite_ext(global.normalForm, 0,x+X[1], y+54,image_xscale,image_yscale,image_angle,image_blend,1);

draw_sprite_ext(global.lifeAvt, 0,x+X[1]+4, y+82,image_xscale,image_yscale,image_angle,image_blend,1);
draw_text_color(x+X[1]+38, y+78, global.life, color, color, color, color, 1);

draw_sprite_ext(spr_weapon_menu_e_tank, 0,x+X[1]+6, y+Y2[0],image_xscale,image_yscale,image_angle,image_blend,1);
draw_text_color(x+X[1]+28, y+Y2[0]-4, global.etank, color, color, color, color, 1);

draw_sprite_ext(spr_weapon_menu_m_tank, 0,x+X[1]+6, y+Y2[1],image_xscale,image_yscale,image_angle,image_blend,1);
draw_text_color(x+X[1]+28, y+Y2[1]-4, global.mtank, color, color, color, color, 1);

draw_text_color(x+X[1], y+Y2[2]-4, "OPTION", color, color, color, color, 1);

draw_text_color(x+X[1], y+Y2[3]-4, "ESCAPE", color, color, color, color, 1);

draw_text_color(x+X[1], y+Y2[4]-4, "EXIT", color, color, color, color, 1);











	






