/// @description Insert description here
// You can write your code in this editor
if eBoost == false and wBoost == false{
	if keyboard_check_pressed(global.keyUp){	
		audio_stop_sound(WeaponChange);
		audio_play_sound(WeaponChange, 1, false);
		row--;
		if col == 0{
			if row < 0{
				row = 9;
			}
		}else{
			if row < 0{
				row = 4;
			}	
		}
	}
	if keyboard_check_pressed(global.keyDown){
		audio_stop_sound(WeaponChange);
		audio_play_sound(WeaponChange, 1, false);
		row++;
		if col == 0{
			if row > 9{
				row = 0;
			}
		}else{
			if row > 4{
				row = 0;		
			}	
		}
	}
	if keyboard_check_pressed(global.keyRight){
		audio_stop_sound(WeaponChange);
		audio_play_sound(WeaponChange, 1, false);
		col++;
		if col > 1{
			col = 0;			
		}
		row = 0;
	
	}
	if keyboard_check_pressed(global.keyLeft){
		audio_stop_sound(WeaponChange);
		audio_play_sound(WeaponChange, 1, false);
		col--;
		if col < 0{
			col = 1;			
		}
		row = 0;
	}

	if keyboard_check_pressed(global.keyShoot){	
		if col == 0{
			tempIndex = row;	
			if global.weaponUnlocked[tempIndex] == true{
				global.weaponIndex = tempIndex;	
			}
		}else{			
			switch (row) {
				case 0:
					if global.hp < 27{
						eBoost = true;		
					}
					timer = 0;
				    break;
				
				case 1:
					if !IsAllWeaponFullEnergy(){
						wBoost = true;				
					}
					timer = 0;
				    break;
				
				case 2:
					instance_destroy(selector);					
					instance_create_depth(x,y, depth-1, objOptionMenu);
					instance_destroy();
					break;
					
					
				#region deprecated
				//case 3:					
					//if global.character == objMegaman{
					//	if global.adapter == false{
					//		global.adapter = true;						
					//	}else{
					//		global.adapter = false;	
					//	}
					//}else if global.character == objProtoMan{
					//	if global.override == false{
					//		global.override = true;
					//		global.weaponIndex = 0;
					//	}else{
					//		global.override = false;
					//	}
					//}
					//break;
				#endregion
				
				case 3:
				
					break;
					
				case 4:
					alarm[0] = 2;
					break;
					
				
				default:
				    // code here
				    break;
			}				
		}
	}
}

if eBoost == true{
	if global.hp >= 27{
		eBoost = false;	
		global.etank=clamp(global.etank-1,0,9);
	}else{
		timer++;
		if timer>=5{
			global.hp++;
			audio_play_sound(HealthUp, 1,false);
			timer = 0;			
		}
	}
}

if wBoost == true{
	if IsAllWeaponFullEnergy(){
		wBoost = false;
		global.mtank=clamp(global.mtank-1,0,9);
	}else{
		timer++;
		if timer>=5{
			for (var i = 1; i < 10; ++i) {
				if global.weaponEnergy[i] < 27{
					global.weaponEnergy[i]++;
				}
			}
			audio_play_sound(EnergyUp, 1,false);
			timer = 0;			
		}
	}
}


selector.x = x + X[col]-10;
if col == 0{
	selector.y = y + Y[row];
}else{
	selector.y = y + Y2[row];	
}

