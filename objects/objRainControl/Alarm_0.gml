/// @description Insert description here
// You can write your code in this editor
repeat hard?120:40{
	//part_particles_create(part_sys, random_range(x, x+camera_get_view_width(view_camera[0])), y + random_range(-100, 0), part_rain, 1);
	var blob = instance_create_depth(random_range(x, x+camera_get_view_width(view_camera[0])),y + random_range(-100, 0), 0, objRainDrops);
	blob.gravity = 0.1;
	blob.gravity_direction = dir;
	blob.image_angle = dir-90;
	blob.alarm[0] = life;
	blob.image_alpha = 0.8;
}
alarm[0] = 10;