/// @description Insert description here
// You can write your code in this editor
event_inherited();
instance_create_depth(x,y,0,objZoneLeft);
instance_create_depth(x,y,0,objZoneRight);
yIndex = y + 8;
for (var i = 0; i < 14; ++i) {
    instance_create_depth(x-8,yIndex, 11,objZoneGoRight);
	instance_create_depth(x+8,yIndex, 11,objZoneGoLeft);
	yIndex+=16;
}
