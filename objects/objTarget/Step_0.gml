/// @description Insert description here
// You can write your code in this editor
if instance_exists(objPlayer){
	if global.weaponIndex == 1{
		if follow == true{
			x = objPlayer.x;
			y = objPlayer.y;
			if !instance_exists(objPlayerWeapon){
				instance_destroy();
			}
		}
	}
	if global.weaponIndex == 2{
		if instance_exists(target){
			visible = true;
			sprite_index = spr_target_locked;
			x = target.x;
			y = target.y;
		}else{
			instance_destroy();	
		}
	}
}