/// @description Insert description here
// You can write your code in this editor
camera = view_camera[0];
if objPlayer.openDoor == false{
	if objPlayer.climb == true{
		playerVerSpd = standartPlayerSpd/2;	
	}else{
		playerVerSpd = standartPlayerSpd*2;	
	}
}else{
	playerHorSpd = standartPlayerSpd*2;
	playerVerSpd = standartPlayerSpd*3;
}
switch (dir) {
    case TransitionDirection.GoRight:
		camera_set_view_pos(camera, camera_get_view_x(camera) + cameraHorSpd,camera_get_view_y(camera));  
		hspeed = playerHorSpd;
		objPlayer.vspeed = 0;
		objPlayer.gravity = 0;
		objPlayer.hspeed = playerHorSpd;
		
		if playerDash == true{
			objPlayer.slide = true;				
		}
		
		if camera_get_view_x(camera) >= objPlayer.sectionLeft{
			camera_set_view_pos(camera, objPlayer.sectionLeft,camera_get_view_y(camera));
			objPlayer.visible = true;		
			objPlayer.hspeed = 0;
			objPlayer.block = false;
			objPlayer.openDoor = false;
			with objPlayer{
				PlayerCameraInit();
			}
			if !BossInsideView(){
				objPlayer.block = false;				
			}			
			instance_destroy();
			
		}		
        break;
		
	case TransitionDirection.GoLeft:
		camera_set_view_pos(camera, camera_get_view_x(camera) - cameraHorSpd,camera_get_view_y(camera));  
		hspeed = -playerHorSpd;	
		objPlayer.vspeed = 0;		
		objPlayer.gravity = 0;
		objPlayer.hspeed = -playerHorSpd;
		
		if playerDash == true{
			objPlayer.slide = true;				
		}
		if camera_get_view_x(camera) <= objPlayer.sectionRight - camera_get_view_width(camera){
			camera_set_view_pos(camera, objPlayer.sectionRight - camera_get_view_width(camera),camera_get_view_y(camera));
			objPlayer.visible = true;			
			objPlayer.hspeed = 0;
			objPlayer.block = false;
			objPlayer.openDoor = false;
			
			if !BossInsideView(){
				objPlayer.block = false;
				with objPlayer{
					PlayerCameraInit();
				}
			}
			instance_destroy();
		}
		break;
		
	case TransitionDirection.GoUp:
		camera_set_view_pos(camera, camera_get_view_x(camera) ,camera_get_view_y(camera) - cameraVerSpd); 		
		vspeed = -playerVerSpd;
		objPlayer.vspeed = -playerVerSpd;		
		objPlayer.hspeed = 0;
		objPlayer.gravity = 0;
		
		if camera_get_view_y(camera) <= objPlayer.sectionFloor - camera_get_view_height(camera){
			camera_set_view_pos(camera, camera_get_view_x(camera),objPlayer.sectionFloor - camera_get_view_height(camera));
			objPlayer.visible = true;
			objPlayer.block = false;
			objPlayer.vspeed = 0;
			with objPlayer{
				PlayerCameraInit();
			}
			instance_destroy();
		}
		break;
		
	case TransitionDirection.GoDown:
		camera_set_view_pos(camera, camera_get_view_x(camera) ,camera_get_view_y(camera) + cameraVerSpd);  
		vspeed = playerVerSpd;
		objPlayer.vspeed = playerVerSpd;
		objPlayer.hspeed = 0;
		objPlayer.gravity = 0;
		if camera_get_view_y(camera) >= objPlayer.sectionCeil{
			camera_set_view_pos(camera, camera_get_view_x(camera),objPlayer.sectionCeil);
			objPlayer.visible = true;
			objPlayer.block = false;
			objPlayer.vspeed = 0;
			with objPlayer{
				PlayerCameraInit();
			}
			instance_destroy();
		}
		break;
		
		
    default:
        break;
}