/// @description Insert description here
// You can write your code in this editor
dir = noone;
charge = false;
if instance_exists(objPlayer){	
	objPlayer.visible = false;	
	sprite_index = objPlayer.sprite_index;
	image_speed = objPlayer.image_speed;
	image_xscale = objPlayer.image_xscale;	
	if objPlayer.supportFly == true{
		objPlayer.supportFly = false;	
		SupporterDismiss();
	}
}

if global.weaponIndex != 0{
	event_perform_object(objPlayer, ev_create, 0);
	PlayerInitial(objPlayer.character);
}

playerDash = false;


cameraHorSpd = 4;
cameraVerSpd = 4;
playerHorSpd = 0.4;
playerVerSpd = 0.4;

standartPlayerSpd = 0.45;

