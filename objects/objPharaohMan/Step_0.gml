/// @description Insert description here
// You can write your code in this editor
if ready == true{	
	event_inherited();	
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;
	}
	switch (phase) {
		case -1:
			TurnToPlayer();
			if ground == false{
				sprite_index = spr_pharaoh_man_jump;
			}else{
				if phaseOn == false{	
					TurnToPlayer();
					hspeed = 0;
					sprite_index = spr_pharaoh_man_idle;
					alarm[2] = nextPhaseDelay;
					phaseOn = true;					
				}	
			}			
			break;
		
		case 0:			
			if count >= 3{
				phase = -1;
				count = 0;
				phaseOn = false;	
				jump = false;
			}else{
				if phaseOn == false{				
					dist = DistanceToPlayer()/3;					
					phaseOn = true;				
				}	
				
				if jump == false{
					vspeed = -sqrt(abs(2*grav*60));
	                var airtime = abs(2*vspeed/grav);
					hspeed = (dist/airtime)*image_xscale;	
					alarm[3] = airtime;					
					jump = true;
				}			
				if ground == false and jump == true{
					if attack == false and floor(vspeed) == -1{	
						TurnToPlayer();
						sprite_index = spr_pharaoh_man_jump_shoot;	
						var shot = instance_create_depth(box,y,0,objPharaohManWeapon);
						shot.sprite_index = spr_pharaoh_shot_a;
						BossProjectilesDirectToPlayer(4, shot);
						vspeed = 0;
						attack = true;						
					}
					if attack == false{
						sprite_index = spr_pharaoh_man_jump_charge; 	
					}					
				}else if ground == true{
					sprite_index = spr_pharaoh_man_stand_charge;	
				}
			}			
			nextPhaseDelay = 5;
			lastPhase = 0;
			break;
			
			
		case 1:
			if phaseOn == false{
				vspeed = -sqrt(abs(2*grav*140));
	            var airtime = abs(2*vspeed/grav);				
				phaseOn = true;	
				sprite_index = spr_pharaoh_man_jump;
				timer = airtime/2;
				jump = true;
			}
			
			timer--;
			if timer <= 0 {
				if timer > -60{
					sprite_index = spr_pharaoh_man_jump_charge;
					gravity = 0;
					vspeed = 0;
				}else if timer <= -60 and timer > -80{		
					sprite_index = spr_pharaoh_man_jump_shoot;
					if attack == false{
						TurnToPlayer();
						var i = 20;
						repeat 3{
							var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
							shot.sprite_index = spr_pharaoh_shot_b;
							shot.image_angle = point_direction(x,y,objPlayer.x+i,objPlayer.y);
							BossProjectilesDirectToPlayer(5, shot);		
							shot.direction = point_direction(x,y,objPlayer.x+i,objPlayer.y);
							i-=20;
						}		
						attack = true;
					}					
					gravity = 0;
					vspeed = 0;	
				}else if timer <= -80 and timer > -100{
					sprite_index = spr_pharaoh_man_jump_charge;
					gravity = 0;
					vspeed = 0;	
					attack = false;					
				}else if timer <= -100 and timer > -120{	
					sprite_index = spr_pharaoh_man_jump_shoot;
					if attack == false{
						attack = true;
						TurnToPlayer();
						var i = 20;
						repeat 3{
							var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
							shot.sprite_index = spr_pharaoh_shot_b;
							shot.image_angle = point_direction(x,y,objPlayer.x+i,objPlayer.y);
							BossProjectilesDirectToPlayer(5, shot);		
							shot.direction = point_direction(x,y,objPlayer.x+i,objPlayer.y);
							i-=20;
						}		
					}					
					gravity = 0;
					vspeed = 0;		
				}else if timer <= -120 and timer > -140{
					sprite_index = spr_pharaoh_man_jump_charge;	
					gravity = 0;
					vspeed = 0;	
					attack = false;
				}else  if timer <= -140 and timer > -160{					
					sprite_index = spr_pharaoh_man_jump_shoot;
					if attack == false{
						TurnToPlayer();
						var i = 20;
						repeat 3{
							var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
							shot.sprite_index = spr_pharaoh_shot_b;
							shot.image_angle = point_direction(x,y,objPlayer.x+i,objPlayer.y);
							BossProjectilesDirectToPlayer(5, shot);		
							shot.direction = point_direction(x,y,objPlayer.x+i,objPlayer.y);
							i-=20;
						}	
						attack = true;
					}
					gravity = 0;
					vspeed = 0;		
				}else{
					sprite_index = spr_pharaoh_man_jump;
					if ground == true{
						phase = -1;
						nextPhaseDelay = 25;
						attack = false;
						timer = 0;
						phaseOn = false;
					}
				}
			}		
			lastPhase = 1;
			break;
			
		case 2:
			if phaseOn == false{
				sprite_index = spr_pharaoh_man_shield;
				phaseOn = true;
			}
			if image_index + image_speed >= image_number{
				image_index = image_number - 1;	
				
				if attack == false{					
					alarm[5] = 60;
					attack = true;
				}
				if attack == true {
					if instance_exists(objPlayer){
						if image_xscale == -objPlayer.image_xscale{
							objPlayer.knockBack = true;
							objPlayer.xspeed = 0;
							objPlayer.block = true;
							objPlayer.vspeed = 0;
						}
					}
				}
			}						
			lastPhase = 2;
			break;
			
		case 3:
			
			timer++;
			if timer >= 0 and timer < 20{
				sprite_index = spr_pharaoh_man_stand_charge;
			}else if timer >= 20 and timer < 30 and attack == false{
				TurnToPlayer();
				sprite_index = spr_pharaoh_man_stand_shoot;
				var shot = instance_create_depth(x,y-26,0,objPharaohManWeapon);
				shot.sprite_index = spr_pharaoh_shot_b;
				shot.image_xscale = image_xscale;
				shot.hspeed = 4*image_xscale;
				if instance_exists(objPlayer){
					if objPlayer.block == true{
						objPlayer.block = false;
						objPlayer.knockBack = false;
					}
				}
				attack = true;
			}else if timer >= 30 and timer < 50{
				sprite_index = spr_pharaoh_man_stand_charge;
				attack = false;
			}else if timer >= 50 and timer < 60 and attack == false{
				TurnToPlayer();
				sprite_index = spr_pharaoh_man_stand_shoot;
				var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
				shot.sprite_index = spr_pharaoh_shot_b;
				shot.image_xscale = image_xscale;
				shot.hspeed = 4*image_xscale;	
				attack = true;
			}else if timer >= 60 and timer < 80{
				sprite_index = spr_pharaoh_man_stand_charge;
				attack = false;
			}else if timer >= 80 and timer < 90 and attack == false {
				TurnToPlayer();
				sprite_index = spr_pharaoh_man_stand_shoot;
				var shot = instance_create_depth(x,y-26,0,objPharaohManWeapon);
				shot.sprite_index = spr_pharaoh_shot_b;
				shot.image_xscale = image_xscale;
				shot.hspeed = 4*image_xscale;
				if instance_exists(objPlayer){
					if objPlayer.block == true{
						objPlayer.block = false;
						objPlayer.knockBack = false;
					}
				}
				attack = true;			
			}else if timer >= 90{
				phase = -1;
				nextPhaseDelay = 15;
				attack = false;
				timer = 0;
				phaseOn = false;
			}
			lastPhase = 3;
			break;
			
		case 4:
			if hp <= 14{
				ultimate = true;
				if phaseOn == false{
					dist = abs(x - (camera_get_view_x(view_camera[0])+camera_get_view_width(view_camera[0])/2));
					hspeed = 3*image_xscale;
					sprite_index = spr_pharaoh_man_run;
					timer = round(dist/3);
					phaseOn = true;
				}
				
				timer--;		
				if timer == 0{
					sprite_index = spr_pharaoh_man_charge_ultimate;
					hspeed = 0;
					dist = abs(y-(camera_get_view_y(view_camera[0])+camera_get_view_height(view_camera[0])/2));	
					vspeed = -sqrt(abs(2*grav*dist));
					timer2 = abs(2*vspeed/grav)/2;
				}
				
				if timer <= 0{
					timer2--;	
					if timer2 <= 0{
						vspeed = 0;
						gravity = 0;
						sprite_index = spr_pharaoh_man_ultimate;
						timer3++;
						if timer3 >= 40 and timer3 < 70 and attack == false{	
							var i = 0;
							
							repeat 8{
								var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
								shot.speed = 4;
								shot.sprite_index = spr_pharaoh_shot_b;
								shot.damageToPlayer = 3;
								shot.direction = i;
								shot.image_angle = i;
								
								i+=45;
							}
							attack = true;
						}else if timer3 >= 70 and timer3 < 100{
							attack = false;
						}else if timer3 >= 100 and timer3 < 130 and attack == false{
							var i = 22.5;
							var j = 0;
							repeat 8{
								var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
								shot.speed = 4;
								shot.sprite_index = spr_pharaoh_shot_b;
								shot.damageToPlayer = 3;
								shot.direction = i;
								shot.image_angle = i;
								
								var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
								shot.speed = 1;
								shot.sprite_index = spr_pharaoh_shot_a;
								shot.damageToPlayer = 3;
								shot.direction = j;
								shot.image_angle = j;
								j+=45;
								i+=45;
							}	
							attack = true;
						}else if timer3 >= 130 and timer3 < 160{
							attack = false;
						}else if timer3 >= 160 and timer3 < 190 and attack == false{
							var i = 0;
							var j = 22.5;
							repeat 8{
								var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
								shot.speed = 4;
								shot.sprite_index = spr_pharaoh_shot_b;
								shot.damageToPlayer = 3;
								shot.direction = i;
								shot.image_angle = i;
								
								var shot = instance_create_depth(x,y,0,objPharaohManWeapon);
								shot.speed = 1;
								shot.sprite_index = spr_pharaoh_shot_a;
								shot.damageToPlayer = 3;
								shot.direction = j;
								shot.image_angle = j;
								j+=45;
								i+=45;
							}	
							attack = true;
						}else if timer3 >= 220{
							phase = -1;
							nextPhaseDelay = 10;
							attack = false;
							timer = 0;
							timer2 = 0;
							timer3 = 3;
							phaseOn = false;	
						}
					}
				}					
			}else{
				phase = -1;
				nextPhaseDelay = 15;
				attack = false;
				timer = 0;
				timer2 = 0;
				timer3 = 3;
				phaseOn = false;	
			}		
			lastPhase = 4;
			break;
		
			
				
	    default:
	        phase = 0;
	        break;
	}
}