if instance_exists(objBoss){
	if objBoss.hp > 0{
		image_index = objBoss.hp-1;		
	}else{
		image_index = 0;
	}		
}

if BossInsideView(){
	visible = true;	
}else{
	visible = false;	
}

x = camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) - 16 - abs(bbox_right - x) ;
y = camera_get_view_y(view_camera[0]) + 8;