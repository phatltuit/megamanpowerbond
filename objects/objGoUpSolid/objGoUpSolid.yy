{
  "spriteId": {
    "name": "spr_moving_platform",
    "path": "sprites/spr_moving_platform/spr_moving_platform.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": {
    "name": "spr_moving_platform",
    "path": "sprites/spr_moving_platform/spr_moving_platform.yy",
  },
  "persistent": false,
  "parentObjectId": {
    "name": "objMovingSolid",
    "path": "objects/objMovingSolid/objMovingSolid.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"objGoUpSolid","path":"objects/objGoUpSolid/objGoUpSolid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"objGoUpSolid","path":"objects/objGoUpSolid/objGoUpSolid.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "MovingSolid",
    "path": "folders/Objects/COLLISION/MovingSolid.yy",
  },
  "resourceVersion": "1.0",
  "name": "objGoUpSolid",
  "tags": [],
  "resourceType": "GMObject",
}