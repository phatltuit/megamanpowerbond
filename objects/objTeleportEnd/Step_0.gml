/// @description Insert description here
// You can write your code in this editor
if instance_exists(objPlayer){
	if receivePlayer == true and objPlayer.bbox_right < bbox_right and objPlayer.bbox_left > bbox_left and objPlayer.y > bbox_top and objPlayer.y < bbox_bottom{
		objPlayer.sprite_index = objPlayer.sprStand;
		global.Freeze = true;
		draw_sprite(spr_teleporter_light, image_index, x,y);
		if on == false{
			var light = instance_create_depth(x,y, -2, objTeleportLight);
			light.alarm[0] = 80;
			on = true;
		}
	}
}