/// @description Insert description here
// You can write your code in this editor
draw_self();
if option_index < 0{
	option_index = 1;	
	option_x_index = 0;
	timer[0] = 0;
	timer[1] = 0;
	timer[2] = 0;
	i = 178;
	j = 224;
}else if option_index > 1{
	option_index = 0;	
	timer[0] = 0;
	timer[1] = 0;
	timer[2] = 0;
	i = 178;
	j = 224;
}

if option_x_index < 0{
	option_x_index = 1;	
}else if option_x_index > 1{
	option_x_index = 0;	
}

if player_index > 2{
	player_index = 0;	
	timer[0] = 0;
	timer[1] = 0;
	timer[2] = 0;
	i = 178;
	j = 224;
}else if player_index < 0{
	player_index = 2; 	
	timer[0] = 0;
	timer[1] = 0;
	timer[2] = 0;
	i = 178;
	j = 224;
}

draw_sprite_ext(spr_player_select_screen_player_demo, 0,0,0,image_xscale,image_yscale, image_angle, image_blend,1);
draw_sprite_ext(select_player[player_index], 0,0,0,image_xscale,image_yscale, image_angle, image_blend,1);

if player_index == 0{
	draw_sprite_ext(player_demo, -1,144,130,image_xscale,image_yscale, image_angle, image_blend,1);	
	if timer[player_index] >= 240 and timer[player_index] < 260{
		i+=3;
		draw_sprite_ext(spr_mega_man_giga_buster, -1,i,117,image_xscale,image_yscale, image_angle, image_blend,1);	
	}
}else if player_index == 1{
	draw_sprite_ext(player_demo, -1,144,130,image_xscale,image_yscale, image_angle, image_blend,1);	
	if timer[player_index] >= 240 and timer[player_index] < 260{
		i+=3;
		draw_sprite_ext(spr_proto_man_giga_buster, -1,i,117,image_xscale,image_yscale, image_angle, image_blend,1);	
	}
	if timer[player_index] >= 420 and timer[player_index] < 450{
		j-=2;
		draw_sprite_ext(spr_bass_mini_buster, -1,j,117,image_xscale,image_yscale, image_angle, image_blend,1);	
	}else if timer[player_index] >= 450 and timer[player_index] < 520{
		j+=2;
		draw_sprite_ext(spr_bass_mini_buster, -1,j,117,image_xscale,image_yscale, image_angle, image_blend,1);	
	}
}

if option_index == 0 {
	timer[player_index]++;	
	draw_sprite_ext(spr_player_select_screen_select_arrow, 0,0,0,image_xscale,image_yscale, image_angle, image_blend,irandom(1));		
}else{
	draw_sprite_ext(spr_player_select_screen_option_arrow, 0,option_x[option_x_index], option[option_index],image_xscale,image_yscale, image_angle, image_blend,1);		
}