/// @description Insert description here
// You can write your code in this editor
if keyboard_check_pressed(global.keyUp){	
	option_index--;	
	timer[0] = 0;
	timer[1] = 0;
	timer[2] = 0;
	i = 178;
	j = 224;
}else if keyboard_check_pressed(global.keyDown){
	option_index++;	
	if option_index == 1{
		option_x_index = 0;			
	}
	timer[0] = 0;
	timer[1] = 0;
	timer[2] = 0;
	i = 178;
	j = 224;	
}
if option_index == 0{
	if keyboard_check_pressed(global.keyLeft){		
		player_index--;	
		timer[0] = 0;
		timer[1] = 0;
		timer[2] = 0;
		i = 178;
		j = 224;
	}else if keyboard_check_pressed(global.keyRight){	
		player_index++;	
		timer[0] = 0;
		timer[1] = 0;
		timer[2] = 0;
		i = 178;
		j = 224;
	}	
	if player_index == 0{
		if timer[player_index] >= 0 and timer[player_index] < 120{
			player_demo = spr_player_select_screen_mega_idle_demo;		
		}else if timer[player_index] >= 120 and timer[player_index] < 240{
			player_demo = spr_player_select_screen_mega_charge_demo;	
		}else if timer[player_index] >= 240 and timer[player_index] < 300{
			player_demo = spr_player_select_screen_mega_shoot_demo;	
		}else if timer[player_index] >= 300 and timer[player_index] < 420{
			player_demo = spr_player_select_screen_mega_idle_demo;		
		}else if timer[player_index] >= 420 and timer[player_index] < 435{
			player_demo = spr_player_select_screen_mega_slide_demo;	
		}else if timer[player_index] >= 420{
			timer[player_index] = 0;	
			i = 178;
			j = 224;
		}
	}else if player_index == 1{
		if timer[player_index] >= 0 and timer[player_index] < 120{
			player_demo = spr_player_select_screen_proto_idle_demo;		
		}else if timer[player_index] >= 120 and timer[player_index] < 240{
			player_demo = spr_player_select_screen_proto_charge_demo;	
		}else if timer[player_index] >= 240 and timer[player_index] < 300{
			player_demo = spr_player_select_screen_proto_shoot_demo;	
		}else if timer[player_index] >= 300 and timer[player_index] < 420{
			player_demo = spr_player_select_screen_proto_idle_demo;		
		}else if timer[player_index] >= 420 and timer[player_index] < 435{
			player_demo = spr_player_select_screen_proto_idle_demo;		
		}else if timer[player_index] >= 435 and timer[player_index] < 555{
			player_demo = spr_player_select_screen_proto_idle_demo;		
		}else if timer[player_index] >= 555 and timer[player_index] < 655{
			player_demo = spr_player_select_screen_proto_slide_demo;		
		}else if timer[player_index] >= 655 and timer[player_index] < 715{
			player_demo = spr_player_select_screen_proto_idle_demo;		
		}else if timer[player_index] >= 715{
			timer[player_index] = 0;	
			i = 178;
			j = 224;
		}
	}
}else{
	if keyboard_check_pressed(global.keyLeft){		
		option_x_index--;	
		
	}else if keyboard_check_pressed(global.keyRight){	
		option_x_index++;
	}
	
	if keyboard_check_pressed(global.keyStart) or keyboard_check_pressed(global.keyShoot){
		if option_index == 1{
			audio_stop_all();
			room_goto(INTRO);	
		}
		if option_index == 0{	
			audio_stop_all();
			room_goto(STAGE_SELECT);	
		}
	}
}	