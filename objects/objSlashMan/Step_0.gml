/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_shadow_man_pose and image_index + image_speed >= image_number{
	image_index = image_number - 1;	
}

if ready == true{	
	event_inherited();	
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;
	}
	switch (phase) {
			case -1:
				TurnToPlayer();
				if ground == false{
					if lastPhase == 0{
						if maxCount == 3{
							sprite_index = spr_slash_man_pounce;
						}
					}else{
						sprite_index = spr_slash_man_air_roll;
					}
				}else{
					if phaseOn == false{					
						hspeed = 0;
						sprite_index = spr_slash_man_idle;						
						alarm[2] = nextPhaseDelay;						
						phaseOn = true;					
					}	
				}			
				break;
		
			case 0:
				if phaseOn == false{
					TurnToPlayer();
					if instance_exists(objPlayer){						
						dist = abs(x-objPlayer.x);
						if dist > 100{
							dist = (dist/3)-5;
							maxCount = 3;
						}else{								
							maxCount = 1;
						}
					}else{
						dist = 0;	
					}			
					count = 0;
					phaseOn = true;
				}				
				if timer <= 0{
					sprite_index = spr_slash_man_air_roll;
					if maxCount == 3{
						vspeed = -sqrt(abs(2*grav*5));
					}else{
						vspeed = -sqrt(abs(2*grav*60));
					}
					timer = abs(2*vspeed/grav);
					hspeed = (dist/timer)*image_xscale;	
					timer_a = (timer-timer/3);
					count++;
				}
		
				if hspeed != 0 and place_meeting(x+hspeed+sign(hspeed),y,objSolid){
					hspeed = 0;						
				}		
				if timer <= timer_a and maxCount == 3{
					sprite_index = spr_slash_man_pounce;
				}
				timer--;
				
				if count >= maxCount{
					nextPhaseDelay = 10;	
					phaseOn = false;
					phase = -1;
				}
				lastPhase = 0;
				break;
		
			case 1:
				if phaseOn == false{
					phaseOn = true;
					sprite_index = spr_slash_man_stand_slash;
				}
				if sprite_index == spr_slash_man_stand_slash and image_index + image_speed >= image_number{
					var shot = instance_create_depth(x+image_xscale*30,y,0,objSlashManWeapon);
					shot.sprite_index = spr_slash_man_sonic_boom_b;
					shot.image_xscale = image_xscale;
					nextPhaseDelay = 10;	
					phaseOn = false;
					phase = -1;
					sprite_index = spr_slash_man_idle;
				}
				lastPhase = 1;
				break;
		
			case 2:
				if phaseOn == false{	
					xscale[0] = -1;
					xscale[1] = 1;
					rd_dir = irandom(1);
					image_xscale = xscale[rd_dir];					
					if instance_exists(objPlayer){	
						dist = 128;
						maxCount = 1;						
					}else{
						dist = 0;	
					}			
					count = 0;
					phaseOn = true;
				}				
				if timer <= 0{
					sprite_index = spr_slash_man_air_roll;					
					vspeed = -sqrt(abs(2*grav*80));					
					timer = abs(2*vspeed/grav);					
					hspeed = (dist/timer)*image_xscale*2;
					if image_xscale == 1{
						gravity_direction = 0;
					}else{
						gravity_direction = 180;
					}
					count++;
				}
		
				if hspeed != 0 and place_meeting(x+hspeed+sign(hspeed),y,objSolid){					
					sprite_index = spr_slash_man_wall_stand;
					hspeed = 0;	
					vspeed = 0;
					hspeed = 0;
					speed = 0;
					if gravity_direction == 0 or gravity_direction == 180{
						wallLedge = true;						
						alarm[3] = 40;
						if gravity_direction == 0{
							image_xscale = -1;	
						}else if gravity_direction == 180{
							image_xscale = 1;	
						}
					}
				}
				
				if wallLedge == false{
					timer--;
				}else{							
					vspeed = 0;
					hspeed = 0;
					speed = 0;					
				}				
				lastPhase = 2;				
				break;			
			
			case 3:
				if phaseOn == false{					
					phaseOn = true;
					if irandom(1) == 0{
						TurnToPlayer();
						range_attack = true;	
						melee_attack = false;
						count = 0;
						timer = 1;
					}else{
						melee_attack = true;	
						range_attack = false;	
					}						
				}
				if range_attack == true{
					timer--;
					if timer<=0{
						sprite_index = spr_slash_man_wall_stand_slash;		
					}
					if sprite_index == spr_slash_man_wall_stand_slash and image_index + image_speed >= image_number{
						var shot = instance_create_depth(x,y,0, objSlashManWeapon);
						shot.sprite_index = spr_slash_man_sonic_boom_a;
						if instance_exists(objPlayer){
							shot.speed = 5;
							shot.direction = point_direction(x,y, objPlayer.x, objPlayer.y);
							shot.image_angle = point_direction(x,y, objPlayer.x, objPlayer.y);
						}else{
							shot.hspeed = 5*image_xscale;
							shot.image_xscale = image_xscale;
							shot.vspeed = 1;
						}	
						sprite_index = spr_slash_man_wall_stand;	
						count++;
						timer = 15;
					}
					
					if count >= 3{
						range_attack = false;
						phaseOn = false;
						phase = -1;			
						count = 0;
						wallLedge = false;
						nextPhaseDelay = 20;
						gravity_direction = 270;
					}
				}	
				
				if melee_attack == true{
					if pounce == false{
						pounce = true;
						if instance_exists(objPlayer){
							TurnToPlayer();
							dist = abs(point_distance(x,y,objPlayer.x,objPlayer.y));
							speed = 5;
							direction = point_direction(x,y,objPlayer.x,objPlayer.y);
							timer = dist/5;
						}else{
							dist = 0;
							melee_attack = false;
							phaseOn = false;
							phase = -1;	
							count = 0;
							wallLedge = false;
							nextPhaseDelay = 20;
							gravity_direction = 270;
						}					
						sprite_index = spr_slash_man_rush;
						gravity_direction = 270;
						
					}
					timer--;
					if timer <= 0{
						melee_attack = false;
						phaseOn = false;
						phase = -1;		
						count = 0;
						wallLedge = false;
						nextPhaseDelay = 20;	
						pounce = false;
					}
					gravity = 0;	
				}
				lastPhase = 3;
				break;
				
			case 4:
				if hp <= 14{
					ultimate = true;
					if phaseOn == false{
						TurnToPlayer();
						phaseOn = true;	
						timer = 60;
					}
					timer--;
					if timer<=0{
						if timer == 0{
							sprite_index = spr_slash_man_stand_slash;
							if instance_exists(objPlayer){
								x = clamp(objPlayer.x + image_xscale*35,camera_get_view_x(view_camera[0]) + 30, camera_get_view_x(view_camera[0]) + 226);
								TurnToPlayer();
							}
						}						
						if sprite_index == spr_slash_man_stand_slash and image_index + image_speed >= image_number{
							var shot = instance_create_depth(x+image_xscale*30,y,0,objSlashManWeapon);
							shot.sprite_index = spr_slash_man_sonic_boom_b;
							shot.image_xscale = image_xscale;
							phaseOn = false;
							sprite_index = spr_slash_man_idle;
							count++;
						} 	
					}
					
					if count == 4{
						phase = -1;
						phaseOn = false;
						count = 0;
						nextPhaseDelay = 20;
					}
						
				}else{
					phase = -1;			
				}
				lastPhase = 4;
				break;
	    default:
	        phase = 0;
	        break;
	}
}