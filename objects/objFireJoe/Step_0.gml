/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if instance_exists(objPlayer) and enable == true{
	if objPlayer.ready == true{
		var box;
		if image_xscale > 0
			box = bbox_right 
		else 
			box = bbox_left;
		
	
		if attack == false and timer == 180{
			sprite_index = spr_fire_joe_shoot;
			flame = instance_create_depth(box, y-5, 0,objEnemyWeapon);
			flame.sprite_index = spr_fire_joe_weapon;
			flame.image_xscale = image_xscale;		
			flame.alarm[0] = 90;
			attack = true;
			timer--;
		}
		timer--;
		if attack == true and timer < 90{
			sprite_index = spr_fire_joe_stand;	
			TurnToPlayer();
			if timer <= 0{
				timer = 180;	
				attack = false;
			}
		}
	}
}else{
	instance_destroy(flame);
	speed = 0;		
}

