/// @description Insert description here
// You can write your code in this editor

if target != noone{
	if instance_exists(target){
		if initial == true{
			radius = point_distance(target.x, target.y, x, y);
			phase = point_direction(target.x, target.y, x, y);
			initial = false;
			alarm[0] = 60;
		}
		x = target.x + lengthdir_x(radius, phase);
		y = target.y + lengthdir_y(radius, phase);
		phase+=10;
	}else{
		instance_destroy();	
	}
}else{
	instance_destroy();	
}