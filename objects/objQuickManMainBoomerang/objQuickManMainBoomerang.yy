{
  "spriteId": {
    "name": "spr_quick_man_boomerang_1",
    "path": "sprites/spr_quick_man_boomerang_1/spr_quick_man_boomerang_1.yy",
  },
  "solid": true,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "objBossWeapon",
    "path": "objects/objBossWeapon/objBossWeapon.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"objQuickManMainBoomerang","path":"objects/objQuickManMainBoomerang/objQuickManMainBoomerang.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"objQuickManMainBoomerang","path":"objects/objQuickManMainBoomerang/objQuickManMainBoomerang.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":2,"collisionObjectId":null,"parent":{"name":"objQuickManMainBoomerang","path":"objects/objQuickManMainBoomerang/objQuickManMainBoomerang.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "QUICK_MAN",
    "path": "folders/Objects/BOSSES/SHADOW_MAN/QUICK_MAN.yy",
  },
  "resourceVersion": "1.0",
  "name": "objQuickManMainBoomerang",
  "tags": [],
  "resourceType": "GMObject",
}