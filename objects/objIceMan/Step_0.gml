/// @description Insert description here
// You can write your code in this editor
if sprite_index = spr_shadow_man_pose and image_index + image_speed >= image_number{
	image_index = image_number - 1;	
}

if ready == true{	
	event_inherited();	
	var box;
	if image_xscale == 1{
		box = bbox_right;	
	}else{
		box = bbox_left;
	}
	switch (phase) {
		case -1:
			TurnToPlayer();
			if ground == false{
				if lastPhase == 1{
					sprite_index = spr_ice_man_jump_back;
				}else{
					sprite_index = spr_ice_man_jump;
				}
					
			}else{
				if phaseOn == false{					
					hspeed = 0;
					sprite_index = spr_ice_man_idle;						
					alarm[2] = nextPhaseDelay;						
					phaseOn = true;					
				}	
			}			
			break;		
				
			
		case 0:
			if phaseOn == false{
				TurnToPlayer()
				sprite_index = spr_ice_man_raise_hand;
				phaseOn = true;					
				shot = instance_create_depth(x+image_xscale*5,y - 26, -2, objIceManWeapon);
				shot.sprite_index = spr_ice_man_ice_ball_gathering;					
				timer = 60;					
			}
			timer--;
			if timer == 0{	
					
				var shot = instance_nearest(x,y, objIceManWeapon);	
				if shot >= 0{
						
						if instance_exists(objPlayer){
							dist = abs(x+image_xscale*5-objPlayer.x);
						}
					if count == 0{
						sprite_index = spr_ice_man_throw;	
						h = 60;
						shot.grav = 0.25;
						shot.vspeed = -sqrt(abs(2*shot.grav*h));
						airtime = abs(2*shot.vspeed/shot.grav);
						shot.hspeed = (dist/airtime)*image_xscale;	
					}else{
						shot.hspeed = 4*image_xscale;	
						shot.y = ystart-5;
						shot.x = x+image_xscale*25;
						shot.alarm[0] = dist/4;
						//shot.grav = 0.25;							
					}	
					count++;
				}
					
				phaseOn = false;
			}		
				
			if count >= 2{
				phase = -1;
				nextPhaseDelay = 20;
				phaseOn = false;
				count = 0;
			}
			lastPhase = 0;
			break;
			
		case 1:
			if phaseOn == false{				
				if instance_exists(objPlayer){
					dist = abs(x - objPlayer.x);
				}else{
					dist = 0;	
				}
				if count==0{
					sprite_index = spr_ice_man_jump;
					vspeed = -sqrt(2*grav*100);
					airtime = abs(2*vspeed/grav);
					hspeed = (dist/airtime)*image_xscale;		
				}else{
					dist=128;
					sprite_index = spr_ice_man_jump_back;
					vspeed = -sqrt(2*grav*20);
					airtime = abs(2*vspeed/grav);
					hspeed = -(dist/airtime)*image_xscale;
				}				
				timer = airtime;
				phaseOn = true;
				count++;
			}
			timer--;
			if timer<=0{
				if timer <= 15{
					phaseOn = false;
				}
				hspeed = 0;
			}else{
				if vspeed >= 0 and count == 1{
					sprite_index = spr_ice_man_jump_back;
				}
			}
			if count >= 2{				
				phase = -1;
				nextPhaseDelay = 2;
				phaseOn = false;	
				count = 0;				
			}
			
			lastPhase = 1;
			break;
				
				
		case 2:
			if phaseOn == false{
				sprite_index = spr_ice_man_jump_charge_b;
				vspeed = -sqrt(abs(2*grav*120));
				timer_max = abs(2*vspeed/grav);
				timer = timer_max;
				phaseOn = true;
				TurnToPlayer();
			}
			timer--;
			if timer <= (timer_max/2){
				if attack == false{
					if instance_exists(objPlayer){
						dir = point_direction(x,y, objPlayer.x, objPlayer.y);
					}else{
						dir = image_xscale == -1?225:315;	
					}
						
					attack = true;
				}					
				sprite_index = spr_ice_man_two_hand_jump_shoot;					
				var shot = instance_create_depth(box,y-6, -3, objIceManWeapon);						
				if instance_exists(objPlayer){	
					shot.sprite_index = spr_ice_man_ice_beam;
					shot.speed = 8;			
					shot.image_angle = dir;		
					shot.direction = dir;
				}else{
					shot.sprite_index = spr_ice_man_ice_beam;
					shot.hspeed = 8*image_xscale;
					shot.image_xscale = image_xscale;	
				}		
			
				vspeed = 0;
				gravity = 0;
			}else if timer > (timer_max/2) and timer <= timer_max{
				sprite_index = spr_ice_man_jump;					
			}
				
			if timer <= -30{
				phase = -1;
				nextPhaseDelay = 10;
				phaseOn = false;
				attack = false;
			}
				
			lastPhase = 2;
			break;	
			
		case 3:
			if phaseOn == false{
				sprite_index = spr_ice_man_stand_charge;
				phaseOn = true;
				if instance_exists(objPlayer){
					dist = abs(x-objPlayer.x);
				}else{
					dist = 0;	
				}
				timer = 30;					
			}
			timer--;
			if timer <= 0{
				if timer == 0{
					if instance_exists(objPlayer){
						dir = objPlayer.x;
					}else{
						dir = xstart;	
					}
				}
				sprite_index = spr_ice_man_one_hand_stand_shoot;
				if timer == -20{
					if instance_exists(objPlayer){		
						var i = 90;
						repeat 5{
							var shot = instance_create_depth(dir+i,ystart, -3, objIceManWeapon);
							shot.image_xscale = image_xscale;
							shot.sprite_index = spr_ice_man_ice_pillar;	
							i-=45;
						}
													
					}else{
						var shot = instance_create_depth(x + 30*image_xscale, ystart, -3, objIceManWeapon);
						shot.image_xscale = image_xscale;
						shot.sprite_index = spr_ice_man_ice_pillar;
					}
						
					count++;
				}
				if timer<=-30{
					phaseOn = false
				}
			}	
				
			if count >= 3{
				phase = -1;
				phaseOn = false;
				nextPhaseDelay = 15;
				count = 0;
			}
			lastPhase = 3;
			break;
			
		case 4:
			if hp <= 20{
				ultimate = true;
				if phaseOn == false{	
					TurnToPlayer();
					dist = 20;					
					sprite_index = spr_ice_man_jump;
					vspeed = -sqrt(2*grav*22);
					airtime = abs(2*vspeed/grav);
					hspeed = (dist/airtime)*image_xscale;
					timer = airtime/2;
					phaseOn = true;					
				}	
				
				timer--;
				if timer <= 0{					
					if attack == false{	
						vspeed = 0;
						hspeed = 0;
						sprite_index = spr_ice_man_ultimate;
						if instance_exists(objPlayer){
							dist = 128;							
							hspeed = 5*image_xscale;							
						}else{
							dist = 64;
							hspeed = 5*image_xscale;
						}		
						attack = true;
						timer_a = dist/5;
						grav = 0;
					}
					timer_a--;
					if timer_a <= 0 or hspeed == 0{
						grav = 0.25;
						if ground == false{
							sprite_index = spr_ice_man_jump;	
						}else{
							count++;
							phaseOn = false;	
							attack = false;
							hspeed = 0;
						}
					}
					if count >= 5{
						grav = 0.25;
						phase = -1;
						attack = false;
						phaseOn = false;
						nextPhaseDelay = 5;
					}
				}
			}else{
				grav = 0.25;
				phase = -1;
				attack = false;
				phaseOn = false;
				nextPhaseDelay = 2;	
			}
			
			lastPhase = 4;
			break;
			
			
	    default:
	        phase = 0;
	        break;
	}
}