{
  "spriteId": {
    "name": "spr_tile_bg_2",
    "path": "sprites/spr_tile_bg_2/spr_tile_bg_2.yy",
  },
  "tileWidth": 256,
  "tileHeight": 224,
  "tilexoff": 0,
  "tileyoff": 0,
  "tilehsep": 1,
  "tilevsep": 1,
  "spriteNoExport": true,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "out_tilehborder": 2,
  "out_tilevborder": 2,
  "out_columns": 3,
  "tile_count": 8,
  "autoTileSets": [],
  "tileAnimationFrames": [],
  "tileAnimationSpeed": 15.0,
  "tileAnimation": {
    "FrameData": [
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
    ],
    "SerialiseFrameCount": 1,
  },
  "macroPageTiles": {
    "SerialiseWidth": 0,
    "SerialiseHeight": 0,
    "TileSerialiseData": [],
  },
  "parent": {
    "name": "Tile Sets",
    "path": "folders/Tile Sets.yy",
  },
  "resourceVersion": "1.0",
  "name": "TileSet3",
  "tags": [],
  "resourceType": "GMTileSet",
}