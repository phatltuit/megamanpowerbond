{
  "compression": 0,
  "volume": 0.5,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 11025,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "BossBattle.mp3",
  "duration": 180.040924,
  "parent": {
    "name": "MUSIC",
    "path": "folders/Sounds/MUSIC.yy",
  },
  "resourceVersion": "1.0",
  "name": "BossBattle",
  "tags": [],
  "resourceType": "GMSound",
}