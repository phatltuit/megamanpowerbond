{
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Victory.mp3",
  "duration": 4.419694,
  "parent": {
    "name": "MUSIC",
    "path": "folders/Sounds/MUSIC.yy",
  },
  "resourceVersion": "1.0",
  "name": "Victory",
  "tags": [],
  "resourceType": "GMSound",
}