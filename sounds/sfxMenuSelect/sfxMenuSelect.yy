{
  "compression": 0,
  "volume": 1.0,
  "preload": true,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfxMenuSelect",
  "duration": 0.605091,
  "parent": {
    "name": "MENU",
    "path": "folders/Sounds/MENU.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfxMenuSelect",
  "tags": [],
  "resourceType": "GMSound",
}