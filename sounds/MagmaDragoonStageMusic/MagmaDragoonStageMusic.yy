{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 11025,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "MagmaDragoonStageMusic.wav",
  "duration": 0.0,
  "parent": {
    "name": "STAGE",
    "path": "folders/Sounds/STAGE.yy",
  },
  "resourceVersion": "1.0",
  "name": "MagmaDragoonStageMusic",
  "tags": [],
  "resourceType": "GMSound",
}